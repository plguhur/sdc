\documentclass[runningheads,a4paper]{llncs}

\newcommand*{\rootPath}{./}

\input{\rootPath Annexes/Packages.tex}

\DeclareMathOperator{\FPR}{FPR}
\DeclareMathOperator{\GTE}{GTE}
\DeclareMathOperator{\LTE}{LTE}
\DeclareMathOperator{\TPR}{TPR}

\standalonetrue

\title{Lightweight and Accurate Silent Data Corruption Detection in Ordinary Differential Equation Solvers}
\titlerunning{Hot Rod} 

\begin{document}

\author{Pierre-Louis Guhur,  \inst{1,2}  
        Hong Zhang,  \inst{1}   
        Tom Peterka,  \inst{1} 
        Emil Constantinescu, \inst{1} and
        Franck Cappello\inst{1}}
\authorrunning{Guhur et al.}
\institute{Argonne National Laboratory (USA)\\
\email{\{hongzh, emconsta, tpeterka, cappello\}@mcs.anl.gov}
\and ENS de Cachan (France)\\
\email{pierre-louis.guhur@ens-cachan.fr}
}


\maketitle

\begin{abstract}
% context
Silent data corruptions (SDCs) are errors that corrupt the system or falsify results while remaining unnoticed by firmware or operating systems. In numerical integration solvers, SDCs that impact the accuracy of the solver are considered significant.
%In high performance computing (HPC), the increase of the number of components in large scale architectures makes SDC more likely to occur. 
Detecting SDCs in high-performance computing is necessary because results need to be trustworthy and the increase of the number and complexity of components in emerging large-scale architectures makes SDCs more likely to occur.
% problematique
Until recently, SDC detection methods consisted in replicating the processes of the execution or in using checksums (for example algorithm-based fault tolerance). 
Recently, new detection methods have been proposed relying on mathematical properties of numerical kernels or performing data analysis of the results modified by the application.  
None of those methods, however, provide a lightweight solution guaranteeing that all significant SDCs are detected.
% solution
We propose a new method called \emph{Hot Rod} as a solution to this problem. It checks and potentially corrects the data produced by numerical integration solvers. Our theoretical model shows that all significant SDCs can be detected.
% results
We present two detectors and conduct experiments on streamline integration from the WRF meteorology application. Compared with the algorithmic detection methods, the accuracy of our first detector is increased by $52 \%$ with a similar false detection rate. The second detector has a false detection rate one order of magnitude lower than these detection methods while improving the detection accuracy by $23\%$. The computational overhead is lower than $5\%$ in both cases.
The model has been developed for an explicit Runge-Kutta method, although it can be generalized to other solvers. 
\end{abstract}


\keywords{resilience, fault tolerance, Runge-Kutta, numerical integration  solvers, HPC, SDC}


%%=============================================================================

\section{Introduction}
\label{sec:intro}


Ensuring trustworthy results has always been a critical challenge for scientists. In numerical simulations, results can be impaired by silent data corruptions (SDCs). Because of an ever increasing number of processes, exascale reports \cite{snir2014addressing} project an increase in the SDC rate in future systems.
The origins of SDC are diverse. Examples of SDC sources are electromagnetic interferences \cite{lapinsky2006electromagnetic}, ionizing radiation \cite{bagatin2015ionizing}, and aging of hardware components.

Replication \cite{guerraoui1997software} can detect SDCs by duplicating the same program (or other versions in $n$-version programming \cite{chen1978n}) and comparing their results. In a deterministic program, all duplications must provide exactly the same result; otherwise the results are considered corrupted. 
The protection of linear algebra results in algorithm-based fault tolerance (ABFT) \cite{huang1984algorithm} and the error-correction code memory (ECC) \cite{ghosh2005selecting} are both based on checksums: ABFT computes and performs detection inside the software, and ECC memory is inside the hardware. All these methods are generic (although ABFT is limited to certain numerical kernels), and only a few percents of SDCs are undetected. However, these methods may be too computationally expensive (replication), or they do not protect each component. ABFT covers only the data used in the kernel and not the other data used by the application. ECC protects only memory, caches, and registers; it usually does not protect the CPU control logic or its functional units.

In the context of iterative, time-stepping methods, new detection techniques compare the results of the numerical method with results produced by a surrogate function.  Because previous steps of the numerical method have already been validated, the surrogate function can use these values as trusted references to compute its own results for the current step. In the adaptive impact-driven (AID) detector \cite{di2016aid}, the surrogate function computes value predictions for the current step by extrapolation from several past steps of the numerical method. If the difference between the numerical method results and the surrogate function predictions is outside a certain confidence interval, an SDC is reported. AID uses different extrapolation methods from order 0 to 2 and selects dynamically the one that minimizes the prediction error. The confidence interval is built from the acceptable bound  (given by the user) upon which SDCs are considered as impacting the results, the number of false positives, and the maximum error of extrapolation. Following a different direction, Benson et al. \cite{benson2014silent} propose a more complex surrogate function by computing an error estimate. As with the AID detector, the estimate is compared to a predicted value. If the estimate for the current step is not similar to previous estimates, an SDC is reported. Their detector is called \emph{BSS14}. While the two approaches are different, they both use extrapolation and thus rely on the smoothness property of the data set (AID) or of the estimate (BSS14) to perform accurate detection. As shown in \cref{sec:xp}, they do not guarantee that all SDCs impacting the accuracy of the iterative methods are detected, in particular when the data set (AID) or the estimate (BSS14) presents stiff variations. 


Our objective is to design and develop a new SDC detection technique that presents a high detection accuracy (also called recall or true positive rate, $\TPR$) and a low false detection rate (also called false positive rate, $\FPR$), and does not rely on extrapolation. \emph{We mathematically show that all significant SDCs are detected}. In the context of numerical integration solvers, a solver is chosen because its approximation error is acceptable with respect to the required accuracy of the results. We consider that an SDC is significant when the introduced error is bigger than the approximation error of the solver. We built two new detectors relying on mathematical properties of the ordinary differential equation (ODE) integration method. Our detection technique compares two estimates of this approximation error.  We chose estimates that are similar if and only if no significant SDC occurs. A confidence interval on the similarity, established from a simple machine learning algorithm, controls the SDC detection. If an SDC is detected, the correction is done by recomputing the step.  The two detectors present different tradeoffs. One has a high accuracy (we call it \emph{Hot Rod HR}, for resilient ODE high recall) and small false detection rate. The other has a false detection rate lower than $1\%$ but also a lower accuracy (we call it \emph{Hot Rod LFP}, for low false positive). We designed the two detectors for Cash-Karp's method  \cite{cash1990variable}, a fourth-order Runge-Kutta method with a fixed-step size. However, our technique can be applied to other ODE integration methods as discussed in \cref{sec:ccl}. 




Because all significant SDCs are detected, our detectors improve the trustworthiness of the results while avoiding wasting of resources to recover from insignificant SDCs. 
We performed experiments on a streamline integrator used for visualizing of WRF meteorology application results~\cite{peterka_ipdps11}. 


\Cref{sec:context} explains background. In \cref{sec:method}, our method for detecting SDC is detailed, and proof is given that all significant SDC are detected. In \cref{sec:xp}, our SDC detectors are tested in a meteorology application and compared with replication and the AID and BSS14  detectors. 
%\Cref{sec:ccl} summarizes our conclusions.



\section{Background}
\label{sec:context}


An ODE is a differential equation of one independent variable and its derivatives.  Because numerical integration solvers are widely used, trust in their results is critical.  An initial value problem can be formulated as
\begin{equation*}
x'(t) = f(t,x(t)),\quad x(t_0) = x_0,
\end{equation*}
with $t_0 \in \mathbb{R}, x_0\in\mathbb{R}^m,x : \mathbb{R} \rightarrow \mathbb{R}^m$, and $f : \mathbb{R} \times \mathbb{R}^n \rightarrow \mathbb{R}^m$; $f$ is $L$-Lipschitz continuous.



\subsection{Runge-Kutta methods}
\label{sec:rkm}

For each $n = 1,...,N$ with $N$ the total number of steps, Runge-Kutta methods (RKMs) provide an approximation $x_{n}$ of $x(t_n)$, where $t_n = t_0 + nh$, $h \in \mathbb{R}^*_+$ the step size, and $x(t_n)$ the exact solution of the ODE at time $t_n$.
An $s$-stage explicit RKM is defined by
\begin{align*}
\forall i\leq s, k_i = f\left(t_n + c_ih, x_n + h\sum_{j = 1}^{i-1}{a_{ij}k_j} \right); \quad
x_{n+1} = x_n + h\sum_{i = 1}^s{b_i k_i}.
\end{align*}
Here $(k_i)_i$ are called the stage slopes and represent the most computationally expensive part of the method. %The coefficients $(a_{ij})_{ij}, (b_i)_i,(c_i)_i$ are given by the RKM.
 %$x'(t) = f(t,x(t)), x(t_n) = x_n$,
The local truncation error ($\LTE$) is the approximation error introduced at a step $n+1$, and it can be defined by $\LTE _{n+1} = x_{n+1} - \tilde{x}(t_{n+1},x_n)$, with $\tilde{x}(t,x_n)$ the exact solution of the problem $\tilde{x}'(t,x_n) = f(t,\tilde{x}(t,x_n)), \tilde{x}(t_n,x_n) = x_n$. The global truncation error ($\GTE$) is the absolute difference between the correct value $\tilde{x}(t_n,x_0)$ and the approximated value $x_n$. An ODE integration method is said to have an order $p$ if $\LTE _n = O(h^{p+1})$ and $\GTE_N = O(h^p)$, where $N$ is the last step.


In the following, we focus on a RKM called Cash-Karp's method \cite{cash1990variable}, while generalization is discussed in \cref{sec:ccl}. Cash-Karp's method has an order 4 and computes six stages. Although two more stages than the classical fourth-order Runge-Kutta method are required, Cash-Karp's method allows us to compute the embedded method that is used in BSS14, in our detectors, and in the adaptive integration scheme. 



\subsection{Embedded methods}
\label{sec:emb}
$\LTE$  can be estimated with \emph{embedded methods}. These methods compute two results $x_n^p$ and $x_n^{q}$ from two RKMs with orders $p$ and $q$ (in general $|q-p| = 1$). The solution is propagated by one of these results, while its stages (and possibly extra stages) are reused to compute the other result in order to achieve a low overhead. In the case of Cash-Karp's method, $p=4$ and $q=5$. If $\LTE ^{q}_n$ has a higher order than does $\LTE ^p$, the difference between $x_n^p$ and $x_n^{q}$ estimates $\LTE ^p$:
\begin{align*}
\mathcal{E}_n &= x_n^p - x_n^{q} \\
&=  x_n^{p} - \tilde{x}(t_n,x_{n-1}) - \left(\tilde{x}(t_n,x_{n-1}) - x_n^{q}\right), \\
&= \LTE ^{p}_n -\LTE ^{q}_n, \\
&= \LTE ^{p}_n + O(h^{q+1}).
\end{align*}



\subsection{Radau's quadrature}

Another way of estimating $\LTE$  is suggested by Stoller and Morrison \cite{stoller1958method} and extended by Ceschino and Kuntzmann \cite{ceschino1966numerical}. Relying on Radau's quadrature and Taylor's expansion,  Ceschino and Kuntzmann give an expression of the $\LTE$  of a method given its order $p\leq 5$. The estimate $\mathcal{R}_n$, called here \textit{Radau's estimate}, does not require the computation of any extra stage , but it checkpoints previous stages and solutions. Therefore, it has a memory overhead, rather than a computational overhead as does the embedded method. 
%For more explanations about it, the interested reader should look at \cite{butcher1987numerical}.  Because these \LTE  estimates are obtained from the Radau quadrature, we will refer to these methods as the \textit{Radau's estimates}. An extension for adaptive methods has been achieved in \cite{butcher1993estimating}.
Since $\mathcal{E}_n$ is a sixth-order estimate, we use the following estimate $\mathcal{R}_n$ presented by Butcher and Johnston \cite{butcher1993estimating}:
\begin{align*}
\mathcal{R}_n &= \frac{h}{10}\left[3f(t_{n-2},x_{n-2}) + 6f(t_{n-1},x_{n-1}) + f(t_{n},x_{n}) \right] \\
&- \frac{1}{30}\left[x_{n-3} + 18x_{n-2} - 9x_{n-1} -  10x_n\right]\\
&= \LTE ^{p}_n + O(h^{p+2}).
\end{align*}




\section{Proposed Hot Rod method}
\label{sec:method}

Our method relies on a surrogate function $\Delta_n$ that is the difference between two estimates: $\Delta_n = \mathcal{A}_n - \mathcal{B}_n$. For Cash-Karp's method, we use the embedded estimate $\mathcal{A}_n = \mathcal{E}_n$ and Radau's estimate $\mathcal{B}_n = \mathcal{R}_n$.  In the absence of SDC, the surrogate function becomes $O(h^{p+2})$: 
\begin{equation*}
\Delta_n = \left(\LTE _n + O(h^{p+2})\right) - \left(\LTE _n + O(h^{p+2})\right) = O(h^{p+2}).
\end{equation*}


In Hot Rod HR, the surrogate function is compared with a certain confidence interval centered over zero. When the surrogate function is outside the confidence interval, an SDC is reported. We show that all significant SDCs are detected. However, Hot Rod HR may have a false positive rate of a few percents. In Hot Rod LFP, we chose a larger confidence interval, and its false positive rate remains below 1 percent.


\subsection{First detector: Hot Rod HR}
\label{sec:frst_det}
% radau and emb are correlated iff no error
In regular cases, our surrogate function is one order higher than that of the $\LTE$. In presence of an SDC, $\Delta_n$ is outside the confidence interval, as shown in the following paragraph. 
Hence, SDCs whose introduced errors are even smaller than the $\LTE$  are expected to be detected. We show that all significant SDCs are detected by Hot Rod HR. 

\subsubsection{Detection of significant SDCs}
% give a proof of it

An SDC is detected when $|\Delta^{c}_n| \geq \mathcal{C}_n$ with $\mathcal{C}_n$ the half-length of the confidence interval at step $n$. It is all the more difficult to detect when $\Delta^{o}_n = 0$.
We show that the minimum injected error $\epsilon_{min}$ that can be detected is of the same order as that of the approximation error.
%We will show that:
%\begin{equation}
% \epsilon_{min} = O(\sigma_n/h)
% \label{eq:eps_min}
%\end{equation}

We study the case of a corrupted stage $k_i$; the case of a corrupted result $x_n$ itself is similar. Here, $k_i^c = \epsilon - k_i^o$, where $^c$ (resp. $^o$) denotes  corrupted (resp. uncorrupted) data:
\begin{align*}
 \Delta^{c}_n - \Delta^{o}_n = \mathcal{E}^{c}_n - \mathcal{E}^{o}_n - \left(\mathcal{R}^{c}_n - \mathcal{R}^{o}_n\right) 
 =  h\epsilon \left[ \hat{b}_i + b_i \left( \frac{1}{30} - \frac{3\delta_{i,1}}{10}  \right) \right], 
\end{align*}
where $\delta_{ij}$ is defined by $\delta_{ij} = 1$ if $i=j$; otherwise $\delta_{ij} = 0$, $(b_i)$ (resp.  $(\hat{b}_i)$) are the coefficients of the order 4 (resp. 5)  in Cash-Karp's method. 

The minimum error $\epsilon_{min}$ that we can detect corresponds to the case $\left|\Delta^{c}_n - \Delta^{o}_n \right| = \mathcal{C}_n - 0$.  We note that $B = \hat{b}_i + b_i \left( \frac{1}{30} - \frac{3\delta_{i,1}}{10} \right)$. This leads to
\begin{align*}
\epsilon_{min} = \frac{\mathcal{C}_n}{hB} = O\left(\frac{\mathcal{C}_n}{h}\right).
\end{align*}
When $x_n$ is corrupted instead of a stage, one can derive that $\epsilon_{min} = O(\mathcal{C}_n)$.
If $\mathcal{C}_n$ has the same order as $\Delta_n$, then (1) $\epsilon_{min} = O(h^{p+1})$ when an error is injected inside a stage and (2) $\epsilon_{min} = O(h^{p+2})$ when an error is injected inside a result. In other words, the threshold of detection has the same order as (or better than) the $\LTE$  of Cash-Karp's method. This guarantees  that all significant SDCs are detected.

\subsubsection{Confidence interval}
%this leads to a first conf int

Because $\Delta_n = O(h^{p+2})$, one can assume that $\Delta_n$ acts as a random variable, with a zero-mean in the absence of SDC. Its standard deviation can be estimated from a training set $T$ composed of $N_s$ samples with the unbiased sample standard deviation
\begin{equation}
 \sigma = \sqrt{\frac{1}{N_s-1} \sum_{n=1}^{N_s}{\Delta_n^2}}.
 \label{eq:sigma}
\end{equation}

Assuming that $(\Delta_n)_n$ follows a normal distribution, the ``three sigma
rule''~\cite{krishnamoorthy2009statistical} suggests  choosing $\mathcal{C}_n = 3\sigma$. Thus, we expect that $99.7\%$ of uncorrupted $(\Delta_n)_n$ fall within the confidence interval, or in other words a false positive rate of $0.3\%$. The normal distribution is a natural choice for modeling the repartition
of training samples. 

Because items from $T$ are not labeled as trusted or untrusted samples, the evaluation of $\sigma$ might be corrupted. It thus would jeopardize the confidence interval and thus the SDC detector. To improve reliability, we weighted each $\Delta_n$ with its own value. Equation~\eqref{eq:sigma} becomes

\begin{align*}
 \Sigma = \sum_{n=1}^{N_s}{\exp{(-\Delta_n^2)}}; \qquad 
 \sigma = \sqrt{\frac{1}{(N_s-1)\Sigma} \sum_{n=1}^{N_s}{\exp{(-\Delta_n^2)}\Delta_n^2}} .
% \label{eq:sigma2}
\end{align*}

\subsubsection{Adaptive control}
\label{sec:adapt-cont}
%When the samples are not independent, however the central limit theorem \cite{fischer2010history} cannot by applied, and this hypothesis is invalidated. 
The hypothesis of a normal distribution may be invalidated. We therefore developed a correction of the confidence interval based on false positives.

When an SDC is reported, the current step is recomputed. If the result has the same value, we can assume that it was a false positive and not an SDC.  
Because of the ``three sigma rule,'' the $\FPR$  
is expected to be $0.3\%$. 
If the $\FPR$  is an order of magnitude higher, at $3\%$, for $k$ times, the confidence
interval is increased with a certain coefficient $1+\alpha$. $\mathcal{C}_n$ becomes $\mathcal{C}_n = (1+\alpha)^k\times 3\sigma$, where
$\alpha$ fixes the rate of the adaptive control. Because $(1+\alpha)^k = 1 + \alpha k + O(\alpha^2)$, $\alpha$ is taken as $1/(\max{(\FPR)}\times N)$, where $N$ is the number of steps in the application and $\max{(\FPR)}$ is the maximum acceptable false positive rate. Because a false positive requires the recomputation of a noncorrupted step, we suggest setting $\max{(\FPR)}$ at $5\%$ to limit the computational overhead. In our experiments, we have $N = 1000$; thus $\alpha = 0.02$.

Thanks to the adaptive control, the training set requires only a few steps. In our experiments, we have found that $N_s = 5$ samples are sufficient to initialize the confidence interval.


\subsection{Second detector: Hot Rod LFP}
\label{sec:scnd_det}
% but we need a second detector: est1
If the cost of a false positive is too high, Hot Rod HR is not suitable. Hence, we designed a second detector with a larger confidence interval. Nonetheless, all significant SDCs must still be detected.

This new confidence interval is defined by $\mathfrak{C}_n = 10C_{99}(|\Delta| \in T)$, with $C_{99}$ the 99th percentile of the training set. The interval can be interpreted as a threshold that is an order of magnitude bigger than the surrogate functions in the training set. 
Because this threshold is higher than the previous one, this detector's recall is lower. Because the estimates are at order $p=4$ for Cash-Karp's method, the $\LTE$  at step $n$ can be expressed as $\LTE _n = Ch^{p+1} + O(h^{p+2})$. 
We show that the $\GTE$ at the last step $N$ is still an order $p$, since it used to be without corruption. We assume the probability that an SDC occurs and is accepted as small enough to guarantee that at most only one SDC will be accepted. The worst case is when this SDC is accepted at the first step, $n=1$, and when $\mathfrak{C}_n = \Delta_n$. Hence, the introduced error is $\LTE _1 = 10Ch^{p+1} + O(h^{p+2})$. Because $\GTE_1 = \LTE _1$, $\GTE_{1} = 10Ch^{p+1} + O(h^{p+2})$.

With $\tilde{x}(t,x_n)$ the notation in \cref{sec:rkm}, $x(t) = \tilde{x}(t,x_0)$, and one can write that the $\GTE$ at a step $0<n< N$ is
\begin{align*}
|\GTE_{n+1}| &= \left|x(t_{n+1})-\tilde{x}(t_{n+1},x_n) + \tilde{x}(t_{n+1},x_n)-x_{n+1} \right|, \\
&\leq \left|x(t_{n+1})-\tilde{x}(t_{n+1},x_n) \right| + \left| x_{n+1}-\tilde{x}(t_{n+1},x_n) \right|. 
\end{align*}

Because $f$ is $L$-Lipschitz continuous, the Gronwall's inequality simplifies the first term to
\begin{equation*}
\left| x(t_{n+1})-\tilde{x}(t_{n+1},x_n) \right| \leq \left| \tilde{x}(t_{n},x_0)-\tilde{x}(t_{n},x_{n-1}) \right| e^{Lh} = \left| \GTE_n \right| e^{Lh}.
\end{equation*}
The second term,  $\left| x_{n+1}-\tilde{x}(t,x_{n+1}) \right|$, is the $\LTE$  at step $n+1$ and so is evaluated at $Ch^{p+1} + O(h^{p+2})$. 
Denoting $\gamma=e^{Lh}$, we obtain
\begin{align*}
\frac{\left| \GTE_{n+1} \right|}{\gamma^{n}} \leq \frac{\left|\GTE_n \right|}{\gamma^{n-1}} + \frac{Ch^{p+1}}{\gamma^{n}}
\leq ... \leq \left|\GTE_1 \right| + Ch^{p+1}\sum_{i=1}^n {\frac{1}{\gamma^i}}.
\end{align*}
Because $\sum_{i=1}^N {1/\gamma^i}=(\gamma^N-1)/\gamma^N(\gamma-1)$ and $\gamma - 1 \geq Lh$,   noting $\tau=Nh$, we obtain
\begin{equation*}
\left| \GTE_{n+1} \right| \leq  10Ch^{p+1} + \frac{Ch^p}{L}\left(e^{L\tau}-1\right) + O(h^{p+2}).
\end{equation*}

At the last step, we have verified that $\GTE_N = O(h^p)$. The order of $\GTE$ is unchanged: the SDC is insignificant.


% finally this is the algorithm
\subsection{Algorithm}
We presented two detectors and showed their efficiency. They differ in their tradeoffs: Hot Rod HR has a higher $\TPR$, and Hot Rod LFP has a lower $\FPR$. We saw that undetected SDCs have no impact on the accuracy of the ODE method. They require fixing the parameter $\alpha$, but simple indications are given.
We can thus derive two scenarios. If an SDC is likely to happen (it could be the case when the processor is not protected from SDC by ECC memory or other protection system), then Hot Rod HR is employed. Otherwise, employing Hot Rod LFP allows us to detect all significant SDCs with fewer false positives.
The schema is illustrated in \cref{alg:likely-sdc} for a given detector.


\begin{algorithm}[!ht]
  \While{learning} {
  step $\leftarrow$ simulation(prev. step) \;
  $\Delta \leftarrow \left| \mathcal{A}(step, prev. steps) - \mathcal{B}(step, prev. steps) \right|$ \;
  TraininigSet.push($\Delta$) \;
  }
 \While{new step} {
  step $\leftarrow$ simulation(prev. step) \;
  $\Delta \leftarrow \left| \mathcal{A}(step, prev. steps) - \mathcal{B}(step, prev. steps) \right|$ \;
   \If{ (Detector $==$ \emph{Hot Rod HR} and $\Delta \leq C_n$) or (Detector $==$ \emph{Hot Rod LFP} and $\Delta \leq \mathfrak{C}_n$)} {
	report(``no error'') \;
	accept step \;
  		}
  \Else{
    		step $\leftarrow$ simulation(prev. step) \;
  		$\Delta' \leftarrow \left| \mathcal{A}(step, prev. steps) - \mathcal{B}(step, prev. steps) \right|$ \;

    		\If{$\Delta' = \Delta$} 
    		% (Detector $==$ \emph{Hot Rod HR} and $\Delta' \leq C_n$) or (Detector $==$ \emph{Hot Rod LFP} and $\Delta' \leq \mathfrak{C}_n$) {
	     {	report(``false positive'') \;
			\If{$\FPR > 3\%$}{k++ \;}
	    	  }
%	    \Else{
%			report(``systematic SDC'') \;
%	    }
		accept step \;
  	}
 }
	\caption{Pseudocode for the execution of our detectors}
	\label{alg:likely-sdc}
\end{algorithm}



\section{Experiments and results}
\label{sec:xp}
We have shown theoretically that all significant SDCs are detected with Hot Rod. In this section, we evaluate the SDC detectors with a meteorology application. 


% presentation + hypotheses that no error occurs during recovery
\subsection{Environment}

Experiments were computed on a 
machine with four Intel Xeon E5620 CPUs (each with 4 cores and 8 threads), 12 GB RAM, and one NVIDIA Kepler K40 GPU
with 12 GB memory. It was programmed in C++11 using 
CUDA. The application is particle tracing for streamline flow
visualization~\cite{peterka_ipdps11},\cite{GuoHPSCH16}, \cite{mcloughlin09}. The solver integrates a velocity field to compute the streamline. It stops when the streamline goes outside the velocity field. Uncorrected streamlines can thus be shorter than they were supposed to be.%The result can be seen in \cref{fig:sl}.

\begin{figure*}[htb!]
\centering
			\includegraphics[width=.59\textwidth]{../img/flux.png}
		\centering
			\caption{Streamlines computed by the application. The color gradient starts in red at seeds; 1,408 streamlines are computed.}
			\label{fig:sl}
\end{figure*}



\subsection{SDC injection methodology}
\label{sec:methodo}

An SDC can arise from many sources in hardware and software~\cite{bairavasundaram2007analysis}, \cite{hwang2012cosmic}, and these sources may change with new versions and generations of hardware and
software. 
We do not attempt to evaluate exhaustively the coverage of our approach because of space limitations.
SDCs are simulated by flipping bits in data items. SDCs affect one or several
bits in the same data item, called respectively singlebit and multibit corruption.
We experimented with both cases. In multibit
corruption, we chose the number of bit-flips $Nflips$ from a uniform distribution. Other
distributions such as normal and beta distributions were tested with several different parameters, but the
results were not significantly different from those reported below.
Corruption can affect data items in any stage (or even directly in the result). The position of a bit-flip is drawn from a uniform distribution. In multibit corruption, we have forced the $Nflips$ bit-flips to be applied on $Nflips$ different positions.
Some SDC have no impact on the results. In a third scenario, we inject only significant singlebit corruptions. We considered that an SDC is significant when the difference between the corrupted result and the safe result is higher than the mean $\LTE$. 

\subsection{Benchmark}
We compared our approach with similar methods presented in \cref{sec:intro}: replication, AID and BSS14 detectors. Those methods need to be parametrized. We  compared results with a set of parameters and selected the parameters that provide the best results in our application. Using the same notation as in \cite{di2016aid}, we configure AID with $\theta r = 1$. Results were improved if the confidence interval is taken as $(1+\alpha)^k(\epsilon+\theta r)$ with $\alpha = 0.2$ and $k$ defined in \cref{sec:adapt-cont}.
Concerning BSS14, five parameters should be set, but no indication is detailed  in \cite{benson2014silent} about two of them.  With the notation of \cite{benson2014silent}, the considered values are $\tau_j = 1e^{-5}$, $\tau_v = 0.02$, $\Gamma = 1.4$, $\gamma = 0.95$, and $p=10$. %Finally, we combined the idea of taking as a surrogate function the embedded estimate as in \emph{BSS14}, the adaptive extrapolation as in AID and the confidence interval presented in \cref{sec:scnd_det} to create a new detector called \emph{Embedded 3}, which has no parameter to tune.


\subsection{Results}
\label{sec:res}
%
%	

{\renewcommand{\arraystretch}{1.3}%
\begin{table}[!ht]
\footnotesize
\centering
  \caption{Benchmark of our detectors Hot Rod (H.R.) LFP and HR, replication, AID and BSS14. Values in the column ``IRE 95\%'' are the injected relative errors (IRE) that were detected $95\%$ of the time.  }
\begin{tabular}{ |r | c |c|c|c|c|c|c| }
		%\fontsize{8}{14}\selectfont
		\cline{1-8}
  \multirow{2}{*}{Detector} & \multicolumn{3}{c|}{$\TPR$ (\%)} & \multirow{2}{*}{$\FPR$ (\%)} & \multirow{2}{*}{IRE $95\%$} & \multicolumn{2}{c|}{Overheads (\%)}  \\ \cline{2-4} \cline{7-8}
%
     & Singlebit & Multibit & Significant & & & Comp. & Memory\\
     \hline
  Replication & $100.0$ & $100.0$ & $100.0$ & $100.0$ & $0.0$ & $+100$ & $+100 $ \\
  \hline
  AID & $14.3$ &  $43.2$ &  $86.7$ & $1.6$ & $7e^{-6}$ & $+4.6$ & $+50$ \\
  \hline
  BSS14 & $ 18.8$ & $49.5$  & $91.2$ & $0.6$ & $4e^{-6}$ & $+3.7$ & $+13$\\
  \hline
  H.R. LFP & $23.1$  & $64.6$ & $99.9$ &  $0.01$ & $7e^{-8}$ & $+3.8$ & $+50$ \\
  \hline
  H.R. HR & $28.6$ & $69.6$ & $99.9$ & $1.2$ & $5e^{-9}$ & $+4.4$ & $+50$ \\
	\cline{1-8}
\end{tabular}
 \label{table:res}
 \end{table}
 } \quad
 

 
\Cref{table:res} presents results from our benchmark. We did not compare each detector with a solver with no detector. We compared each detector with a perfect detector that returns the ground truth. For computational overhead, we  divided the execution time of each detector with that of the perfect detector. Our detectors have a computational overhead lower than $5\%$, as do the BSS14 and AID detectors. It is $20$ times less computationally expensive than replication. But unlike the AID detector, our detectors have to employ an embedded integration method that computes more stages than does another Runge-Kutta method of the same order. 

Our detectors have a higher memory cost than does the BSS14 detector, but a smaller memory cost than does replication.
For estimating memory overheads, we counted the number of stored vectors, such as solutions $(x_n)_n$, stage slopes $(k_i)_i$ and estimates.
Cash-Karp's method requires computing and storing two additional stage slopes than does Runge-Kutta 4, but the same number as the other embedded fourth-order methods.
Cash-Karp's method requires  storing 6 $(k_i)_i$ (among them $f(x_{n-1})$), and $x_n$; $x_{n-1}$ is stored to allow a rollback in case of SDC detection;  when $f(x_{n-1})$ is employed in the Radau estimation, $f(x_n)$ can be computed at the position (the result is employed at the next step if the step is accepted). Thus in total, 8 data elements are stored by the perfect detector, whereas $\mathcal{E}$ ($\mathcal{R}$ can use the same storage as $\mathcal{E}$), $f(x_{n-2}), x_{n-2}$ and $x_{n-3}$ are stored for our detectors; AID stores $x_{n-2}$, $x_{n-3}$, $x_{n-4}$, and the extrapolated solution; and BSS14 stores $\mathcal{E}$.  
%AID suggests to sample stored solutions for reducing the memory overhead. We did not sample solutions in our experiments, because it might decrease the detection accuracy.

The true positive rate ($\TPR$) shows that our detectors detect perfectly (at $99.9\%$) significant SDCs. Replication does as well, but the BSS14 and AID detectors have a $\TPR$ of $91.2\%$ and $86.7\%$ of significant SDCs, respectively. For BSS14 and AID, some SDCs can  thus be undetected while affecting the accuracy of the solvers.
Moreover, the ``IRE 95\%'' value of our detectors is smaller than the mean local error estimate ($1.5e^{-6}$) by a factor of 100. Because all significant SDCs are detected,  SDCs undetected by Hot Rod are sure to have no impact. The undetected $76.9\%$ of SDCs by Hot Rod LFP are thus insignificant and do not need to be corrected: correcting these insignificant SDCs would not improve results and would demand extra computation. 
\Cref{fig:solutions} shows the $\LTE$  of the solver in the confidence interval in the absence of SDC. It represents the approximation error. As defined in \cref{sec:intro}, significant SDCs inject errors that are higher than this error. Because the streamlines of the AID and BSS14 detectors are pushed outside the confidence interval at SDC injections, they do not detect those SDCs. On the other hand, Hot Rod HR and LFP's streamlines are not affected by SDCs: these detectors protected the solver. This result is consistent with the fact that the IRE $95\%$ of Hot Rod is two orders of magnitudes less than the approximation error. 




\begin{figure*}[!ht]
		\centering
			\includegraphics[width=.70\textwidth]{../img/benchmark.eps}
			\caption{One streamline computed by the different detectors.  Singlebit injection is made every 50 steps. In the window, the position of the bit-flip  varies from 31 to 35 in  IEEE754 doubleprecision. The interval ``$\pm\LTE$ '' represents the approximation error. Significant SDCs shift the solution outside this interval. In the application, the origin is the center of the Earth. }
			\label{fig:solutions}
	\end{figure*}


\section{Conclusion}
\label{sec:ccl}


This study presented our SDC detection method Hot Rod for ODE integration solvers. Both experimental and theoretical results show that all significant SDCs are detected. Except for replication, no other tested SDC detectors achieve these results. More specifically, compared with the algorithmic detection SDC detectors, the true positive rate is improved by $52\%$ for singlebit corruptions; whereas compared with replication, the computational overhead is reduced by $20$ times. Moreover, users need only to fix the maximum false positive rate, as explained in \cref{sec:method}.


Our detectors were employed for one of the ODE integration methods.  Other embedded Runge-Kutta methods can be directly employed. Radau's estimates have a general expression in the case of adaptive step size; see the work of Butcher and Johnston \cite{butcher1993estimating}. For implicit methods or linear multisteps, Richardson's estimates can also be used.  In future work, we plan to investigate detection in partial differential equation solvers.



%=============================================================================
\subsubsection*{Acknowledgments}

We express our gratitude to Julie Bessac for assistance with the algorithm and Gail Pieper for comments that greatly improved the manuscript. We also gratefully acknowledge the use of the services and facilities of the Decaf project at Argonne National Laboratory, supported by U.S. Department of Energy, Office of Science, Advanced Scientific Computing Research, under Contract DE-AC02-06CH11357, program manager Lucy Nowell. We also thank the anonymous reviewers for their helpful comments.

%=============================================================================
%%=============================================================================
\ifstandalone
	\bibliographystyle{splncs03}
	\bibliography{\rootPath Annexes/biblio.bib}
\fi
%%=============================================================================
%%=============================================================================

%The submitted manuscript has been created by UChicago Argonne, LLC, Operator of Argonne National Laboratory (``Argonne'').  Argonne, a U.S. Department of Energy Office of Science laboratory, is operated under Contract No. DE-AC02-06CH11357.  The U.S. Government retains for itself, and others acting on its behalf, a paid-up nonexclusive, irrevocable worldwide license in said article to reproduce, prepare derivative works, distribute copies to the public, and perform publicly and display publicly, by or on behalf of the Government.

\end{document}
