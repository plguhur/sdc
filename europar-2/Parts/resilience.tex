\documentclass[conference]{IEEEtran}
\newcommand*{\rootPath}{../}
\renewcommand{\arraystretch}{1.3}
\input{\rootPath Annexes/Packages.tex}

\standalonetrue

\begin{document}


%-----------------------------------------------------------------------------------------
\section{Resilience of Adaptive Controllers}
\label{sec:adaptive}

Chen et al. \cite{chen2013comprehensive} remarked that solvers have an inherent resilience by computing an error estimate from the Richardson extrapolation. In the following, we extend this point to all adaptive solvers. %Experimentally, we observe that the adaptive controller rejects some steps where the error estimate exceeds a certain threshold due to an SDC.

However, this assumes that the adaptive controller is not corrupted in the presence of an SDC. This assumption is not verified, because the error estimation used by the adaptive controller is computed from corrupted results. In Section \ref{sec:issue}, we observe that the error estimate can be shifted under the threshold of the adaptive controller.

\subsection{Inherent Resilience}
\label{sec:intrinsic}

\subsubsection{Not all SDCs have an impact on the results}
Numerical integration solvers have an inherent approximation error depending on the integration method and its order $p$: the LTE is $O(h^{p+1})$.
When the lowest-order bit is flipped, the impact is insignificant with respect to the approximation error, whereas other SDC can drastically increase the error, or they may even cause the solver to diverge.%, and this SDC does not affect the accuracy of the results.
Basically, we call an \emph{insignificant} SDC any SDC that does not affect the user's expectation in accuracy, whereas other SDCs that drift the scaled error above $1.0$  are referred to as \emph{significant}.
% In the case of an adaptive solver, the user explicitly states the maximum acceptable approximation error with  the tolerances $Tol_A$ and $Tol_R$.
% When the scaled error was drifted above $1.0$, the corruption is considered significant.


\subsubsection{Rejection of corrupted steps}
\label{sec:rejection}
When an SDC occurs and the approximation error is shifted outside the tolerance because of the SDC, the step is naturally rejected. In this case, the step size is reduced according to Section \ref{sec:system}; then the next noncorrupted step observes that the error estimate is too small and increases the step size. Overall, the computation time is  increased just during one step, while the accuracy is preserved.

The corrupted step is not rejected when the SDC shifts the approximation error below the tolerance or when the SDC is small enough to avoid the approximation error to exceed the tolerance. %Accepting such steps seems dangerous, however.
One could object that the approximation error can be higher than it would have been without the SDC; even if the current step is below the tolerance, it might affect next steps. However, an adaptive solver is designed in a way that if all steps are below the tolerances, then the expected accuracy is achieved. Accepting such corrupted steps might increase the approximation error on the next steps, but the expected accuracy will be achieved.
%Detecting and correcting such SDCs would thus be a waste of ressources.
 %The approximation error is only estimated.


\subsection{Significant SDCs Not Detected}
\label{sec:issue}


One caveat must be added. In the presence of a corruption, the error estimate is  corrupted, and can underestimate the real value of the approximation error. In particular, it may be shifted below the tolerances of the adaptive controller; in such a case, the step would be accepted. We give two examples.

\begin{itemize}
  \item In the extreme case, the registers of $(K_i)_{i\geq 0}$ could be erased, and thus, the corrupted error estimate is equal to zero; the step is then accepted, and the step size is increased by $\alpha_{max}$. %The solution would be the same as during last step: $x_{n}=x_{n-1}$. The approximation error could then be unacceptable with respect to the user's requirements.
  \item Because any $K_i$ depends on other $(K_j)_{j\ne i}$, the corruption of a certain $K_l$ corrupts the other $(L_j)_{j\ne l}$. Such cascading patterns increase the possibility of underestimating the approximation error.
  % \item The SDCs can affect only the estimate. In this case, the estimate can be completely decorrelated from the approximation error.
\end{itemize}
Consequences of accepting a corrupted step can be disastrous. Not only would the corrupted step exceed the user's accuracy expectation, but the next steps will be initialized with a corrupted result. Moreover, the step size might be increased after the corrupted step, and %it might even exceed its stability region; in such a case,
the solution may not converge at all.

\subsection{Experiments}
\label{sec:issue}

We injected SDCs in the use case introduced in Section \ref{sec:problem}.
In Table~\ref{tab:detectivity}, we show the detection performances of the adaptive controller.
Detection performances are based on the false positive rate and the true positive rate. The false positive rate is defined as the ratio between the number of noncorrupted steps that are rejected and the number of noncorrupted steps. Similarly, the true positive rate is the ratio between the number of corrupted and rejected steps and the number of corrupted steps.
Because not all SDCs have an impact on the accuracy of the results, we distinguish the case where a step is corrupted by any kind of SDC and steps corrupted by at least one significant SDCs.

The false positive rate remains below $0.1\%$ for all considered ODE methods, thanks to the choices of $\alpha$,
 $\alpha_{\max}$, $\alpha_{\min}$ and the low probability of injecting SDCs. At the same time, the true positive rate is usually below $50\%$. Singlebit SDCs are the hardest SDCs to detect ($9.3\%$), whereas the multibit SDCs are the easiest ($55.1\%$). The reason is that singlebit SDCs usually have  a lower impact on the results, as explained in Section \ref{sec:problem}. %The true positive rate decreases with the ODE methods. The ODE methods differ in their number $N_k$ of the function evaluations $(K_i)_i$ ($N_k = 7$ for the Dormand-Prince method, $N_k = 4$ for the Bogacki-Shampine method, and $N_k = 2$ for the Heun-Euler method).




 The true positive rate with all steps is higher than the  true positive rate with only significant SDCs, because insignificant SDCs can have  an impact too low on the results to be detectable.


 While the true positive rate with significant SDCs achieves $86.7\%$ for Heun-Euler's method with scaled SDCs, the rate increases dramatically to $49.6\%$ for Dormand-Prince's method. This comes from the number $N_k$ of function evaluations $(K_i)_i$ that is higher for Dormand-Prince's method than for Heun-Euler's method. In this case, more patterns of SDCs can lead to an underevaluation of the error estimation, and the probability of a nondetection is thus higher.
 While the false negative rate with all steps is higher with singlebit SDCs than with scaled SDCs, the significant false negative rate is lower with singlebit SDCs than with scaled SDCs. Indeed, a singlebit SDC becomes significant when one of the highest-order bits is flipped, and this is easily detectable, whereas a scaled SDC can be significant while being difficult to detect.






 %We can not currently explain this phenomenon.

% The true positive rate may seem low, but only significant SDCs need to be rejected. Further experiments must thus distinguish significant from insignificant SDCs in order to know whether the inherent resilience of adaptive solvers is reliable enough.


%     \hline
%   Rate & Injector & Heun-Euler & Bogacki-Shampine & Dormand-Prince \\
%   \hline
%   \hline
%   FP & All & $0.0$ & $0.0$ & $0.0$ \\
%   TP & Scaled & $31.1$ & $23.3$ & $20.1$ \\
%   TP & Multibit  & $55.1$ & $46.8$ & $35.3$   \\
%   TP & Singlebit  & $13.2$ & $11.8$ & $9.3$ \\
%   FN & Significant & $5.4$ & $10.1$ & $90.7$ & $15.0$ \\
%   \hline
% \end{tabular}
%   \caption{Detection accuracy of several ODE methods and several SDC injectors.  FP: false positive. TP: true positive. Results are given in percentage.}
%   \label{tab:detectivity}
% \end{table}

\begin{table}[h]
\footnotesize
  \centering
\begin{tabular}{*{8}{|c}|}
    \hline
Rate        &       FP  & \multicolumn{6}{c|}{TP} \\
    \hline
Which SDCs &       All&All&All&All&Significant&Significant&Significant \\
    \hline
Injector  &         Any & Scaled & Multibit & Singlebit & Scaled & Multibit & Singlebit \\
    \hline
Heun-Euler       & $0.0$ & $31.1$ & $55.1$ & $13.2$& $86.7$ & $94.6$ & $96.1$ \\
    \hline
Bogacki-Shampine & $0.0$ & $23.3$ & $46.8$ & $46.8$& $63.9$ & $89.9$ & $95.5$ \\
    \hline
Dormand-Prince   & $0.0$ & $20.1$ & $35.3$ & $9.3$ & $49.6$ & $85.0$ & $92.1$ \\
    \hline
\end{tabular}
\caption{Detection accuracy of several ODE methods and several SDC injectors.  FP: false positive. TP: true positive. Results are given in percentage.}
\label{tab:detectivity}
\end{table}
% \begin{tabular}{| l | c | c |c | r |}
%     \hline
%   Rate & Injector & Heun-Euler & Bogacki-Shampine & Dormand-Prince \\
%   \hline
%   \hline
%   FP & All & $0.0$ & $0.0$ & $0.0$ \\
%   TP & Scaled & $31.1$ & $23.3$ & $20.1$ \\
%   TP & Multibit  & $55.1$ & $46.8$ & $35.3$   \\
%   TP & Singlebit  & $13.2$ & $46.8$ & $9.3$ \\
%   FN & Significant & $5.4$ & $10.1$ & $90.7$ & $15.0$ \\
%   \hline



\vspace{-1cm}






\end{document}
