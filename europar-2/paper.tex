\documentclass[runningheads,a4paper]{llncs}
\newcommand*{\rootPath}{./}

\input{\rootPath Annexes/Packages.tex}
%\newcolumntype{C}[1]{>{\centering}m{#1}}
%\newcolumntype{C}[1]{>{\centering\raggedright\arraybackslash}m{#1}}
\newcolumntype{L}[1]{>{\raggedright\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}
\newcolumntype{C}[1]{>{\centering\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}
\newcolumntype{R}[1]{>{\raggedleft\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}

\standalonetrue

\title{Detection of Silent Data Corruption in Adaptive Numerical Integration Solvers}
\titlerunning{Detection of Silent Data Corruption}

\author{Pierre-Louis Guhur,  \inst{1,2}
        Emil Constantinescu,  \inst{1}
        Debojyoti Ghosh, \inst{3}
        Tom Peterka,  \inst{1}
        and   Franck Cappello\inst{1}}
\authorrunning{Guhur et al.}
\institute{Argonne National Laboratory (USA)\\
\email{\{emconsta, tpeterka, cappello\}@mcs.anl.gov}
\and ENS Paris-Saclay (France)\\
\email{pierre-louis.guhur@ens-paris-saclay.fr}
\and Lawrence Livermore National Laboratory (USA)\\
\email{ghosh5@llnl.gov}
}

\begin{document}





\maketitle

\begin{abstract}
Scientific computing requires trust in results. In high-performance computing, trust is impeded by silent data corruption (SDC), in other words corruption that remains unnoticed.
Numerical integration solvers are especially sensitive to SDCs because an SDC introduced in a certain step affects all the following steps. SDCs can even cause the solver to become unstable.
Adaptive solvers can change the step size, by comparing an estimation of the approximation error to a user-defined tolerance. If the estimation exceeds the tolerance, the step is rejected and recomputed. Adaptive solvers have an inherent resilience, because 1) some SDCs might have no consequences on the accuracy of the results, and  2) some SDCs might push the approximation error beyond the tolerance. Our first contribution shows that the rejection mechanism is not reliable enough to reject all SDCs that affect the results accuracy, because the estimation is also corrupted.
We therefore provide another protection mechanism: at the end of each step, a second error estimation is employed to increase the redundancy. Because of the complex dynamics, the choice of the second estimate is difficult: two different methods are explored.
We evaluated them in HyPar and PETSc, on a cluster of 4096 cores. We injected SDCs that are large enough to affect the trust or the convergence of the solvers.
The new approach can detect 99\% of the SDCs, reducing by more than 10 times the number of undetected SDCs.
Compared with replication, a classic SDC detector,  our protection mechanism reduces the memory overhead by more than 2 times and the computational overhead by more than 20 times in our experiments.
\end{abstract}


\keywords{
  high-performance computing, resilience, fault tolerance, silent data corruption, numerical integration solver
}



\input{\rootPath Parts/introduction}
\input{\rootPath Parts/background}
\input{\rootPath Parts/problem}
\input{\rootPath Parts/resilience}
\input{\rootPath Parts/method}
\input{\rootPath Parts/xp}
% \input{\rootPath Parts/related-work}



\section{Conclusion}
\label{sec:ccl}

In this study, we showed that two kinds of SDCs can occur in a numerical integration solver. Some, called \emph{significant}, can exceed the user-defined tolerance of the approximation error. They may even hinder the convergence of a solver. Others, called \emph{insignificant}, have no impact on the results with respect to the intrinsic approximation error of a solver.
%
% Some solvers have an adaptive controller that controls the step size from an estimation of the approximation error.
We showed that the rejection mechanism of this adaptive controller can correct some SDCs by rejecting corrupted steps. But an important fraction of significant SDCs are not rejected, because the rejection mechanism is corrupted itself in the presence of an SDC.

Our solution consists in double-checking the acceptance of a step. Two strategies are proposed: LIP-based double-checking and integration-based double-checking.
%The detector developed Subasi et al. \cite{subasi2016spatial} using support vectors. It reduces the memory overhead of AID. Such a method could be employed also in the double-checking methods to mitigate their memory overheads.

% The first strategy, called LIP-based double-checking, is derived from AID, a state-of-the-art SDC detector.
% An error estimate is obtained from the difference between the result of the solver and a prediction obtained by Lagrange interpolation polynomials. When the error estimate is higher than a certain threshold function, the step is rejected.
% The second strategy, called integration-based double-checking, computes an error estimate from the difference between two results:  one from the ODE method used by the solver and one from another ODE method.  This latter ODE method is a backward differentiation formula, which will usually get a larger stability area than with the ODE method of the solver. An algorithm is given to automatically select the best order of this family of ODE methods.
% With respect to the LIP-based double-checking, the integration-based double-checking is more difficult to apply for high-order methods, but the error estimate is closer to the error estimate of the adaptive controller.
The second strategy is more difficult to apply for high-order methods than the first strategy, but its detection accuracy  is higher. It reduces by a factor of $10$ the number of false negatives of the adaptive controller in our experiments. At the same time, the overheads are smaller than with replication, by a factor of 10 in computation and by a factor of 2 in memory in average.
% In our experiments, we suggested  further improvements to reduce those overheads.
%In Di and Cappello. \cite{di2016aid}, vectors are sampled by considering only a component over three of a vector.
% We plan also to explore the use of the double-checking mechanism for implicit solvers.

%For those methods, one could also consider employing physical constraints on the step-size to rejectsuch that CFL etc.

%A consequence of this study is to promote adaptive controller for implicit solvers. The physical constraints may limit sthe space of step sizes, such that the memory and computational overheads of the adaptive controller are not compenste for; though, the resilience improvements avoid to recompute the whole solver in the case of SDCs.

%We plan to explore the use of error estimations for resilience in other families of algorithms such as adaptive mesh refinement.

\section*{Acknowledgment}
\addcontentsline{toc}{section}{Acknowledgment}
This material is based upon work supported by the U.S. Department of Energy,
Office of Science, Advanced Scientific Computing Research, under contract DE-AC02-06CH11357.
Part of this work was performed under the auspices of the U.S. Department of Energy by
Lawrence Livermore National Laboratory under contract DE-AC52-07NA27344.
%
We gratefully acknowledge the computing resources provided on Blues, a high-performance
computing cluster operated by the Laboratory
Computing Resource Center at Argonne National Laboratory.




%%=============================================================================
%%=============================================================================
\ifstandalone
	\bibliographystyle{IEEEtran}
	\bibliography{\rootPath Annexes/biblio.bib}
\fi
%%=============================================================================
%%=============================================================================



The submitted manuscript has been created by UChicago Argonne, LLC, Operator of Argonne National Laboratory (“Argonne”). Argonne, a U.S. Department of Energy Office of Science laboratory, is operated under Contract No. DE-AC02-06CH11357. The U.S. Government retains for itself, and others acting on its behalf, a paid-up nonexclusive, irrevocable worldwide license in said article to reproduce, prepare derivative works, distribute copies to the public, and perform publicly and display publicly, by or on behalf of the Government.  The Department of Energy will provide public access to these results of federally sponsored research in accordance with the DOE Public Access Plan. http://energy.gov/downloads/doe-public-access-plan.


\end{document}
