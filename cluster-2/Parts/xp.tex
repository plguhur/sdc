\documentclass[conference]{IEEEtran}
\newcommand*{\rootPath}{../}
\input{\rootPath Annexes/Packages.tex}

\standalonetrue

\begin{document}

%%=============================================================================

\section{Experiments}
\label{sec:xp}

In Section \ref{sec:adaptive}, we showed that the rejection mechanism of an adaptive solver  is  able to correct only a part of the SDCs. Some SDCs, although significant, remained in the solution because the rejection mechanism was corrupted and did not detect any outlier. Therefore, in Section \ref{sec:method}, we proposed a method that enhances the rejection mechanism by double-checking the acceptance of a step.

In this section, we experimentally validate our method with the use case introduced in Section \ref{sec:problem}.  First, we show that our method greatly reduces the risk of accepting a significant SDC.  Secondly, we measure the overheads and the scalability of our double-checkings, in order to compare them with replication and to suggest improvements.


\subsection{Detection accuracy}
\label{sec:detc}


\renewcommand{\arraystretch}{1.3}%

\begin{table}[h]
    \caption{Our double-checking based on Lagrange interpolation polynomials (LBDC) and on a numerical integration method (IBDC) compared with the expensive state-of-the-art replication and the classic adaptive controller without our enhancement (Classic). FPR = false positive rate. TPR = true positive rate. FNR = false negative rate.}
\footnotesize
  \centering
\begin{tabular}{| r | c|c|c  |}
    \hline
 % & \multicolumn{3}{c|}{Rates (\%)} & \multicolumn{2}{c|}{Overheads (\%)} \\
 % \cline{2-6}
  & FPR & TPR & Significant FNR \\%& Memory & Computational \\
  \hline
  Classic & $0.0$ & $31.1$ & $13.3$ \\%& $+0.0$ & $+0.0$ \\
  \hline
  LBDC & $2.3$ & $33.1$ & $4.1$ \\%& $+57.6$ & $+2.4$ \\
  \hline
  IBDC & $4.2$ & $41.9$ & $1.1$ \\%& $+42.7$ & $+4.5$\\
  \hline
  Replication & $0.0$ & $100.0$ & $0.0$ \\%& $+100.0$ & $+100$ \\
  \hline
\end{tabular}

  \label{tab:detectivity-dchk}
\end{table}


We applied the integration-based double-checking and the LIP-based double-checking to the Heun-Euler method.  Table~\ref{tab:detectivity-dchk} compares their detection performances with replication and the classic adaptive controller. Details on how we defined the performances are given in Section \ref{sec:adaptive}.
%We consider that replication has a false positive rate of $100\%$, because all steps are recomputed, at least once.

%and the overheads in computation and in memory of the double-checkings, replication and the classic adaptive controller. The overheads are obtained when the computation is done with one core. Almost all significant false negatives were detected. However, the false positive rate achieves $4.2\%$ and additional memory is required to store previous solutions. For high dimension problems, the storage of solutions occupies the largest part of the memory.  The overhead in memory is $+42.7\%$ and in computation is $+4.5\%$.

LIP-based double-checking reduces the rate of significant false negatives by a factor of $3$, whereas integration-based double-checking decreases the rate by a factor of $10$. This difference in accuracy results from the fact that the error estimate used by integration-based double-checking is more precise than that used by the LIP-based double-checking. One might suggest  tightening the threshold function of the LIP-based double-checking in order to improve the detection accuracy. This sounds reasonable, because tightening the threshold function increases the false positive rate and the false positive rate of the LIP-based double-checking is lower than the false positive rate of the integration-based double-checking. Furthermore, the threshold function can initially  be tightened with a smaller error bound $\theta_1 < \theta$. However, results would hardly change. Indeed,  at the beginning of the simulation, the LIP-based double-checking would detect many false positives, until $\eta$, the number of false positives, increases at the point that the threshold function presents a similar size as previously.





\subsection{Overheads}
\label{sec:overheads}

SDCs impact the convergence rate. For example, by shifting to the error estimate to an high value, the next step size is reduced and the computation takes more time. We measure the computation time ratio (defined as the computational overhead)  between our method with injected errors and the classic adaptive controller without injected errors to confirm that the convergence rate does not burst. Table \ref{tab:overheads-dchk} presents also the memory overhead, due to the store of previous step size in the double-check mechanism.

\begin{table}[h]
    \caption{Benchmark of overheads between our double-checking based on Lagrange interpolation polynomials (LBDC) and on a numerical integration method (IDBC), replication, and the classic adaptive controller (Classic). }
\footnotesize
  \centering
\begin{tabular}{| r | c|c |}
    \hline
 % & \multicolumn{3}{c|}{Rates (\%)} & \multicolumn{2}{c|}{Overheads (\%)} \\
	Overheads  & Memory (\%) & Computation (\%) \\
  \hline
  Classic &  $+0.0$ & $+0.0$ \\
  \hline
  LBDC &  $+57.6$ & $+2.4$ \\
  \hline
  IBDC &  $+42.7$ & $+4.5$\\
  \hline
  Replication &  $+100$ & $+100$ \\
  \hline
\end{tabular}

  \label{tab:overheads-dchk}
\end{table}


	%$, we compare the overheads in memory and in computation of the classic adaptive controller, our methods, and 
	As an indication, we added the overheads of replication: the computational overhead of replication is exactly $+100\%$ plus the rate of corrected steps, but the rate of corrected steps is below $1\%$, and thus the overhead is equal to  $+100$.

	The computational overhead is partly due to the cost of the double-checking and to false positives, since false positives require  recomputing a noncorrupted step. For integration-based double-checking, the false positive rate is $4.2\%$, while the computational overhead is $+4.5\%$. Therefore, the computational cost of our method is due mainly  to the cost of recomputing a false positive. To a certain extent, the computational overhead can be reduced by decreasing the parameters $\gamma$ and $\Gamma$, although doing so would also decrease the detection accuracy.
The memory overhead can appear important but is on average two times lower than the memory overhead of replication.
It decreases with the complexity of the ODE method of the solver: in general, the solver requires $N_k + 2$ vectors of data with $N_k$ the number of function evaluations, whereas  double-checking requires a fixed number of vectors.
Using support vectors machine by Subasi et al. \cite{subasi2016spatial} to reduce the memory overhead on another SDC detector, AID. Such a method could be employed also in the double-checking methods to mitigate their memory overheads.



\subsection{Scalability}
\label{sec:scal}



\begin{table}[h]
    \caption{Details of the mean execution time computation for the classic adaptive controller (Class.), LIP-based double-checking (LBDC), and integration-based double-checking (IBDC). Results are given in seconds.}
\footnotesize
  \centering
\begin{tabular}{| l |  c|c|c | c|c|c | c|c|c|}
    \hline
  Cores        &    \multicolumn{3}{c|}{512} & \multicolumn{3}{c|}{4096}   \\
    \hline
     Protection  & Class. & LBDC & IBDC & Class. &  LBDC & IBDC \\
 %   \hline
 % Memory (B) &  $1.8e^5$ & $2.8e^5$ & $2.4e^5$ & $5.6e^4$ & $8.9e^4$ & $6.8e^4$ \\
  \hline
 Double-check & -  & $3.8e^2$ & $3.9e^2$ & $-$ & $1.5e^1$  &  $1.6e^1$\\
%  \hline
%  Adapt. (s) & $1.5e^2$ & $1.5e^2$ & $5.1e^2$ & $1.4e^2$ & $1.4e^2$ &  $2.4e^2$ \\
  \hline
  Step & $1.2e^3$ & $1.3e^3$ & $1.3e^3$ &    $4.6e^2$ & $4.8e^2$ & $4.8e^2$ \\
  \hline
\end{tabular}

  \label{tab:details}
\end{table}


Table~\ref{tab:details} shows the mean execution time computation for the double-checking methods and the classic adaptive controller. The computational overheads remain below $5\%$. Double-checking scales similarly to the step itself, mainly because of the collective operation for computing the norms. Moreover, the table shows that double-checking is almost a pure additional cost to the classic adaptive controller, because the time execution of a step with double-checking is almost equal to the addition between the time execution of the step of the classic adaptive controller and double-checking itself. A better implementation must instead better integrate the double-checking inside the adaptive controller. This could be done by computing the norm of the error estimates used by the classic adaptive controller and the double-checking methods at the same time. Doing so requires allocating an additional vector and thus increasing the memory overheads.


\begin{figure}[!htb]
  \centering
         \includegraphics[width=0.48\textwidth]{\rootPath ../img/curve-time-mem.png}
    \caption{Relative performance in time and in memory of LIP-based double-checking (LBDC) and integration-based double-checking (IBDC) compared with the classic adaptive controller until 4096 cores.}
  \label{fig:scaling}
\end{figure}

Figure \ref{fig:scaling} shows the relative performance in time (yellow) and memory (green) compared to the classical adaptive controller of the LIP-based double-checking (square) and the integration-based double-checking (circle) up to 4096 cores.
The integration-based double-checking shows better performances than does LIP-based double-checking in memory, time execution, and detection accuracy. The reason is that the integration-based double-checking is based on mathematical properties of solvers and is more specific than Lagrange interpolation polynomials.
The relative performance is computed as the difference between the performance of the double-checking and the classic adaptive controller, divided by the performance of the classic adaptive controller.    The overheads tend to decrease with the number of cores, because the SDC detectors provide a better scalability than the rest of HyPar.
Indeed, as the number of cores decreases, parts of HyPar that cannot be parallelized become more and more important with respect to the cost of double-checking.

%For improving the scalability, a possible improvement consists in computing the norm of the error estimate of the double-checking when the norm of the error estimate of the classic adaptive controller is computed. As a drawback, more vectors need to be allocated, and the memory overhead is thus increased.

%  he mean overhead in time is 5\% higher for the integration-based double-checking method than for the LIP-based double-checking, because our method has more false positives than  has. However, the mean memory overhead of SAID is 58\% higher than for the BDF-based double-checking method, because another previous solution is required to be stored.
% Our method requires less computations in term of floating-point operation than SAID. While operations on vector are similar to both cases, the difference is relative to algorithm for selecting the order. For our method, it consists of a simple fraction, whereas SAID computes norms and linear combination of solutions.
% Finally, the computation rate of SAID is significantly better than our method because... ??


\end{document}
