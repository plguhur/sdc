\documentclass[conference]{IEEEtran}
\newcommand*{\rootPath}{../}
\input{\rootPath Annexes/Packages.tex}

\standalonetrue

\begin{document}

%%=============================================================================

% modeling SDC
\section{Detection of Silent Data Corruption}
\label{sec:problem}

We describe here the SDC model and the application we used.

\subsection{Silent Data Corruption Model}

A silent data corruption occurs when a program provides an output that is correctly formatted, but is unexpected. For example, in the case of the Pentium FDIV bug \cite{price1995pentium}, $4195835/3145727$ provided $1.333820449136241002$ instead of $1.333\emph{739068902037589}$: final outcomes were wrong but it might not be detected.
A corruption is more likely to occur in data than in instructions, because  instructions occupy less memory than data do, and corrupted instructions typically result in crashes and not silent corruptions.
Other mechanisms besides SDC detection, such as checkpointing, may be employed  for protecting an execution against instruction corruptions.
We assume here that corruptions affect only data.

An SDC is called \emph{nonsystematic} when it affects a program randomly. Such SDCs typically are triggered when radiation or aging hardware flips one or several bits.  On the contrary, a \emph{systematic} corruption is triggered by a repeatable pattern such as a bug.
The probability of SDCs is low, and it is unlikely that two nonsystematic SDCs occur two times consecutively in the same bits. Therefore, correction can be obtained by recomputing a corrupted step to recover from a nonsystematic SDC.
In this study, we consider only nonsystematic SDCs.



\subsection{Consequences of SDCs in Numerical Integration Solvers}
Numerical integration solvers are particularly sensitive to SDCs: because of the iterative scheme, an SDC affects not only the corrupted step but also the following steps. We illustrate this sensitivity with two examples.
\begin{itemize}
  \item In nonlinear ODEs, the stability region of the ODE method depends on the current step. An SDC can bring the solution outside the stability region. For example, in the equation $\frac{dx}{dt} = (x-1)^2$, an initial point greater than $1$ diverges to infinity, while an initial point less than $1$ converges to $1$.
  \item Even though the corruption is silent in the solver, it can produce corrupted results in the next stages of the application's workflow. For example, in image processing, feature extraction can be based on solving a PDE as shown by Zhou et al. \cite{zhou2010image}. If the PDE solution is incorrect, the iterative process of level set evolution may not converge.
\end{itemize}

\subsection{Objectives of Our SDC Detector}
Replication is a generic solution for detecting all nonsystematic SDCs.
%, although replication may generate false positives in nondeterministic replicated execution.
Hence, a new SDC detector should have  lower memory and/or computational overhead than does replication.
For a numerical integration solver, SDC detection can be interpreted as a function of $(x_{n-k})_{k\geq 0}$ and $(f_{n-k,i})_{k\geq 0,i}$.
Minimizing the computational overhead means computing as few additional operations as possible. Minimizing the memory overhead is equivalent to storing as little  extra data as possible.

Correction can be achieved by recomputing a step that is detected as corrupted. A detector can do a false positive, when it asked  a non-corrupted step to be recomputing.
False positives are wasting resources, minimizing the overheads requires maintaining the number of false positives at a low level.


\subsection{Components}

% the adaptive controller
The numerical integration solvers represent one step in a scientific application. \Cref{fig:model-dual-step} shows an overview of a typical high-performance computing (HPC) workflow composed of a resilient numerical integration solver. The SDC detection is done at each step. When a step is found to be corrupted, it is recomputed in order to allow the solver to continue.



\begin{figure}[!ht]
  \centering
  \includegraphics[width=0.98\columnwidth]{../\rootPath img/system-overview.pdf}
  \caption{SDC detector for an HPC application with a numerical integration solver. At the end of each step, the SDC detector decides whether to validate or reject the step.}
  \label{fig:model-dual-step}
\end{figure}







%\subsection{Methodology}

\subsection{Modeling SDC}
We consider here SDCs that occur randomly on data.
For mathematical discussions, we model an SDC as a random variable $\epsilon_i$ added to a deterministic variable $K$. If $K^o$ and $K^c$ are resp. the noncorrupted and corrupted value of $K$, then $K^o = K$, and $K^c = K^o + \epsilon_i$.

Concerning simulations, recent papers on SDC detections propose different ways to inject SDCs. In. We prefered therefore being exhaustive in our simulations.
On the one hand,  several papers \cite{di2015efficient, bautista2015detecting} injections were done by  randomly flipping bits in data items.
 In the following, we refer to \emph{singlebit} SDCs when one bit is flipped inside a data item, or \emph{multibit} SDCs when several bits are flipped. For example, the IEEE 754 half-precision representation of 1, $0 01111 0000000000$,  might become $0 11111 0000000000 = \infty$ with a singlebit SDC or $0 00001 0000000000 = 2^{-14}$ with a multibit SDC.
 The number of bit-flips in multibit SDCs is drawn from a uniform distribution. In our previous work  \cite{guhur2016lightweight}, we compared several probability distributions to choose the position of the bit-flip, and we noticed that  uniform distribution provides similar results to other distributions. Though multibit SDCs might seem less likely than singlebit SDCs, they are also not protected by error-correcting code memory \cite{ghosh2005selecting}.

On the other hand, a bit-flip on lowest-order positions may not have an impact on the results, whereas a bit-flip in highest-order positions may crash the application or be easy to detect. Consequently, Benson et al. \cite{benson2014silent} simulated SDC injections by multiplying a data item with a random factor. The factor is drawn from a normal distribution with zero mean and unit variance. We refer to this method as \emph{scaled injections}.

In both cases, we injected SDCs on function evaluations (defined in Section \ref{sec:system}) with a probability of 1/100. A too high probability rate creates unrealistic cascading patterns, while a too low probability rate provides insignificant outcomes. In every experiments, we injected at least $10 000$ SDCs. 


\subsection{Simulations}


% PETSC
Our numerical experiments use HyPar~\cite{hypar}, a high-order, conservative finite-difference solver for hyperbolic-parabolic
PDEs. We also use the time integrators (ODE solvers) implemented in PETSc~\cite{petsc-web-page, petsc-user-ref, petsc-efficient}, a portable and scalable toolkit for scientific applications.
HyPar and PETSc are written in C and use the MPICH library on distributed computing platforms.


The use case solves the problem of a rising warm bubble in the atmosphere. This problem is used as a benchmark for atmospheric flows \cite{giraldo2008, ghosh2016}. The governing equations are the
three-dimensional nonhydrostatic unified model of the atmosphere~\cite{giraldokellyconsta2013}, expressed as
\begin{align}
\frac{\partial \rho'}{\partial t} + \nabla \cdot \left(\rho{\bf u}\right) &= 0,\nonumber \\
\frac{\partial \rho{\bf u}}{\partial t} + \nabla \cdot \left(\rho {\bf u}\otimes{\bf u}\right) &= -\nabla P' - \rho'{\bf g},\nonumber \\
\frac{\partial \rho\theta'}{\partial t} + \nabla\cdot \left(\rho{\bf u}\theta\right) &= 0,
\end{align}
where $\rho$ and $P$ are density and pressure, respectively; ${\bf u}$ is the flow velocity; ${\bf g}$ is the gravitational force vector per unit
mass; $\theta$ is the potential temperature; and $\left(\cdot\right)'$ denotes the perturbation to that quantity with respect to
the hydrostatic mean value. The initial solution comprises a stationary atmosphere with $P=10^5\,\textup{N}/\textup{m}^2$ and
$\theta=300\,\textup{K}$, with a warm bubble defined as a potential temperature perturbation~\cite{giraldokellyconsta2013},
\begin{equation}
\Delta \theta = \left\{\begin{array}{cc} 0 & r > r_c \\ \frac{1}{2}\left[1+\cos\left(\frac{\pi r}{r_c}\right)\right] & r \le r_c \end{array}\right.,
\end{equation}
where $r = \|{\bf x}-{\bf x}_c\|_2$, $r_c=250\,\textup{m}$ is the radius of the bubble, and $x_c = \left[500\,\textup{m},500\,\textup{m},260\,\textup{m}\right]$
is the center of the bubble. The domain is a cube of side $1000\,\textup{m}$, and no-flux boundary conditions are applied at all boundaries. The gravitational
force ${\bf g}$ is $9.8\,\textup{m}/\textup{s}^2$ along the $z$-axis.


\begin{figure}[t!]
\begin{center}
\includegraphics[width=0.24\textwidth]{../img/rtb_000.png}\label{fig:rtb_000}
\includegraphics[width=0.24\textwidth]{../img/rtb_100.png}\label{fig:rtb_100}
\includegraphics[width=0.24\textwidth]{../img/rtb_150.png}\label{fig:rtb_150}
\includegraphics[width=0.24\textwidth]{../img/rtb_200.png}\label{fig:rtb_200}
\end{center}
\caption{Rising thermal bubble: Density perturbation ($\rho'$) contours at $0\,\textup{s}$ (initial), $100\,\textup{s}$, $150\,\textup{s}$, and $200\,\textup{s}$ (final).
Ten contours are plotted between $-0.0034$ (red) and $-0.0004$ (blue). The cross-sectional profile is shown at $y=500\,\textup{m}$.}
\label{fig:rtb_contours}
\end{figure}

The use case is solved with HyPar. The domain is discretized on equispaced Cartesian grids,
and the fifth-order WENO~\cite{jiangshu} and CRWENO~\cite{ghoshbaederSISC2012} schemes are used to
compute the spatial derivatives. This computation results in an ODE in time that is solved by using the time integration methods implemented in PETSc.
\Cref{fig:rtb_contours} shows the density perturbation ($\rho'$) contours for
the rising thermal bubble case at $0\,\textup{s}$, $100\,\textup{s}$, $150\,\textup{s}$, and $200\,\textup{s}$, solved on a grid with $64^3$ points. The bubble rises as a result of buoyancy
and deforms as a result of temperature and velocity gradients.

The experiment was done on the Blues cluster  at Argonne National Laboratory. The cluster is composed of 310 compute nodes, 64 GB of memory on each node, 16 cores per compute node with the microarchitecture Intel Sandy Bridge and a theoretical peak performance of 107.8 TFlops.
PETSc is configured with MVAPICH2-1.9.5, shared libraries, 64-bit ints, and O3 flag.



\subsection{Detection performance}


Detection performances are based on the false positive rate and the true positive rate. The false positive rate (FPR) is defined as the ratio between the number of noncorrupted steps that are rejected and the number of noncorrupted steps. Similarly, the true positive rate (TPR) is the ratio between the number of corrupted and rejected steps and the number of corrupted steps.
We did not count rejected steps that would be rejected  in the absence of injections. 


\end{document}
