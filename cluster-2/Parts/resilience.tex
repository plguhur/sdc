\documentclass[conference]{IEEEtran}
\newcommand*{\rootPath}{../}
\renewcommand{\arraystretch}{1.3}
\input{\rootPath Annexes/Packages.tex}

\standalonetrue

\begin{document}


%-----------------------------------------------------------------------------------------
\section{Resilience of Adaptive Controllers}
\label{sec:adaptive}

%Resilience to SDCs is one of the challenge facing exascale computing.
%In Section \ref{sec:problem}, we saw that replication mandates an overhead in computation and in memory higher than $+100.0\%$.
Chen et al. \cite{chen2013comprehensive} remarked that some solvers have an inherent resilience. In the following, we extend this point to all adaptive solvers. Experimentally, we observe that the adaptive controller rejects some steps where the error estimate exceeds a certain threshold due to an SDC.

However, this assumes that the adaptive controller is not corrupted in the presence of an SDC. This assumption is not verified, because the error estimation used by the adaptive controller is computed from corrupted results. In Section \ref{sec:issue}, we observe that the error estimate can be shifted under the threshold of the adaptive controller.

\subsection{Inherent Resilience}
\label{sec:intrinsic}

\subsubsection{Not all SDCs have an impact on the results}
Numerical integration solvers have an inherent approximation error depending on the integration method and its order $p$: the GTE is $O(h^p)$.
When the lowest-order bit is flipped, the impact is insignificant with respect to the approximation error, and this SDC does not affect the accuracy of the results. Basically, we call an insignificant SDC any SDC that does not affect the user's expectation in accuracy. Other SDCs affect higher-order bits, and then they drastically increase the error, or they may even cause the solver to diverge. These SDCs are referred to as \emph{significant}.

Distinguishing significant and insignificant SDCs in the general case is difficult. In our previous work on fixed solvers \cite{guhur2016lightweight}, the user did not give an explicit expectation in accuracy, and we considered that any SDC higher than a tenth of the LTE was significant.
In the case of an adaptive solver, the user explicitly states the maximum acceptable approximation error with  the tolerances $Tol_A$ and $Tol_R$.
Each time a step was corrupted,  we measured the LTE, and we recomputed the step in order to know what would have been the LTE without corruptions. When the error scaled by the tolerances was drifted above $1.0$, the corruption was considered significant.



\subsubsection{Rejection of corrupted steps}
\label{sec:rejection}
In Section \ref{sec:problem}, we saw that an error estimate exceeding the tolerances $Tol_A$ and $Tol_R$ is rejected because the approximation error is considered unacceptable for the user.

When an SDC occurs and the approximation error is shifted outside the tolerance because of the SDC, the step is naturally rejected. In this case, the step size is reduced according to equation \eqref{eq:new-time-step}; then the next noncorrupted step observes that the error estimate is too small and increases the step size. Overall, the computation time is  increased just during one step, while the accuracy is preserved.

The corrupted step is not rejected when the SDC shifts the approximation error below the tolerance or when the SDC is small enough to avoid the approximation error to exceed the tolerance. Accepting such steps seems dangerous, however. One could object that the approximation error can be higher than it would have been without the SDC; even if the current step is below the tolerance, it might affect next steps. However, an adaptive solver is designed in a way that if all steps are below the tolerances, then the expected accuracy is achieved. Accepting such corrupted steps might increase the approximation error on the next steps, but the expected accuracy will be achieved.
%Detecting and correcting such SDCs would thus be a waste of ressources.

One caveat must be added. The approximation error is only estimated. In the presence of an SDC, the estimate is also corrupted, and its value might differ from the real value of the approximation error. This case is considered in Section \ref{sec:issue}.

We injected SDCs in the use case introduced in Section \ref{sec:problem}.
In Table~\ref{tab:detectivity}, we show the detection performances of the adaptive controller.
We report rates for significant corruptions, namely steps whose real scaled LTE is higher than $1.0$. The real scaled LTE is computed from the difference between the corrupted solution $x_n^c$ and a noncorrupted approximation solution $\tilde{x}_n^o$. We reported then the false negative rate of those significant SDCs, called significant false negative (SFN) rate.

The false positive rate remains below $0.1\%$ for all considered ODE methods,% thanks to $\alpha$, $\alpha_{\max}$, $\alpha_{\min}$. 
 At the same time, the true positive rate is usually below $50\%$. Singlebit SDCs are the hardest SDCs to detect ($9.3\%$), whereas the multibit SDCs are the easiest ($55.1\%$). The reason is that singlebit SDCs usually have  a lower impact on the results, as explained in Section \ref{sec:problem}. The true positive rate decreases with the ODE methods. The ODE methods differ in their number $N_k$ of the function evaluations $(K_i)_i$ ($N_k = 7$ for the Dormand-Prince method, $N_k = 4$ for the Bogacki-Shampine method, and $N_k = 2$ for the Heun-Euler method). We can not currently explain this phenomenon.

The true positive rate may seem low, but only significant SDCs need to be rejected. Further experiments must thus distinguish significant from insignificant SDCs in order to know whether the inherent resilience of adaptive solvers is reliable enough.

\begin{table}[h]
\footnotesize
  \centering
\begin{tabular}{ l | c | c |c | r }
    \hline
  Rate & Injector & Heun-Euler & Bogacki-Shampine & Dormand-Prince \\
  \hline
  FP & All & $0.0$ & $0.0$ & $0.0$ \\
  TP & Scaled & $31.1$ & $23.3$ & $20.1$ \\
  TP & Multibit  & $55.1$ & $46.8$ & $35.3$   \\
  TP & Singlebit  & $13.2$ & $11.8$ & $9.3$ \\
  \hline
\end{tabular}
  \caption{Detection accuracy of several ODE methods and several SDC injectors.  FP: false positive. TP: true positive. Results are given in percentage.}
  \label{tab:detectivity}
\end{table}








\subsection{Significant SDCs Not Detected}
\label{sec:issue}

The approximation error is not precisely known but is only estimated. In the presence of a corruption, the estimate is also corrupted. In particular, it may be shifted below the tolerances of the adaptive controller; in such a case, the step would be accepted. We give several examples.

\begin{itemize}
  \item In the extreme case, the registers of $(K_i)_{i\geq 0}$ could be erased. In this case, the corrupted error estimate is equal to zero; consequently the step is accepted, and the step size is increased by $\alpha_{max}$. The solution would be the same as during last step: $x_{n}=x_{n-1}$. The approximation error could then be unacceptable with respect to the user's requirements.
  \item Because any $K_i$ depends on other $(K_j)_{j\ne i}$, the corruption of a certain $K_l$ corrupts the other $(L_j)_{j\ne l}$. Such cascading patterns increase the possibility of underestimating the approximation error.
  \item The SDCs can affect only the estimate. In this case, the estimate can be completely decorrelated from the approximation error.
\end{itemize}
Consequences of accepting a corrupted step can be disastrous. Not only would the corrupted step exceed the user's accuracy expectation, but the next steps will be initialized with a corrupted result. Moreover, the step size might be increased after the corrupted step, and it might even exceed its stability region; in such a case, the solution may not converge at all.

In our use case, we observe that this phenomenon can occur with a random corruption.
In Table~\ref{tab:sfnr}, we show the false negative rate of the classic adaptive controller. The false negative rate is the ratio between the number of accepted but corrupted steps and the number of corrupted steps. Because not all SDCs have an impact on the accuracy of the results, we distinguish the case where a step is corrupted by any kind of SDC and steps corrupted by at least one significant SDCs. In the latter case, the false negative rate is qualified as significant.
The false negative rate with all steps is higher than the significant false negative rate, because insignificant SDCs can have too low an impact on the results to be detectable.

While the significant false negative rate with significant steps achieves $13.3\%$ for Heun-Euler's method with scaled SDCs, the rate increases dramatically to $50.4\%$ for Dormand-Prince's method. The reason is that the number $N_k$ of function evaluations $(K_i)_i$ is higher for Dormand-Prince's method. In this case, more patterns of SDCs can lead to an underevaluation of the error estimation, and the probability of a nondetection is thus higher.
While the false negative rate with all steps is higher with singlebit SDCs than with scaled SDCs, the significant false negative rate is lower with singlebit SDCs than with scaled SDCs. The reason is that a singlebit SDC becomes significant when one of the highest-order bits is flipped, and this is easily detectable, whereas a scaled SDC can be significant while being difficult to detect.






\begin{table}[h]
\footnotesize
\caption{False negative rate for several ODE methods and several SDC injectors. Sign. = significant (only steps that are corrupted with at least one significant SDC are considered). Results are given in percentage.}
  \centering
\begin{tabular}{| l | c c| c c |c c| }
    \hline
\multirow{2}{*}{Injection}  & \multicolumn{2}{c|}{Heun-Euler} & \multicolumn{2}{c|}{Bogacki-Shampine} & \multicolumn{2}{c|}{Dormand-Prince} \\
	\cline{2-7}
& All & Sign. & All & Sign. & All & Sign. \\
  \hline
Singlebit & $86.8$ & $5.4$ & $88.2$ & $10.1$ & $90.7$ & $15.0$ \\
  \hline
Multibit  & $44.9$ & $3.9$ & $53.2$ & $4.5$ & $64.7$ & $7.9$ \\
  \hline
Scaled  & $68.9$ & $13.3$ & $26.7$ & $36.1$ &  $79.9$ & $50.4$ \\

  \hline
\end{tabular}
  \label{tab:sfnr}
\end{table}




\end{document}
