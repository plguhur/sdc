\documentclass[conference]{IEEEtran}
\newcommand*{\rootPath}{../}
\input{\rootPath Annexes/Packages.tex}

\standalonetrue

\begin{document}

%%=============================================================================


\section{Resilience Method For Adaptive Solvers}
\label{sec:method}

We saw that adaptive solvers use an estimate to reject or accept a step. In the presence of SDCs, adaptive solvers can underestimate the approximation error because the estimator is using corrupted data; in this case, the adaptive solvers may not reject all significant SDCs. To address this issue, we increase the reliability of the rejection mechanism by adding a second acceptance step. When the adaptive controller accepts a step, we apply a different rejection mechanism to validate the decision.
%The new workflow is illustrated in Figure \ref{fig:workflow-dchk}.
We use this additional mechanism because the rejection mechanism could be underevaluated following its own pattern of corruptions. By selecting two rejection mechanisms with different patterns, the risk of nondetection of a significant SDCs is reduced. We call our method \emph{double-checking}.

 % \begin{figure}[!ht]
 %   \centering
 %   \includegraphics[width=0.98\columnwidth]{../\rootPath img/workflow-dchk.pdf}
 %   \caption{SDC detector for an HPC application with a numerical integration solver. At the end of each step, the SDC detector decides whether to validate or reject the step.}
 %   \label{fig:workflow-dchk}
 % \end{figure}


We explore here two approaches for computing the double-checking. Both of them compute an estimate of the approximation error and then compare the estimate to a threshold function.
The first approach is using Lagrange interpolating polynomials presented in Section \ref{sec:lbdc}. The  second approach considers an estimate based on another ODE method, and it is explained in Section \ref{sec:ibdc}.

Thereafter, we denote $LTE_1$ the error estimate's vector from the original rejection mechanism, whereas $LTE_2$ is the error estimate's vector used by the double-checking.

% The step is rejected by our method when the second scaled estimate is higher than $1.0$.
%The second method reduces the space of step-size and reduces the maximum of injections with no extra costs by considering mathematical constraints in the stability of the methods. While the first method is the most generic, the second method can be employed for
%


%however, the overhead should be kept at a low level.
%richardson on n-2, n and n-2, n-1, n is a quite expensive, but still less than is replication. moreover recomputing steps avoid a cascading pattern and it is unlikely that a corruption occurs on the first check and the double-check.
%LMM estimates are also interesting because it relies on validated terms.
%the trade-off depends on resources and on orders of the method. cf table.

\subsection{Double-Checking Based On Lagrange Interpolating Polynomials}
\label{sec:lbdc}

One can  obtain an approximation of the solution using Lagrange interpolating polynomials (LIP). We provide formulations for order 0, 1, and 2:
\begin{align*}
\tilde{x}_{n}^0 &= x_{n-1}, \\
\tilde{x}_{n}^1 &= x_{n-1} \frac{h_n+h_{n-1}}{h_{n-1}} - x_{n-2} \frac{h_n}{h_{n-1}}, \\
\tilde{x}_{n}^2 &= x_{n-1} \frac{(h_n+h_{n-1})(h_n+h_{n-1}+h_{n-2})}{h_{n-2}(h_{n-2}+h_{n-1})} \\
&\quad - x_{n-2} \frac{h_n(h_n+h_{n-1}+h_{n-2})}{h_{n-2}h_{n-1}} \\
&\quad+ x_{n-3} \frac{h_n(h_{n}+h_{n-1})}{h_{n-2}(h_{n-1}+h_{n-2})}.
\end{align*}



Secondly, we replaced the threshold function to adapt it for an adaptive solver, in which the user does not give the error bound $\theta$ but the absolute and relative tolerances $Tol_A$ and $Tol_B$.

The obtained double-checking is called \emph{LIP-based double-checking}.



\subsection{Integration-Based Double-Checking}
\label{sec:ibdc}

Our second approach consists in computing another rejection mechanism based on a second error estimate. The second error estimate is computed with a different ODE method from the one used in the solver. It must not require extra computations, in order to reach a low computational overhead. It must also have a larger stability area than the ODE method used by the first method. Because implicit methods have usually a larger stability area than the explicit methods, the latter condition can be followed by employing an implicit method for the double-checking and an explicit method for the solver. The second method computes an approximated solution $\tilde{x}_{n}$. The error estimation is obtained from the difference $x_{n} - \tilde{x}_{n}$. It computes  a local truncation error. We call this method an \emph{integration-based double-checking}. The step is rejected when the norm of $x_{n} - \tilde{x}_{n}$ is higher than $1.0$.

We suggest employing a backward differentiation formula (BDF) for the double-checking because it uses previously computations and because it has a large stability area. We compute the estimates by storing $(x_{n-k})_{k\geq 0}$.  One could also use an Adam-Moulton method: it requires storing $f(t_{n-k}, x_{n-k})$ instead, although it often appears less practical.
BDF are multistep and implicit methods. In the literature, several expressions for a variable step size are given. In the following, we will use the expressions of $\tilde{x}_{n}^1$, $\tilde{x}_{n}^2$, and $\tilde{x}_{n}^3$ for the orders 1, 2, and 3:
%
\begin{align*}
&\tilde{x}_{n}^1 = x_{n-1} + h f(x_{n}), \\
&\tilde{x}_{n}^2 = \frac{(1+\omega_n)^2}{1+2\omega}x_{n-1} - \frac{\omega_n^2}{1+2\omega}x_{n-2} + h f(x_{n}), \\
&\tilde{x}_{n}^3 = h_n\frac{\left(w_n+1\right) \left(w_n w_{n-1}+w_{n-1}+1\right)}{3 w_{n-1}
  w_n^2+4 w_{n-1} w_n+2 w_n+w_{n-1}+1}  f\left(x_{n}\right) \\
&+ \frac{\left(w_n+1\right){}^2 \left(w_{n-1}
     \left(w_n+1\right)+1\right){}^2 }{\left(w_{n-1}+1\right) \left(2
     w_n+w_{n-1} \left(w_n+1\right) \left(3 w_n+1\right)+1\right)} x_{n-1} \\
&- \frac{w_n^2 \left(w_{n-1} \left(w_n+1\right)+1\right){}^2 }{2
  w_n+w_{n-1} \left(w_n+1\right) \left(3 w_n+1\right)+1} x_{n-2}\\
&+ \frac{w_n^2 \left(w_n+1\right){}^2 w_{n-1}^3
}{\left(w_{n-1}+1\right) \left(2 w_n+w_{n-1} \left(w_n+1\right)
  \left(3 w_n+1\right)+1\right)} x_{n-3},
\end{align*}
%
where $\omega_n = \frac{h_{n}}{h_{n-1}}$ and $\omega_{n-1} =
\frac{h_{n-2}}{h_{n-1}}$.

BDF methods have expressions until  order 6, but the stability area  decreases with the order. At the same time, ODE methods with a small $q$ require less computation and less storage of previous solutions $(x_{n-k})_{k \geq 0}$.  In this study, we restrict our work to the orders 1, 2, and 3, in order to avoid stability issues and to mitigate the overheads.

By employing previous solutions $(x_{n-k})_{k . 0}$ and the current solution $x_n$ computed by the ODE method, BDF requires only the computation of $f(x_{n})$. For most ODE methods, however, $f(x_{n})$ is used for the next step. In this case, there is no extra computation when the step is accepted. Certain ODE methods, called first-same-as-last, compute $f(x_n)$ at step $n$, such as Dormand-Prince's method.

%
% \subsubsection{The Richardson extrapolation}
%
% The Richardson extrapolation can also be employed for estimating the LTE. Details for the application on numerical integration solvers are presented by Abraham and Bolarin \cite{abraham2011error}.
% The idea is to apply the same solver with a different step size $h'$, providing a second solution $\tilde{x}$. Most of the cases consider $h=2h'$ or $h=h'/2$.
% As $LTE(h) = Ch^{p+1} + O(h^{p+2})$, the difference between $x$ and $\tilde{x}$ is simplified into:
% \begin{align*}
% x_n - \tilde{x}_n = LTE[x]_n\left(1-(h'/h)^{p}\right).
% \end{align*}
%The method is always available at the price of large memory and computational overheads.





%----------------------------


\subsubsection{Choice of the order}

The estimation of the approximation error uses solutions computed at order $p$.  Thus, the error estimate does not exceed an accuracy higher than $O(h^{p+1})$, even if the second ODE method is expressed at an higher order $q>p$. However, $\tilde{x}_{n}$ tends to be more similar to $x_{n}$  with an higher value of $q$. Consequently, the higher $q$ is, the smaller the error estimate  tends to be. It makes the detection less sensitive:  the second error estimate is less likely to be higher than $1.0$, and fewer steps tend to be rejected. Also, the number of false positives decreases: fewer noncorrupted steps are rejected.


Because we want to improve the detection while maintaining a low false positive rate, we  adapt the order of the ODE method. When the false positive rate is higher than $\gamma$ for an order $q$, a formula with one higher order $q'\leq q_{max}$ is considered. On the contrary, when the FPR is lower than $\Gamma$, the order of the ODE method is decreased to $q'=q-1 \geq 1$. $\Gamma$ can be chosen as the maximum false positive rate we can accept. $\gamma$ must be lower than $\Gamma$ but in the same order of magnitude as $\Gamma$. In our experiments, we took $\gamma = 0.05$, $\Gamma = 0.1$, and $q_{max} = 3$. This procedure is explained in \cref{alg:lte-dchk}. The selection of the order is every $c_{max} = 10$
times or when the detector makes a false positive.

\begin{algorithm}[h]
%\SetAlgoLined
\KwData{$(x_{n-k})_{k \geq 0}, f(x_{n}), q, N_{steps}$}
\KwResult{Rejection or validation of step $n$}
$rejected = True$\;
$SErr_1 = Estimating_1((x_{n-k})_{k \geq 0}, f(x_{n}))$\;\tcp{Eq.~\eqref{eq:lee}}
\If{$c++ == c_{max}$} {
\tcc{Update ODE method's order $q$}
$c_{max} = 0$\;
\uIf{$FP_q/N_{steps} < \gamma$} { $q = \max{(1, q-1)}$}
\uElseIf{$FP_q/N_{steps} > \Gamma$} { $q = \min{(q_{max}, q+1)}$}
}
\uIf {$SErr_1 == lastSErr$} {
\tcc{Case of a false positive}
  $validation = True$\;
  $FP_q++$\;
  $c = c_{max}$
}
\Else {
  \textbf{bool} validation $= SErr < 1.0$\;
  \If {validation}{
    $SErr_2 = Estimating_2((x_{n-k})_{k \geq 0}, f(x_{n}),q)$\;
    $validation = SErr_2 < 1.0$\;
    $lastSErr = SErr_1$\;
  }
}
\If {validation} {
  $n++$\;
  $rejected = False$\;
  $h = NewStepSize(Serr_1, h)$\; \tcp{Eq.~\eqref{eq:hnew}}
}
 \caption{Adaptive-controller with integration-based double-checking}
 \label{alg:lte-dchk}
\end{algorithm}



%----------------------------
\subsubsection{About correctness}

We wonder under which conditions the double-checking allows to detect SDC that would not have been detected by the adaptive controller.
For the scope of the article, we consider only the case where the solver is using the Heun-Euler method and the LIP-based double-checking at order 1.
At step $n$, we have the following expressions:
\begin{align*}
    x_n &= x_{n-1} + \frac{h_n}{2}\left(f(x_{n-1}) + f\left(x_{n-1}+h_nf(x_{n-1})\right)\right) \\
    LTE_1 &= \frac{h_n}{2}\left(-f(x_{n-1}) + f\left(x_{n-1}+h_nf(x_{n-1})\right)\right) \\
    LTE_2 &= \left(x_{n-2}+x_{n-1}\right) \frac{h_n}{h_{n-1}} \\
    & + \frac{h_n}{2}\left[f(x_{n-1}) + f\left(x_{n-1}+h_nf(x_{n-1})\right)\right].
\end{align*}

If the SDC shifts $x_{n-1}$ by $\epsilon$, then $x_n^c = x_n^o + \epsilon$, $LTE_1^c = LTE_1^o$ and $LTE_2^c = LTE_2^o + \frac{h_n}{h_{n-1}}\epsilon$. Here, only the second estimate is affected by the SDC and is able to detect it. The double-checking is thus necessary.

If the SDC shifts $K_{i}$ by $\epsilon$, then $x_n^c = x_n^o + \epsilon\frac{h_n}{2}$, $LTE_1^c = LTE_1^o-\epsilon\frac{h_n}{2}$ and $LTE_2^c = LTE_2^o + \epsilon\frac{h_n}{2}$. The double-checking is necessary, if $x_n^c-x(t_n)$ and $LTE_2$ exceed the tolerance, whereas $LTE_1$ does not. By noting $\tau = \sqrt n .Err > 0$, it provides the following inequalities for each component $j$ of the vectors:
\begin{align*}
    2.\frac{\tau- x_{n,j}^o + x(t_n)_j}{h_n}  >   \epsilon_j> 2.\frac{-\tau- x_{n,j}^o + x(t_n)_j}{h_n}  \\
    2.\frac{\tau + LTE_{1,j}}{h_n} >  \epsilon_j> 2.\frac{-\tau + LTE_{1,j}}{h_n} \\
    2.\frac{\tau - LTE_{2,j}}{h_n} >  \epsilon_j> 2.\frac{-\tau - LTE_{2,j}}{h_n}.
\end{align*}
In this case, all terms can be interpreted as random variables, and thus an evaluation of the probability to achieve all equations is impossible. That is why, we did empirical evaluations in Section \ref{sec:xp}.

% We consider the worst case, where $LTE_{1} = 0$. To simplify, we assume that $x_n^o = x(t_n)$. Two inequations are remaining:
% \begin{align*}
%     2.\frac{\tau}{h_n} >  \epsilon_j> 2.\frac{-\tau}{h_n} \\
%     2.\frac{\tau - LTE_{2,j}}{h_n} >  \epsilon_j> 2.\frac{-\tau - LTE_{2,j}}{h_n} \\.
% \end{align*}

%----------------------------
\subsubsection{Implementation}

The implementation was directly done inside the adaptive controller.
This allows  reuse of some allocation in memory to compute the second estimate. After, we refer to the adaptive controller without the double-check mechanism a \emph{classic adaptive controller}.
Because $x_{n-1}$ is already stored by the classic adaptive controller,  double-checking  requires the storage of only $x_{n-2}$ and $x_{n-3}$.
% The implementation is illustrated in \cref{fig:adaptive-controller}.
%
%   \begin{figure}[!htb]
%     \centering
%         \includegraphics[width=0.48\textwidth]{../\rootPath img/adaptive-controller.pdf}
%       \caption{Scheme of the adaptive controller without our method (left) and with our method (right). The SDC detector can reject a step but does not change the step size.}
%     \label{fig:adaptive-controller}
%   \end{figure}



\end{document}
