\documentclass[conference]{IEEEtran}
\newcommand*{\rootPath}{../}
\input{\rootPath Annexes/Packages.tex}

\standalonetrue

\begin{document}

%%=============================================================================

\section{Results}
\label{sec:results}

In \cref{sec:classic}, we show that the classic adaptive controller is not able to detect all significant SDCs. \Cref{sec:detc} shows that our method reduces significantly the risk of accepting a significant SDC.  In \cref{sec:overheads} and in \cref{sec:scal}, we measure the overheads and the scalability of our method and AID in the use case. 
%In \cref{fig:viz}, we visualize the influence of SDCs injections on results. It shows the density perturbation ($\rho'$) contours for
% the rising thermal bubble case at $0\,\textup{s}$, $100\,\textup{s}$, $150\,\textup{s}$, and $200\,\textup{s}$. The solutions
% are obtained on a grid with $101^2$, the 5th order WENO scheme, and the Heun-Euler method. The bubble rises as a result buoyancy
% and deforms as a result of temperature and velocity gradients.
%In \cref{fig:viz-b}, SDCs were injected. The adaptive controller did not reject all of the significant SDCs; therefore the results are visibly different. Figure~\ref{fig:viz-c} illustrates that eventually our BDF-based double-checking allows the user to obtain trustworthy results.



\subsection{Corruption of the Classic Adaptive Controller}
 \label{sec:classic}
% \begin{figure}[h!]
%         \centering
%         \begin{center}
%           \includegraphics[width=0.49\columnwidth]{../\rootPath img/rtb_000.png}\label{fig:rtb_000}
%           \includegraphics[width=0.49\columnwidth]{../\rootPath img/rtb_100.png}\label{fig:rtb_100}
%           \includegraphics[width=0.49\columnwidth]{../\rootPath img/rtb_150.png}\label{fig:rtb_150}
%           \includegraphics[width=0.49\columnwidth]{../\rootPath img/rtb_200.png}\label{fig:rtb_200}
%         \end{center}
%     \label{fig:viz}
%     \caption{Rising thermal bubble: Density pertubation ($\rho'$) contours at $0\,\textup{s}$ (initial), $100\,\textup{s}$, $150\,\textup{s}$, and $200\,\textup{s}$ (final).
%     Ten contours are plotted between $-0.0034$ (red) and $-0.0004$ (blue). The cross-sectional profile is shown at $y=500\,\textup{m}$.}
% \end{figure}


In the Table~\ref{tab:detectivity}, we present the detection performances of the classic adaptive controller. 
The adaptive controller under-estimates the step when the number $N_k$ of $(K_i)_i$ is increasing. 
The rate of significant false negatives increases as $N_k$ increases. For the Dormand-Prince fifth-order method, $N_k = 7$, $N_k = 4$ for the Bogacki-Shampine third-order method, and $N_k = 2$ for the Heun-Euler method. The rate is given only for scaled SDCs, because it is hardly observable for random bit-flips that engender too high errors (easier to detect by the adaptive solver), or too low errors (not significant).


{\renewcommand{\arraystretch}{1.3}%
\begin{table}[h]
\footnotesize
  \centering
\begin{tabular}{| l | c c r |}
    \hline
  & Heun-Euler & Bogacki-Shampine & Dormand-Prince \\
  \hline 
  FPR  & $0.0$ & $0.0$ & $0.0$ \\
  TPR scaled & $31.1$ & $23.3$ & $20.1$ \\
  TPR multibit  & $55.1$ & $46.8$ & $35.3$   \\
  TPR singlebit  & $13.2$ & $11.8$ & $9.3$ \\
  SFNR scaled & $13.3$ & $36.1$ & $50.4$ \\
  \hline
\end{tabular}   
  \caption{Detection accuracy for different ODE methods and different way of injecting SDCs without our SDC detector.  SFNR: significant false negative rate. FPR: false positive rate. TPR: true positive rate. Results are given in percentage.}
  \label{tab:detectivity}
\end{table}
} \quad


\subsection{BDF-based Double-checking}
\label{sec:detc}
{\renewcommand{\arraystretch}{1.3}%
\begin{table}[h]
\footnotesize
  \centering
\begin{tabular}{| r | c|c|c | c|c |}
    \hline
  & \multicolumn{3}{c|}{Rates (\%)} & \multicolumn{2}{c|}{Overheads (\%)} \\
  \cline{2-6}
  & FPR & TPR & SFNR & Memory & Computational \\
  \hline 
  Classic & $0.0$ & $31.1$ & $13.3$ & $+0.0$ & $+0.0$ \\
  \hline
  AID  \\
  \hline
  SAID & $2.3$ & $33.1$ & $4.1$ & $+57.6$ & $+2.4$ \\
  \hline
  EDBC & $44.8$ & $38.4$ & $2.2$ & $+42.7$ & $+45.0$ \\
  \hline
  BBDC & $4.2$ & $41.9$ & $1.1$ & $+42.7$ & $+4.5$\\ 
  \hline
  Replication & $100.0$ & $100.0$ & $0.0$ & $+100.0$ & $+100$ \\
  \hline
\end{tabular}   
  \caption{Our double-check mechanism's (BBDC) detection performances are compared with the state of the art (AID), two detectors inspired from AID (SAID and EDBC), the replication, and the classic adaptive controller (Classic)}
  \label{tab:detectivity-dchk}
\end{table}
} \quad

We applied the BDF-based double-checking to the Heun-Euler method.  Table~\ref{tab:detectivity-dchk} shows the detection performances and the overheads in computation and in memory of the BDF-based double-checking. The overheads are obtained when the computation is done with one core. Almost all significant false negatives were detected. However, the false positive rate achieves $4.2\%$ and additional memory is required to store previous solutions. For high dimension problems, the storage of solutions occupies the largest part of the memory.  The overhead in memory is $+42.7\%$ and in computation is $+4.5\%$. 
 SAID reduces the rate of significant false negatives with a factor of $3$, whereas our method decreases the rate with a factor of $12$. Contrary to SAID, the threshold of EDBC is not relative to number of false positives, but is fixed to 1.0. Consequently, the FPR bursts at 44.8\%, and the computational overhead achieves $45\%$. This result shows that extrapolation in AID is less accurate than BDF for a stiff problem.


\subsection{Overheads}
\label{sec:overheads}
Though our method has similar performance in SDC detection as replication, its overheads in memory and replication are roughly 10 times smaller than replication. The overheads depend on the number of cores: with the number of cores decreasing, parts of HyPar that can not be parallelized becomes more and more important with respect to the cost of the BDF-based double-checking.

The computational overhead is partly due to the cost of the BDF-based double-checking and to false positives, as false positives require to recompute a noncorrupted step. The false positive rate is $9.8\%$, while the computational overhead is $10.1\%$. Therefore, the computational cost of our method is mainly due to the cost of recomputing a false positive. To a certain extent, the computational overhead can be reduced by decreasing the parameters $\gamma$ and $\Gamma$.
Without our method, the implementation is storing $N_k + 2$ vectors of data. The double-checking requires to store two previous solutions for third-order BDF. The expectation of the memory overhead is thus lower than $2/(N_k+2)$. 


{\renewcommand{\arraystretch}{1.3}%
\begin{table}[h]
\footnotesize
  \centering
\begin{tabular}{| l |  c|c|c|c | c|c|c|c |}
    \hline
  \multirow{2}{*}{Cores} 
        &    \multicolumn{3}{c|}{512} & \multicolumn{3}{c|}{4096}    \\
        \cline{2-7}
        & Class. & AID & SAID & BBDC & Class. & AID & SAID & BBDC \\
    \hline
  Memory (B) &  $1.8e^5$ & $2.8e^5$ & $2.4e^5$ & $5.6e^4$ & $8.9e^4$ & $6.8e^4$ \\
  \hline
 SDC detector (s) &  $0.0$ & $3.8e^2$ & $3.9e^2$ & $0.0$ & $1.0e^2$  &  $1.1e^2$ \\
  \hline
  Adapt. cont. (s) & $1.5e^2$ & $1.5e^2$ & $5.1e^2$ & $1.4e^2$ & $1.4e^2$ &  $2.4e^2$ \\
  \hline
  Step (s) & $1.2e^3$ & $1.3e^3$ & $1.3e^3$ &    $4.6e^2$ & $4.7e^2$ & $4.9e^2$ \\ 
  \hline
\end{tabular}   
  \caption{Comparison of the overheads in memory and in computation. Class.: classic adaptive controller.}
  \label{tab:details}
\end{table}
} \quad

Table~\ref{tab:details} details the cost in memory and in computation of the classic adaptive controller, AID, and our method. The scalability is limited even in the classic adaptive controller, because the main cost of the classic adaptive controller is the norm, and the norm requires to synchronize cores.
Contrary to the double-check mecanisms, AID and SAID were not implemented inside the adaptive controller, but as an independant module. As a consequence, the time duration of the adaptive controller is the same for AID and SAID than for the classic adaptive controller.

 

\subsection{Scalability}
\label{sec:scal}

\begin{figure}[!htb]
  \centering
         \includegraphics[width=0.48\textwidth]{../\rootPath img/time.pdf}
    \caption{Relative performance compared to the classic adaptive controller  in time until 4096 cores.}
  \label{fig:scaling-time}
\end{figure}

\begin{figure}[!htb]
  \centering
      \includegraphics[width=0.48\textwidth]{../\rootPath img/memory.pdf}
    \caption{Relative performance compared to the classic adaptive controller in memory until 4096 cores.}
  \label{fig:scaling-mem}
\end{figure}

\begin{figure}[!htb]
  \centering
      \includegraphics[width=0.48\textwidth]{../\rootPath img/flops.pdf}
     \caption{Relative performance compared to the classic adaptive controller in floating-point operations until 4096 cores.}
  \label{fig:scaling-flop}
\end{figure}

\begin{figure}[!htb]
  \centering
      \includegraphics[width=0.48\textwidth]{../\rootPath img/flopssec.pdf}
  \caption{Relative performance compared to the classic adaptive controller in floating-point operations per second until 4096 cores.}
  \label{fig:scaling-flop-sec}
\end{figure}    


\Cref{fig:scaling-time} to \cref{fig:scaling-flop-sec} show the relative performance compared to the classical adaptive controller of SAID (blue) and BDF-based double-checking (green) 4096 cores. The overheads tend to decrease with the number of cores, because the SDC detectors provide a better scalability than the rest of HyPar. We disclosed results for AID and extrapolation-based double-checking, since the trade-off detection accuracy and overheads is less interesting than our method and for SAID. 
The mean overhead in time is 5\% higher for the BDF-based double-checking method than for SAID, because our method has more false positives than AID has. However, the mean memory overhead of SAID is 58\% higher than for the BDF-based double-checking method, because another previous solution is required to be stored. 
Our method requires less computations in term of floating-point operation than SAID. While operations on vector are similar to both cases, the difference is relative to algorithm for selecting the order. For our method, it consists of a simple fraction, whereas SAID computes norms and linear combination of solutions.
Finally, the computation rate of SAID is significantly better than our method because... ??


\end{document}