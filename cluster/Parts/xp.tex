\documentclass[conference]{IEEEtran}
\newcommand*{\rootPath}{../}
\input{\rootPath Annexes/Packages.tex}

\standalonetrue

\begin{document}



%%=============================================================================
%----------------------------------------------------------------------------------------
%   XP
%----------------------------------------------------------------------------------------

\section{Experiments}
\label{sec:xp}

% PETSC
Our numerical experiments use HyPar~\cite{hypar}, a high-order, conservative finite-difference solver for hyperbolic-parabolic
PDEs. It also uses the time integrators (ODE solvers) implemented in PETSc~\cite{petsc-web-page, petsc-user-ref, petsc-efficient}, a portable and scalable toolkit for scientific applications.
HyPar and PETSc are written in C and use the MPICH library on distributed computing platforms.




\subsection{Rising thermal bubble}
The use case solves the problem of a rising warm bubble in the atmosphere. The governing equations are the 
three-dimensional nonhydrostatic unified model of the atmosphere~\cite{giraldokellyconsta2013}, expressed as
\begin{align}
\frac{\partial \rho'}{\partial t} + \nabla \cdot \left(\rho{\bf u}\right) &= 0,\nonumber \\
\frac{\partial \rho{\bf u}}{\partial t} + \nabla \cdot \left(\rho {\bf u}\otimes{\bf u}\right) &= -\nabla P' - \rho'{\bf g},\nonumber \\
\frac{\partial \rho\theta'}{\partial t} + \nabla\cdot \left(\rho{\bf u}\theta\right) &= 0,
\end{align}
where $\rho$ and $P$ are density and pressure, respectively; ${\bf u}$ is the flow velocity; ${\bf g}$ is the gravitational force vector per unit
mass; $\theta$ is the potential temperature; and $\left(\cdot\right)'$ denotes the pertubation to that quantity with respect to
the hydrostatic mean value. The initial solution comprises a stationary atmosphere with $P=10^5\,\textup{N}/\textup{m}^2$ and
$\theta=300\,\textup{K}$, with a warm bubble defined as a potential temperature perturbation~\cite{giraldokellyconsta2013},
\begin{equation}
\Delta \theta = \left\{\begin{array}{cc} 0 & r > r_c \\ \frac{1}{2}\left[1+\cos\left(\frac{\pi r}{r_c}\right)\right] & r \le r_c \end{array}\right.,
\end{equation}
where $r = \|{\bf x}-{\bf x}_c\|_2$, $r_c=250\,\textup{m}$ is the radius of the bubble, and $x_c = \left[500\,\textup{m},500\,\textup{m},260\,\textup{m}\right]$
is the center of the bubble. The domain is a cube of side $1000\,\textup{m}$, and no-flux boundary conditions are applied at all boundaries. The gravitational
force ${\bf g}$ is $9.8\,\textup{m}/\textup{s}^2$ along the $z$-axis.


\begin{figure}[t!]
\begin{center}
\includegraphics[width=0.24\textwidth]{../img/rtb_000.png}\label{fig:rtb_000}
\includegraphics[width=0.24\textwidth]{../img/rtb_100.png}\label{fig:rtb_100}
\includegraphics[width=0.24\textwidth]{../img/rtb_150.png}\label{fig:rtb_150}
\includegraphics[width=0.24\textwidth]{../img/rtb_200.png}\label{fig:rtb_200}
\end{center}
\caption{Rising thermal bubble: Density pertubation ($\rho'$) contours at $0\,\textup{s}$ (initial), $100\,\textup{s}$, $150\,\textup{s}$, and $200\,\textup{s}$ (final).
Ten contours are plotted between $-0.0034$ (red) and $-0.0004$ (blue). The cross-sectional profile is shown at $y=500\,\textup{m}$.}
\label{fig:rtb_contours}
\end{figure}

The use case is solved with HyPar. The domain is discretized on equi-spaced Cartesian grids, 
and the 5th-order WENO~\cite{jiangshu} and CRWENO~\cite{ghoshbaederSISC2012} schemes are used to
compute the spatial derivatives. This computation results in an ODE in time that is solved by using the time integration methods implemented in PETSc.
\Cref{fig:rtb_contours} shows the density perturbation ($\rho'$) contours for
the rising thermal bubble case at $0\,\textup{s}$, $100\,\textup{s}$, $150\,\textup{s}$, and $200\,\textup{s}$, solved on a grid with $64^3$ points. The bubble rises as a result buoyancy
and deforms as a result of temperature and velocity gradients. 


\subsection{Cluster}
The first case was computed on the cluster Blues at Argonne National Laboratory. The cluster is composed of 310 compute nodes, 64 GB of memory on each node, 16 cores per compute node with the microarchitecture Intel Sandy Bridge and a theoretical peak performance of 107.8 TFlops.
PETSc was configured with MVAPICH2-1.9.5, shared libraries, 64 bit ints, and O3 flag.  

\subsection{Methodology}

\subsubsection{SDC injections}
Recent papers on SDC detections propose different ways to inject SDCs. In several papers \cite{di2015efficient, bautista2015detecting} injection were done by flipping randomly bits. In our previous work  \cite{guhur2016lightweight}, we compared several probability distributions to choose the position of the bit-flip. In the following, we refer to these kinds of injections by \emph{singlebit} injections when one bit is flipped inside a data item, or \emph{multibit} injections when several bits are flipped. The number of bit-flips in multibit injections is drawn from a uniform distribution.

A bit-flip on lowest-order positions may not have an impact on the results, while a bit-flip in highest-order positions may crash the application or be easy to detect. Consequently, Benson et al. \cite{benson2014silent} simulated SDC injections by multiplying a data item with a random factor. The factor is drawn from a normal distribution with zero mean and unit variance. We refer to this method as \emph{scaled injections}.

Corruption can affect one component inside a $(K_i)_{i \geq 0}$. The probability of injections in our experiments was $0.05$.

\subsubsection{Measuring detection accuracy}
We measured performance with false positive and true positive rates. We did not count rejected steps that would be rejected  in the absence of injections. We report also rates for significant corruptions, namely steps whose real scaled LTE is higher than $1.0$. The real scaled LTE is computed from the difference between the corrupted solution $x_n^c$ and a noncorrupted approximation solution $\tilde{x}_n^o$. We reported then the false negative rate of those significant SDCs, called significant false negative (SFN) rate.

%\todo{extend}


\subsubsection{Comparison with the state of the art}
We compare the results of our method with replication, a state-of-the-art method called AID, and two SDC detectors derived from AID,  SAID and extrapolation-based double-checking. Details about these methods are given in \cref{sec:relative}.
AID's memory overhead can be reduced by sampling the stored solutions. This means that only certain components are stored, whereas the other components are disclosed. It assumes that the corruption affects not only one component by also the neighboors. In our case, this assumption is not validated, because we inject SDCs on $(K_i)_{i \geq 0}$, and a solution is obtained from a linear combination of the  $(K_i)_{i \geq 0}$. Consequently, sampling would strongly affected the detectivity. We prefer to not emoploy the sampling to improve AID's results.  
For AID and SAID, the parameter \emph{error bound} was fixed to $1.0e^{-5}$ as it provides the best results in detectivity, while its false positive rate remains below $10\%$. 
\end{document}
