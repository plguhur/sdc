\documentclass[conference]{IEEEtran}
\newcommand*{\rootPath}{../}
\input{\rootPath Annexes/Packages.tex}

\standalonetrue

\begin{document}

%%=============================================================================


%%=============================================================================
%----------------------------------------------------------------------------------------
%   Method
%----------------------------------------------------------------------------------------

\section{Adaptive solvers}
\label{sec:adaptive}

Most time-stepping solvers available in scientific libraries or custom
implementations 
have some kind of error control associated with the
integration process. These solvers are called adaptive solvers. The control mechanism, called an \emph{adaptive controller}, is enforced by changing the
step size based on an error estimate, as explained in \cref{sec:adaptive}.
The error control relies on estimates, as presented in \cref{sec:lee}.
In \cref{sec:intrisinc}, we show that  such an adaptive controller has intrinsinc resilience properties that can improve its reliability. 




%-----------------------------------------------------------------------------------------
\newcommand{\dt}{h}
\newcommand{\TolA}{{\rm Tol}_{\rm A}}
\newcommand{\TolR}{{\rm Tol}_{\rm R}}
\subsection{Design of Adaptive Solvers}
%\label{sec:adaptive}
The user typically provides a
desired absolute $\TolA$ or a relative $\TolR$ error
tolerance. In practice, the error estimate is based on LTE, so for
every step the algorithm verifies that the estimated local truncation
error satisfies the tolerances provided by the user and suggests a new
step size to be taken. For Runge-Kutta methods, the LTE is obtained from
an approximation of the solution. The method to compute this approximation is chosen in order to save computation or memory.
% and in many cases the $LTE$ is one order lower $\widehat{p}=p-1$, which is what we will consider for the rest of this exposition.

The adaptive controller at step $n$ forms the acceptable
error level and scaled level as
%%%
\begin{align*}
Err_n&=\TolA + || x_n || \TolR\,,\\
SErr_n&= m^{\frac{1}{q}} \left\|\frac{|x_n
  -\tilde{x}_n|}{Err_n}\right\|_q\,, 
\end{align*}
%%%
where the errors are computed componentwise, $m$ is the dimension of $x$, and $q$ is typically 2 or
$\infty$ (max norm). The error tolerances are satisfied when $SErr_k
\le 1.0$.

\subsubsection{Estimating the local error truncation}
\label{sec:lee}
Several LEEs can be employed for adaptive solvers.  
Usually, LEEs substract $x_n$ with an approximation of it, $\tilde{x}_n$.  
\begin{align}
x_n - \tilde{x}_n &=  x_n - u(t_n,n-1) - \left(u(t_n,n-1) - \tilde{x}_n\right), \\
&= LTE[x]_n -LTE[\tilde{x}]_n.
\label{eq:lee}
\end{align}
%In pratice, most of the solvers employ an embedded methods; some also use Richardson's extrapolation (GSL for example). 
%We will use that we can also employ an LMM. It saves computations at the cost of accuracy and memory. 

Although estimates based on Richardson's extrapolation can be employed \cite{abraham2011error}, the estimation is generally based on an embedded method. Embedded methods compute at each step two results at two different orders $p$ and $q$: $x_n^p$ and $x_n^{q}$ (in general $|q-p| = 1$).  The solution is propagated by one of these results, while the second result provides the approximation  $\tilde{x}_n$ that is used to compute an LEE at step $n$. 
  If $q$ is at a higher order than $LTE^p$, then the difference between $x_n^p$ and $x_n^{q}$ is an LEE of $x_n^p$:
\begin{align}
  x_n^p - x_n^{q} &=  LTE[x^p]_n -LTE[x^q]_n \\
  &=LTE[x^p]_n + O(h^{q+1}).
\label{eq:new-time-step}
\end{align}


\subsubsection{Control of error estimation}
Based on this error estimate, in practice the step size that would
satisfy the tolerances is
%%%
\begin{align}
  \label{eq:hnew}
 \nonumber
 A_{n+1}&=\alpha (1/SErr_{n+1})^\frac{1}{\widehat{p}+1}, \\
 \dt_{\rm new}(t_n)&=\dt_{\rm{old}}(t_n) \min(\alpha_{\max},
 \max(\alpha_{\min}, A_{n+1} ))\,,
\end{align}
%%%
where $\alpha_{\min}$ and  $\alpha_{\max}$ keep the change in $\dt$ to
within a certain factor. $\alpha<1$ is chosen so that there is
some margin for which the tolerances are satisfied and so that the
probability of rejection is decreased.

In this study we use the following settings: $\alpha=0.9$,
$\alpha_{\max}=10$, $\alpha_{\min}=0.1$ and $q=2$. Therefore, the scaled error is
$SErr= \sqrt{\frac{1}{n} \sum_n \frac{|x-\tilde{x}|^2}{Err^2}}$, and
the step size is adjusted as $\dt_{\rm new}=\dt_{\rm{old}} \min(10
,\max(0.1, 0.9 A))$. 

%%%%%%%
\subsubsection{Scheme of an adaptive controller}
%%%%%%%

The adaptive controller works in the following way. After completing
step $n$, if $SErr_{n} \le 1.0$, then the step is accepted, and the 
next step is modified according to \eqref{eq:hnew}; otherwise the step
is rejected and retaken with the step length computed in
\eqref{eq:hnew}.

%{\color{red} \textbf{This is rentative} Silent data curruption
 %  occuring in the step size or step estimation will likely trigger a
%   step rejection\include{method.tex} and that can lead to two possible outcomes: 1) the
%   step is reduced and re-taken which leads to inefficiency of the
%   overrall time stepping and 2) the step is increased by too much and
%   can lead to both to poor accuracy and efficiency.}
%
% \subsubsection{Regulating the update of step-size}
%
% {\color{red} \textbf{This is rentative} I think here we should list
%   the winning algorithm.}

%-----------------------------------------------------------------------------------------




%-----------------------------------------------------------------------------------------
\subsection{Resilience of Adaptive Controllers}
\label{sec:intrisinc}
Numerical integration solvers have an inherent approximation error depending on the integration method and its order $p$: the GTE is $O(h^p)$.
Because some low-order bits can be flipped without impacting a result, SDCs that affect those bits are called \emph{insignificant}. Other SDCs affect higher-order bits: they increase the error and hence affect the user's accuracy requirement, or they may even cause the solver to diverge. Those are referred to as \emph{significant}.

%\subsubsection{Significant SDC}
% significant SDCs
In an adaptive solver, the user indicates a requirement with the choice of the tolerances $Tol_A$ and $Tol_R$. An LTE of a noncorrupted step below the tolerance  is not rejected, because it is considered acceptable for the user. Similarly, a corruption that brings the LTE below the tolerance is also acceptable. In such cases, detecting and correcting the corruption would be a waste of ressources. 
Even though such corruption impacts the accuracy of the next steps, this cascading pattern is the same as for a noncorrupted step and is assumed to be acceptable.
%It is notifiable that controlling the approximation error with the LTE can not always guarantee the control of the GTE as showed it.

In the case of rejection of a corrupted step, the step size is reduced according to equation \eqref{eq:new-time-step}; then the next noncorrupted step observes that the LEE is too small and increases the step size. Overall, the computation time is just increased during one step, while the accuracy is preserved. 

\subsection{Significant SDCs Not Detected}
The LTE is not precisely known but estimated by the LEE, which may also be corrupted. In presence of a corruption, the LTE may thus be underestimated, and the step would be accepted. We can give several examples where it can happen.
\begin{itemize}
  \item In the extreme case, the registers of $(K_i)_{i\geq 0}$ could be erased. In this case, the corrupted LEE is equal to zero; consequently the step is accepted, and the step size is increased by $\alpha_{max}$. The solution would the same than during last step: $x_{n}=x_{n-1}$. The LTE may then be unacceptable with respect to user's requirements. 
  \item Because any $K_i$ depends on other $(K_j)_{j\ne i}$, the corruption of a certain $K_l$ corrupts the other $(L_j)_{j\ne l}$. Such cascading patterns increase the possibility of underestimating the LTE.
  \item The SDCs can affect only the estimate. In this case, the estimate could be much smaller than it would have been in the absence of SDCs. 
\end{itemize}
Consequences of accepting a corrupted step can be disastrous. Not only the corrupted step exceeds the user's accuracy expectation, but the next steps will be initialized with a corrupted result. Moreover, the step size might be increased after the corrupted step, and it might even exceed its stability region; in such case, the solution may not converge at all. 
  
  



\end{document}
