\contentsline {section}{\numberline {1}Introduction}{6}{section.1}
\contentsline {subsection}{\numberline {1.1}Application Scenario}{6}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Contributions and organizations}{6}{subsection.1.2}
\contentsline {section}{\numberline {2}Related Work}{8}{section.2}
\contentsline {subsection}{\numberline {2.1}Resilience in High-Performance Computing}{8}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Numerical Integration Solver}{8}{subsection.2.2}
\contentsline {subsubsection}{\numberline {2.2.1}Differential equation}{8}{subsubsection.2.2.1}
\contentsline {subsubsection}{\numberline {2.2.2}Single-step and multi-step methods}{9}{subsubsection.2.2.2}
\contentsline {subsubsection}{\numberline {2.2.3}Implicit and explicit methods}{9}{subsubsection.2.2.3}
\contentsline {subsubsection}{\numberline {2.2.4}Function evaluations}{9}{subsubsection.2.2.4}
\contentsline {subsubsection}{\numberline {2.2.5}Approximation error}{10}{subsubsection.2.2.5}
\contentsline {subsubsection}{\numberline {2.2.6}Estimation of the approximation error}{10}{subsubsection.2.2.6}
\contentsline {subsubsection}{\numberline {2.2.7}Adaptive solvers}{12}{subsubsection.2.2.7}
\contentsline {subsection}{\numberline {2.3}Resilience to SDCs}{14}{subsection.2.3}
\contentsline {subsubsection}{\numberline {2.3.1}Generic solutions}{14}{subsubsection.2.3.1}
\contentsline {subsubsection}{\numberline {2.3.2}Algorithmic resilience}{14}{subsubsection.2.3.2}
\contentsline {subsubsection}{\numberline {2.3.3}Fixed numerical integration solvers}{14}{subsubsection.2.3.3}
\contentsline {subsubsection}{\numberline {2.3.4}Consequences of SDCs in Numerical Integration Solvers}{15}{subsubsection.2.3.4}
\contentsline {subsection}{\numberline {2.4}SDC Injector}{15}{subsection.2.4}
\contentsline {section}{\numberline {3}Model and Assumptions}{17}{section.3}
\contentsline {subsection}{\numberline {3.1}Silent Data Corruption Model}{17}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Objectives}{17}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Workflow}{17}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Assumption on the solver}{18}{subsection.3.4}
\contentsline {section}{\numberline {4}SDC Detection in Fixed Solver}{19}{section.4}
\contentsline {subsection}{\numberline {4.1}The proposed \emph {Hot Rod} method}{19}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}First detector: \emph {Hot Rod HR}}{19}{subsection.4.2}
\contentsline {subsubsection}{\numberline {4.2.1}Threshold function}{19}{subsubsection.4.2.1}
\contentsline {subsubsection}{\numberline {4.2.2}Detection of the significant SDC}{20}{subsubsection.4.2.2}
\contentsline {subsubsection}{\numberline {4.2.3}A reliable training set}{20}{subsubsection.4.2.3}
\contentsline {subsubsection}{\numberline {4.2.4}Adaptive control}{21}{subsubsection.4.2.4}
\contentsline {subsection}{\numberline {4.3}Second detector: \emph {Hot Rod LFP}}{21}{subsection.4.3}
\contentsline {subsection}{\numberline {4.4}Algorithm}{22}{subsection.4.4}
\contentsline {subsection}{\numberline {4.5}Experiments and results}{22}{subsection.4.5}
\contentsline {subsubsection}{\numberline {4.5.1}Environment}{22}{subsubsection.4.5.1}
\contentsline {subsubsection}{\numberline {4.5.2}Benchmark}{23}{subsubsection.4.5.2}
\contentsline {subsubsection}{\numberline {4.5.3}Results}{24}{subsubsection.4.5.3}
\contentsline {subsection}{\numberline {4.6}Conclusion}{25}{subsection.4.6}
\contentsline {section}{\numberline {5}SDC Detection in Adaptive Solver}{26}{section.5}
\contentsline {subsection}{\numberline {5.1}Simulations}{26}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Resilience of Adaptive Controllers}{27}{subsection.5.2}
\contentsline {subsubsection}{\numberline {5.2.1}Inherent Resilience}{27}{subsubsection.5.2.1}
\contentsline {subsubsection}{\numberline {5.2.2}Significant SDCs Not Detected}{29}{subsubsection.5.2.2}
\contentsline {subsection}{\numberline {5.3}Resilience method for adaptive solvers}{30}{subsection.5.3}
\contentsline {subsubsection}{\numberline {5.3.1}Double-checking based on Lagrange interpolating polynomials}{31}{subsubsection.5.3.1}
\contentsline {subsubsection}{\numberline {5.3.2}Integration-based double-checking}{31}{subsubsection.5.3.2}
\contentsline {subsection}{\numberline {5.4}Experiments}{33}{subsection.5.4}
\contentsline {subsubsection}{\numberline {5.4.1}Cluster}{34}{subsubsection.5.4.1}
\contentsline {subsubsection}{\numberline {5.4.2}Detection accuracy}{35}{subsubsection.5.4.2}
\contentsline {subsubsection}{\numberline {5.4.3}Overheads}{35}{subsubsection.5.4.3}
\contentsline {subsubsection}{\numberline {5.4.4}Scalability}{36}{subsubsection.5.4.4}
\contentsline {subsection}{\numberline {5.5}Conclusion}{36}{subsection.5.5}
\contentsline {section}{\numberline {6}Conclusion}{38}{section.6}
\contentsline {section}{\numberline {7}References}{39}{section.7}
\contentsline {section}{\numberline {8}Appendix}{43}{section.8}
