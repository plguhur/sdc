\contentsline {section}{\numberline {1}Introduction}{3}{section.1}
\contentsline {subsection}{\numberline {1.1}Application Scenario}{3}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Contributions and organizations}{3}{subsection.1.2}
\contentsline {section}{\numberline {2}Related Work}{4}{section.2}
\contentsline {subsection}{\numberline {2.1}Resilience in High-Performance Computing}{4}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Numerical Integration Solver}{4}{subsection.2.2}
\contentsline {subsubsection}{\numberline {2.2.1}Differential equation}{5}{subsubsection.2.2.1}
\contentsline {subsubsection}{\numberline {2.2.2}Single-step and multi-step methods}{5}{subsubsection.2.2.2}
\contentsline {subsubsection}{\numberline {2.2.3}Implicit and explicit methods}{5}{subsubsection.2.2.3}
\contentsline {subsubsection}{\numberline {2.2.4}Function evaluations}{6}{subsubsection.2.2.4}
\contentsline {subsubsection}{\numberline {2.2.5}Approximation error}{6}{subsubsection.2.2.5}
\contentsline {subsubsection}{\numberline {2.2.6}Estimation of the approximation error}{6}{subsubsection.2.2.6}
\contentsline {subsubsection}{\numberline {2.2.7}Adaptive solvers}{8}{subsubsection.2.2.7}
\contentsline {subsection}{\numberline {2.3}Resilience to SDCs}{10}{subsection.2.3}
\contentsline {subsubsection}{\numberline {2.3.1}Generic solutions}{10}{subsubsection.2.3.1}
\contentsline {subsubsection}{\numberline {2.3.2}Algorithmic resilience}{10}{subsubsection.2.3.2}
\contentsline {subsubsection}{\numberline {2.3.3}Fixed numerical integration solvers}{11}{subsubsection.2.3.3}
\contentsline {subsubsection}{\numberline {2.3.4}Consequences of SDCs in Numerical Integration Solvers}{11}{subsubsection.2.3.4}
\contentsline {subsection}{\numberline {2.4}SDC Injector}{11}{subsection.2.4}
\contentsline {section}{\numberline {3}Model and Assumptions}{12}{section.3}
\contentsline {subsection}{\numberline {3.1}Silent Data Corruption Model}{12}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Objectives}{12}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Workflow}{13}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Assumption on the solver}{13}{subsection.3.4}
\contentsline {section}{\numberline {4}SDC Detection in Fixed Solver}{14}{section.4}
\contentsline {subsection}{\numberline {4.1}The proposed \emph {Hot Rod} method}{14}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}First detector: \emph {Hot Rod HR}}{14}{subsection.4.2}
\contentsline {subsubsection}{\numberline {4.2.1}Threshold function}{14}{subsubsection.4.2.1}
\contentsline {subsubsection}{\numberline {4.2.2}Detection of the significant SDC}{15}{subsubsection.4.2.2}
\contentsline {subsubsection}{\numberline {4.2.3}A reliable training set}{15}{subsubsection.4.2.3}
\contentsline {subsubsection}{\numberline {4.2.4}Adaptive control}{16}{subsubsection.4.2.4}
\contentsline {subsection}{\numberline {4.3}Second detector: \emph {Hot Rod LFP}}{16}{subsection.4.3}
\contentsline {subsection}{\numberline {4.4}Algorithm}{17}{subsection.4.4}
\contentsline {subsection}{\numberline {4.5}Experiments and results}{17}{subsection.4.5}
\contentsline {subsubsection}{\numberline {4.5.1}Environment}{17}{subsubsection.4.5.1}
\contentsline {subsubsection}{\numberline {4.5.2}Benchmark}{18}{subsubsection.4.5.2}
\contentsline {subsubsection}{\numberline {4.5.3}Results}{19}{subsubsection.4.5.3}
\contentsline {subsection}{\numberline {4.6}Conclusion}{20}{subsection.4.6}
\contentsline {section}{\numberline {5}SDC Detection in Adaptive Solver}{21}{section.5}
\contentsline {subsection}{\numberline {5.1}Inherent Detection}{21}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Resilience Method for Adaptive Solvers}{22}{subsection.5.2}
\contentsline {subsubsection}{\numberline {5.2.1}BDF-based double-checking}{22}{subsubsection.5.2.1}
\contentsline {subsubsection}{\numberline {5.2.2}Extrapolation-based double-checking}{23}{subsubsection.5.2.2}
\contentsline {subsubsection}{\numberline {5.2.3}Extension of AID to adaptive solvers}{23}{subsubsection.5.2.3}
\contentsline {subsubsection}{\numberline {5.2.4}Implementation}{24}{subsubsection.5.2.4}
\contentsline {subsection}{\numberline {5.3}Experiments}{25}{subsection.5.3}
\contentsline {subsubsection}{\numberline {5.3.1}Rising thermal bubble}{25}{subsubsection.5.3.1}
\contentsline {subsubsection}{\numberline {5.3.2}Cluster}{26}{subsubsection.5.3.2}
\contentsline {subsubsection}{\numberline {5.3.3}Methodology}{26}{subsubsection.5.3.3}
\contentsline {subsection}{\numberline {5.4}Results}{26}{subsection.5.4}
\contentsline {subsubsection}{\numberline {5.4.1}Corruption of the Classic Adaptive Controller}{26}{subsubsection.5.4.1}
\contentsline {subsubsection}{\numberline {5.4.2}BDF-based Double-checking}{27}{subsubsection.5.4.2}
\contentsline {subsubsection}{\numberline {5.4.3}Overheads}{28}{subsubsection.5.4.3}
\contentsline {subsubsection}{\numberline {5.4.4}Scalability}{28}{subsubsection.5.4.4}
\contentsline {subsection}{\numberline {5.5}Conclusion}{30}{subsection.5.5}
\contentsline {section}{\numberline {6}Conclusion}{30}{section.6}
\contentsline {section}{\numberline {7}References}{31}{section.7}
\contentsline {section}{\numberline {8}Appendix}{31}{section.8}
