# inject errors
# pierre-louis.guhur@laposte.net
# created: 1.5.16

from math import *
import numpy as np
import random
from ctypes import *
from struct import pack,unpack

PROBA_NON_SYSTEMATIC = 0#8e-5; 

def bitflip(x,pos): 
    fs = pack('d',x)
    bval = list(unpack('BBBBBBBB',fs))
    [q,r] = divmod(pos,8)
    bval[q] ^= 1 << r
    fs = pack('BBBBBBBB', *bval)
    fnew=unpack('d',fs)
    return fnew[0]
    
def singleBits(x):
    pos = random.randint(0,63)
    x = bitflip(x,pos)
    return [x,pos]
	  
def injectionNonSystematic(y):
    y_corrupted = y
    bit = -1
    GT = False
    if random.random() < PROBA_NON_SYSTEMATIC:
    	[y_corrupted, bit] = singleBits(y)
    	GT = True
    return [y_corrupted, bit, GT]