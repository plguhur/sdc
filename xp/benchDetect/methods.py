# methods for solving ODE
# pierre-louis.guhur@laposte.net
# created: 6.1.16

from math import *
import numpy as np

       
class App:
    
    def SetApplication(self, app):
        self.app = app
        self.k = np.zeros((self.GetNumberOfStages(),self.app.GetDimension()))
        self.NaN = np.ones(self.app.GetDimension())*float('nan')

    def SetStepsize(self,h):
        self.h = h
        
    def GetNumberOfStages(self):
        return self.Nstages
    
    def ComputeStage(self, n,t,x):
        s = np.zeros(self.app.GetDimension())
        for j in range(self.app.GetDimension()):
            l_x = [self.a[n-1,i]*self.k[i][j] for i in range(n)]
            s[j] = sum(l_x)
            
        self.k[n] = self.app.Function(t+self.c[n]*self.h, x+self.h*s)
        return self.k[n]
    
    def ComputeStageX(self, n,t,x,h,k):
       """ compute stage n but with a special stepsize and without storing the stage"""
       s = np.zeros(self.app.GetDimension())
       for j in range(self.app.GetDimension()):
           l_x = [self.a[n-1,i]*k[i][j] for i in range(n)]
           s[j] = sum(l_x)
    
       k[n] = self.app.Function(t+self.c[n]*self.h, x+self.h*s)
       return k
       
        
    def SetStage(self,n,k):
        self.k[n] = k
    
    def ComputeSolution(self,x):
        s = np.zeros(self.app.GetDimension())
        for j in range(self.app.GetDimension()):
            l_x = [self.b1[i]*self.k[i][j] for i in range(self.Nstages)]
            s[j] = sum(l_x)
        self.x1 = x + self.h*s
        
        for j in range(self.app.GetDimension()):
            l_x = [self.b2[i]*self.k[i][j] for i in range(self.Nstages)]
            s[j] = sum(l_x)
        self.x2 = x + self.h*s
        
        return [self.x1, self.x2]
        
    def ComputeSolutionX(self,x,h,k):
        s = np.zeros(self.app.GetDimension())
        for j in range(self.app.GetDimension()):
            l_x = [self.b1[i]*k[i][j] for i in range(self.Nstages)]
            s[j] = sum(l_x)
        x1 = x + h*s
        
        return x1
        
    def ComputeRichEst(self,t,x):
        """ compute the richardson estimate
        /!\ the real solution has to be already computed
        """
        
        # sol t -> t + h/2 
        h = self.h/2
        k = np.zeros((self.Nstages,self.app.GetDimension()))
        for i in range(self.Nstages):
            k= self.ComputeStageX(i,t,x,h,k)
        x_half = self.ComputeSolutionX(x,h,k)
        
        # sol t + h/2 -> t + h
        k = np.zeros((self.Nstages,self.app.GetDimension()))
        for i in range(self.Nstages):
            k = self.ComputeStageX(i,t+h,x_half,h,k)
        x_tile = self.ComputeSolutionX(x_half,h,k)
        
        # est
        est = (self.x1-x_tile)/(1-2**(-self.order))
        
        return est
        
    def SetSolutions(self,x1,x2):
         self.x1 = x1
         self.x2 = x2

class HeunEuler1(App):
    # propagate Euler's method x1
    # estimate with Heun's method x2
    order = 1
    Nstages = 2

    
    x2_prev = [float('nan')]
    x2_pprev = [float('nan')]
    
    a = np.array([[1.0]])
    b2 = np.array([.5, .5])
    b1 = np.array([1.0, .0])
    c = np.array([.0, 1.0])
    
    def ComputeEmbEst(self):
        return self.x2 - self.x1
        
    def ComputeScndEst(self):
    	if not isnan(self.x2_pprev[0]):
    	    quad_est = +(self.x2-2.0*self.x2_prev + self.x2_pprev)*.5
        else:
            quad_est = self.NaN
        return quad_est
        
    def Update(self):
        self.x2_pprev = self.x2_prev
        self.x2_prev = self.x2
        
class HeunEuler2(App):
    # propagate Heun's method x1
    # estimate with Euler's method x2
    order = 2
    Nstages = 2
    
    x1_prev = [float('nan')]
    x1_pprev = [float('nan')]
    
    a = np.array([[1.0]])
    b1 = np.array([.5, .5])
    b2 = np.array([1.0, .0])
    c = np.array([.0, 1.0])
  
    def ComputeEmbEst(self):
        return self.x1 - self.x2
        
    def ComputeScndEst(self):
    	if not isnan(self.x1_pprev[0]):
    	    quad_est = +(self.x1-2.0*self.x1_prev + self.x1_pprev)*.5
        else:
            quad_est = self.NaN
        return quad_est
        
    def Update(self):
        self.x1_pprev = self.x1_prev
        self.x1_prev = self.x1
    

class RalstonEuler(App):
    """
    propagate Ralston's method x1
    estimate with Euler's method x2
    
    butcher:
    0   | 0
    2/3 | 2/3
    -----------
       | 1/4 3/4
       | 1   0
    -----------
    | -3/4 3/4
    """
    
    order = 2
    Nstages = 2
    
    x1_prev = [float('nan')]
    x1_pprev = [float('nan')]
    
    a = np.array([[2.0/3.0]])
    b1 = np.array([.25, .75])
    b2 = np.array([1.0, .0])
    c = np.array([.0, 2.0/3.0])
    
    def ComputeEmbEst(self):
        return self.x1 - self.x2
        
    def ComputeScndEst(self):
    	if not isnan(self.x1_pprev[0]):
    	    quad_est = +(self.x1-2.0*self.x1_prev + self.x1_pprev)*.5
        else:
            quad_est = self.NaN
        return quad_est
        
    def Update(self):
        self.x1_pprev = self.x1_prev
        self.x1_prev = self.x1
    


class BogackiShampine(App):
    """
    propagate 2rd method x1
    estimate with 3rd method x2
    
    method 2(3)
        butcher:
        0   | 0
        1/2 | 1/2
        3/4 | 0   3/4
        1   | 2/9 1/3 4/9
        -----------
           | 2/9 1/3 4/9 0 __ 3rd order
           | 7/24 1/4 1/3 1/8 __ 2nd order
        -----------
    """
    
    order = 3
    Nstages = 4
    
    x_prev = [float('nan')]
    x_pprev = [float('nan')]
    f_prev = [float('nan')]
    f_pprev = [float('nan')]
    
    a = np.array([[.5,.0,.0],[.0, .75,.0],[2.0/9.0,1.0/3.0,4.0/9.0]])
    b2 = np.array([7.0/24.0, .25, 1.0/3.0, .125])
    b1 = np.array([2.0/9.0,1.0/3.0,4.0/9.0,.0])
    c = np.array([.0, .5, .75, 1.0])
    
    def ComputeEmbEst(self):
        return self.x2 - self.x1
        
    def ComputeScndEst(self):
        if not isnan(self.x_pprev[0]):
#            quad_est = self.h/3.0*(2.0*f_nMinus1 + f_nMinus2) + \
 #                    (5.0*self.x_pprev - 4.0*self.x_prev - self.x1)/6.0
            quad_est = self.h/6.0*(self.k[3] + 4.0*self.k[0] + self.f_prev) + \
                     (self.x_pprev - self.x1)/2.0
        else:
           quad_est = self.NaN
    	return quad_est
        
    def Update(self):
        self.f_prev = self.k[0]
        self.x_pprev = self.x_prev
        self.x_prev = self.x1


class CashKarp(App):
    """
    propagate 2rd method x1
    estimate with 3rd method x2
    
    method 2(3)
        butcher:
        0   | 0
        1/2 | 1/2
        3/4 | 0   3/4
        1   | 2/9 1/3 4/9
        -----------
           | 2/9 1/3 4/9 0 __ 3rd order
           | 7/24 1/4 1/3 1/8 __ 2nd order
        -----------
    """
    
    order = 3
    Nstages = 4
    
    x_prev = [float('nan')]
    x_pprev = [float('nan')]
    f_prev = [float('nan')]
    f_pprev = [float('nan')]
    
    a = np.array([[.5,.0,.0],[.0, .75,.0],[2.0/9.0,1.0/3.0,4.0/9.0]])
    b2 = np.array([7.0/24.0, .25, 1.0/3.0, .125])
    b1 = np.array([2.0/9.0,1.0/3.0,4.0/9.0,.0])
    c = np.array([.0, .5, .75, 1.0])
    
    def ComputeEmbEst(self):
        return self.x2 - self.x1
        
    def ComputeScndEst(self):
        if not isnan(self.x_pprev[0]):
#            quad_est = self.h/3.0*(2.0*f_nMinus1 + f_nMinus2) + \
 #                    (5.0*self.x_pprev - 4.0*self.x_prev - self.x1)/6.0
            quad_est = self.h/6.0*(self.k[3] + 4.0*self.k[0] + self.f_prev) + \
                     (self.x_pprev - self.x1)/2.0
        else:
           quad_est = self.NaN
    	return quad_est
        
    def Update(self):
        self.f_prev = self.k[0]
        self.x_pprev = self.x_prev
        self.x_prev = self.x1


class DormandPrince(App):
    """
    propagate 4th method x1
    estimate with 5th method x2
    
    ref paper: Dormand, J. R.; Prince, P. J. (1980), 
                A family of embedded Runge-Kutta formulae 
                Journal of Computational and Applied Mathematics
    called RK5(4)7M
    """
    
    order = 4
    Nstages = 7
    
    x_prev = [float('nan')]
    x_pprev = [float('nan')]
    x_ppprev = [float('nan')]
    f_ppprev = [float('nan')]
    f_prev = [float('nan')]
    f_pprev = [float('nan')]
    
    a = np.array([[.2,.0,.0, .0, .0, .0], \
            [3.0/40.0, 9.0/40.0, .0, .0, .0, .0],
            [44.0/45.0,-56.0/15.0,32.0/9.0, .0, .0, .0], \
            [19372.0/6561.0,-25360.0/2187.0,64448.0/6561.0,-212.0/729.0,.0,.0], \
            [9017.0/3168.0, -355.0/33.0, 46732.0/5247.0, 49.0/176.0, -5103.0/18656.0, .0], \
            [35.0/384.0, .0, 500.0/1113.0, 125.0/192.0, -2187.0/6784.0, 11.0/84.0] ])       
    b1 = np.array([5179.0/57600.0, .0, 7571.0/16695.0, 393.0/640.0, -92097.0/339200.0, 187.0/2100.0, 1.0/40.0])
    b2 = np.array([35.0/384.0, .0, 500.0/1113.0, 125.0/192.0, -2187.0/6784.0, 11.0/84.0, 0.0])
    c = np.array([.0, .2, 3.0/10.0, 0.8, 8.0/9.0, 1.0, 1.0])
    
    def ComputeEmbEst(self):
        return self.x2 - self.x1
        
    def ComputeScndEst(self):
        if not isnan(self.x_ppprev[0]):
            f_nMinus3 = self.f_pprev
            f_nMinus2 = self.f_prev
            f_nMinus1 = self.k[0]
            quad_est = self.h/10.0*(f_nMinus3 + 3.0*f_nMinus1 + 6.0*f_nMinus2) \
              + 1.0/30.0*(-self.x1 - 18.0*self.x_prev + 9.0*self.x_pprev + 10.0*self.x_ppprev)
        else:
           quad_est = self.NaN
    	return quad_est
        
    def Update(self):
        self.f_ppprev = self.f_pprev
        self.f_pprev = self.f_prev
        self.f_prev = self.k[0]
        self.x_ppprev = self.x_pprev
        self.x_pprev = self.x_prev
        self.x_prev = self.x1
        
        

class RK41(App):
    """
    propagate 4th order x1 
    estimate with 1st order x2
    """
    
    order = 4
    Nstages = 4
    
    x_p1 = [float('nan')]
    x_p2 = [float('nan')]
    x_p3 = [float('nan')]
    x_p4 = [float('nan')]
    x_p5 = [float('nan')]
    
    a = np.array([[.5,.0,.0],[0.0,.5,.0],[.0,.0,1.0]])
    b1 = np.array([1.0/6.0,1.0/3.0,1.0/3.0,1.0/6.0])
    b2 = np.array([1.0, .0,.0,.0,.0])
    c = np.array([.0, .5,.5,1.0])
    ab = np.array([0.0])
  
    def ComputeEmbEst(self):
        return self.x1 - self.x2
        
    def ComputeScndEst(self):
    	if not isnan(self.x_p5[0]):
#    	    quad_est = +(7.0*self.x1-18.0*self.x1_prev + 15.0*self.x1_pprev-4.0*self.x1_ppprev)/6.0
#            quad_est = +(self.x1-2.0*self.x1_prev + self.x1_pprev)*.5
            quad_est = +(23.0*self.x_p1-72.0*self.x_p2 + 84.0*self.x_p3 - 44*self.x_p4 + 9*self.x_p5)/12.0
        else:
            quad_est = self.NaN
#            sys.stdout.write(".")
        return quad_est
        
    def Update(self):
        self.x_p5 = self.x_p4
        self.x_p4 = self.x_p3
        self.x_p3 = self.x_p2
        self.x_p2 = self.x_p1
        self.x_p1 = self.x1

class RK41c(App):
    """
    propagate 4th order x1 
    estimate with 1st order x2
    """
    
    order = 4
    Nstages = 4
    
    x_p1 = [float('nan')]
    x_p2 = [float('nan')]
    x_p3 = [float('nan')]
    x_p4 = [float('nan')]
    x_p5 = [float('nan')]
    
    a = np.array([[.5,.0,.0],[0.0,.5,.0],[.0,.0,1.0]])
    b1 = np.array([1.0/6.0,1.0/3.0,1.0/3.0,1.0/6.0])
    b2 = np.array([1.0, .0,.0,.0,.0])
    c = np.array([.0, .5,.5,1.0])
    ab = np.array([0.0])
  
    def ComputeEmbEst(self):
        return self.x1 - self.x2
        
    def ComputeScndEst(self):
    	if not isnan(self.x_p5[0]):
#    	    quad_est = +(7.0*self.x1-18.0*self.x1_prev + 15.0*self.x1_pprev-4.0*self.x1_ppprev)/6.0
#            quad_est = +(self.x1-2.0*self.x1_prev + self.x1_pprev)*.5
            quad_est = +(23.0*self.x_p1-72.0*self.x_p2 + 84.0*self.x_p3 - 44*self.x_p4 + 9*self.x_p5)/12.0
        else:
            quad_est = self.NaN
#            sys.stdout.write(".")
        return quad_est
        
    def Update(self):
        self.x_p5 = self.x_p4
        self.x_p4 = self.x_p3
        self.x_p3 = self.x_p2
        self.x_p2 = self.x_p1
        self.x_p1 = self.x1
          
class RK31(App):
     """
     propagate 3rd order x1 
     estimate with 1st order x2
     """
    
     order = 3
     Nstages = 3
     
     x1_prev = [float('nan')]
     x1_pprev = [float('nan')]
     x1_ppprev = [float('nan')]
        
     a = np.array([[.5,.0,.0],[-1.0,2.0,.0]])
     b1 = np.array([1.0/6.0,2.0/3.0,1.0/6.0])
     b2 = np.array([1.0, .0,.0,.0,.0])
     c = np.array([.0, .5,1.0])
     ab = np.array([0.0])
  
     def ComputeEmbEst(self):
         return self.x1 - self.x2
        
     def ComputeScndEst(self):
     	 if not isnan(self.x_p4[0]):
    	    quad_est = +(7.0*self.x_p1-18.0*self.x_p2 + 15.0*self.x_p3-4.0*self.x_p4)/6.0
             
         else:
             quad_est = self.NaN
         return quad_est
        
     def Update(self):
        self.x_p4 = self.x_p3
        self.x_p3 = self.x_p2
        self.x_p2 = self.x_p1
        self.x_p1 = self.x1
  
  