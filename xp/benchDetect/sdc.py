# this file updates all simulations and plots results
# pierre-louis.guhur@laposte.net
# created: 12.22.15

import os, sys
import itertools
import sdctools
import argparse
import shutil
import matplotlib.pyplot as plt

parser = argparse.ArgumentParser()
parser.add_argument('--reset',  action='store_true', 
                   help='Reset all simulations')
args = parser.parse_args()

res_folder = "results/"
simu_folder = "results/simulation"
rate_folder = "results/rate"
sol_folder = "results/solution/"
est_folder = "results/estimate/"
inj_folder = "results/inject/"
ini_file = "params.ini"



# check if environment is correct
if args.reset:
    print "Reset"
    shutil.rmtree(simu_folder, True)
    shutil.rmtree(rate_folder, True)
    shutil.rmtree(est_folder, True)
    shutil.rmtree(sol_folder, True)
    shutil.rmtree(inj_folder, True)
try:
    os.mkdir(res_folder)
except:
    print "Results' folders exist"
try:
    os.mkdir(sol_folder)
except:
    print "Solution' folders exist"
try:
    os.mkdir(simu_folder)
except:
    print "Simulations' folders exist"
try:
    os.mkdir(rate_folder)
except:
    print "Rates' folders exist"
try:
    os.mkdir(est_folder)
except:
    print "Estimates' folders exist"
try:
    os.mkdir(inj_folder)
except:
    print "Inject' folders exist"
if not os.path.isfile(ini_file):
  exit(ini_file + " does not exist!")
  
    
  
# load params and check they are admissible
[methods, injections, scenarii, applications,detectors,plots] = sdctools.initialization(ini_file)

# for all kind of simulations, update it
for application, method, injection, detector, scenario in itertools.product(applications, methods, injections, detectors, scenarii) :
    sdctools.launch(application, method, injection, scenario,detector,plots)

plt.show() 
      