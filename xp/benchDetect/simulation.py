# launch a simulation
# pierre-louis.guhur@laposte.net
# created: 12.23.15

from math import *
import numpy as np
import random
from output import *
from injection import *
from methods import *
NaN = float('nan')


class Simulation:
    """Parent class for all application"""
    k = 0.0
    Nfp = 0
    Nnegative = 0
    alpha = 1e-1
    output = Output()
    
    def IsDone(self):
        return self.output.Exist()
        
    def SetApplication(self, appl):
        self.app = appl
        self.output.SetApplication(appl)

    def SetDetector(self, detector):
         self.detector = detector
         self.output.SetDetector(detector)
      
    def SetMinIRE(self, minire):
         self.minire = minIRE

    def SetMaxIRE(self, maxire):
         self.maxire = maxIRE
         
            
    def SetMethod(self, method):
        if method == "HeunEuler1":
            self.method = HeunEuler1()
        elif method == "HeunEuler2":
            self.method = HeunEuler2()
        elif method == "RalstonEuler":
            self.method = RalstonEuler()
        elif method == "BogackiShampine":
            self.method = BogackiShampine()
        elif method == "DormandPrince":
            self.method = DormandPrince()
        elif method == "CashKarp":
            self.method = CashKarp()
        elif method == "RK41":
            self.method = RK41()
        elif method == "RK41c":
            self.method = RK41c()
        elif method == "RK31":
            self.method = RK31()            
        self.output.SetMethod(method)
        self.methodName = method;
        self.method.SetApplication(self.app)
        

                    
    def SetInjection(self, injection):
        self.injection = injection
        self.output.SetInjection(injection)
    
    def SetScenario(self, scenario):
        self.scenario = scenario
        self.output.SetScenario(scenario)
        
    def GetStepsizes(self):
        return self.app.GetStepsizes()
        
    def GetName(self):
        return self.app.GetName()
        
    def SetStepsize(self,stepsize):
        self.stepsize = stepsize
        self.output.SetStepsize(stepsize)
        self.method.SetStepsize(stepsize)
        
    def Step(self,t,x):
        GT = False
        for i in range(self.method.GetNumberOfStages()):
            k = self.method.ComputeStage(i,t,x)
            [k_corr, bit, GT_i] = injectionNonSystematic(k)
            self.method.SetStage(i,k_corr)
            GT += GT_i
            
        [x1,x2] = self.method.ComputeSolution(x)
            
        #corrupt estimating solution
        #[x1, bit, GT_i] = injectionNonSystematic(x1)
        #[x2, bit, GT_j] = injectionNonSystematic(x2)
        #GT += GT_i + GT_j
        #\
        self.method.SetSolutions(x1,x2)
        
        emb_est = self.method.ComputeEmbEst()
        quad_est = self.method.ComputeScndEst()
        rich_est = self.method.ComputeRichEst(t,x)
        
         
        return [emb_est, quad_est, rich_est, GT, x1, x2]
        
        
    def Simulate(self):
        if self.app.GetName() == "wrf":
            if hasattr(self,"minire") and hasattr(self,"maxire"):
                self.app.Launch(self.scenario, self.injection, self.methodName, self.detector, self.stepsize,self.maxire,self.minire)
            else:
                self.app.Launch(self.scenario, self.injection, self.methodName, self.detector, self.stepsize)
            return
            
        self.output.Start()
        
        [t0,x0] = self.app.GetInitialPoint()
        t_end = self.app.GetTimeLimit()
        [t,x] = [t0, x0]
        
        # learning phase
        # TO DO: inject errors on learning_set ??
        self.learning = True
        learning_set = []
        for i in range(0,5):
             # compute
             [emb_est, quad_est, rich_est, GT, x, x2] = self.Step(t,x)
             # we can only accept solution
             self.method.Update()
             t = t + self.stepsize
             if not isnan(quad_est[0]):
                 learning_set.append(self.Delta(emb_est, quad_est))
            
        self.learning = False
        self.Learning(learning_set)
        
        
        # application 
        self.needRecomputed = False
        self.recomputed = False
        self.lastGT = False
        t = t0
        x = x0
        while t <= t_end:
            [emb_est, quad_est, rich_est, GT, x1, x2] = self.Step(t,x)

            delta = self.Delta(emb_est, quad_est)
            detect1 = self.Detection1(delta)
            detect2 = False
            self.recomputed = self.needRecomputed
            self.needRecomputed = detect1 and not self.needRecomputed

            if detect1 and self.recomputed:
                detect2 = self.Detection2(delta) 
                if not detect2:
                    self.AdaptiveControl()
                    self.Nfp += 1
         

            if not self.needRecomputed:
                if not isnan(quad_est[0]):
                    self.output.Corrupt(detect1 or self.recomputed, GT or self.lastGT) # save flags
                    self.output.Estimates(emb_est, quad_est,rich_est, self.threshold1) # save estimates
                    sol = self.app.Solution(t)
                    self.output.Solution(x1,x2, sol)  # save solution if it is not NaN
                t = t + self.stepsize
                x = x1
                self.Nnegative += not detect1 and not detect2
                self.method.Update()
            self.lastGT = GT and self.needRecomputed

        # close
        self.output.End()   



            
    def Delta(self, a, b):
        return abs(abs(a) - abs(b) )#check if it + or -
         
    def AdaptiveControl(self):
        FPR = self.Nfp/self.Nnegative if self.Nnegative != 0 else 1
        if FPR > 0.03:
            self.k += 1
            self.threshold1 = [pow(1+self.alpha,self.k)*3*sig for sig in self.sigma]
        
  
    def Detection1(self, delta):
       res = False
       for d,t in zip(delta,self.threshold1):
           res |= d > t
       return res
          
    def Detection2(self, delta):
        res = False
        for d,t in zip(delta,self.threshold2):
            res |= d > t
        return res
    
    def Learning(self, learning_set):
        # compute sigma and C99 and threshold
        l = np.array(learning_set)
        dim = len(l[0])
        length = len(l)
        self.sigma = np.zeros(dim)
        self.percentile = np.zeros(dim)
        
        pond_set = np.array([[exp(-l[j,i])*l[j,i] for i in range(dim)] for j in range(length)])
        sum_weights = np.array([sum(exp(-l[j,i]) for j in range(length)) for i in range(dim)])
        self.sigma = [3*sum(pond_set[:,i])/length/sum_weights[i] for i in range(dim)]
        self.percentile = [10*np.percentile(pond_set[:,i],99) for i in range(dim)]
        
        self.threshold1 = self.sigma
        self.threshold2 = self.percentile
        
        print self.threshold1
        print self.threshold2
        
    # lists of methods. computation of estimates is inside
 