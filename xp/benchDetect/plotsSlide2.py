# this file plots results
# pierre-louis.guhur@laposte.net
# created: 4.1.16

import os, sys
import ConfigParser
import numpy as np
import matplotlib.pyplot as plt
from math import isnan, pow, sqrt,trunc 
from collections import OrderedDict
from matplotlib import rc
cmr = {'family':'sans-serif','sans-serif':['Computer Modern Roman']}
cmfont = {'fontname':'Computer Modern Roman'}
rc('font',**cmr)
## for Palatino and other serif fonts use:
#rc('font',**{'family':'serif','serif':['Palatino']})
rc('text', usetex=True)
earth_radius = 6371000 #meters
simu_folder = "results/simulation/"
rate_folder = "results/rate/"
est_folder = "results/estimate/"
sol_folder = "results/solution/"
inj_folder = "results/inject/"
res_folder = "results/"

def LTE(application, method, injection, scenario, detector, stepsize):
    """Plot Dist(LTE1,LTE2) with confidence interval"""
    
    # read results
    dim = 1
    app = application.GetName()
    corrupt_file = simu_folder + app + "_" + method + "_" + scenario + "_" + injection + "_" + detector + "_" + str(stepsize) + ".csv"
    est_file = est_folder + app + "_" + method + "_" + scenario + "_" + injection + "_" + detector + "_" + str(stepsize) + ".csv"    
    corrupt = np.genfromtxt(corrupt_file, delimiter=",", dtype=int)
    est = np.genfromtxt(est_file, delimiter=",", dtype='float128')    
    
    sur_fct, pred, sig = np.zeros(len(est)),np.zeros(len(est)),np.zeros(len(est))
    for j in range(len(est)):
        sur_fct[j] = sqrt(sum(est[j,i]*est[j,i] for i in range(dim) ))
        pred[j] = sqrt(sum(est[j,i]*est[j,i] for i in range(dim,2*dim) ))
        sig[j] = sqrt(sum(est[j,i]*est[j,i] for i in range(2*dim,3*dim) ))
    gt = np.array(corrupt[:,1], dtype=bool)
    detect =   np.array(corrupt[:,0], dtype=bool)
    abscis = np.arange(len(est))
    
    nocorr_delta = sur_fct[~gt]
    nocorr_abscis = abscis[~gt]
    corr_delta = sur_fct[gt]
    corr_abscis = abscis[gt]
    recover_steps = abscis[gt * detect]
    corr_steps = abscis[gt * ~detect]
    
    # plot y = f(x)
    fig = plt.figure()
    ax = plt.axes()
    for x in corr_steps:
       ax.axvline(x, linestyle='--', color='r')
    for x in recover_steps:
        ax.axvline(x, linestyle='--', color='g')
    plt.plot(nocorr_abscis, nocorr_delta, 'b+', label='Non corrupted delta')
    plt.plot(corr_abscis, corr_delta, 'rv', label='Corrupted delta')
    plt.xlabel('$Time$')
    plt.ylabel('$Samples$')
    plt.fill(np.concatenate([abscis, abscis[::-1]]),
    np.concatenate([(pred - sig),
    	      (pred + sig)[::-1]]),
    alpha=.1, fc='c', ec='None', label='Confidence interval')
    
    #plt.xlim([0.0, 1.e-1])
    plt.ylim([0, min(max(nocorr_delta), 1)])
    plt.legend(loc='upper right')
    plt.title('Correlation between both estimates')
 #   plt.show()
    
def Errors(application, method, injection, scenario, detector, stepsize):
    """Plot LTE1,LTE2 and GT (if available) """
    
    # read results
    app = application.GetName()
    dim = application.GetDimension()
    corrupt_file = simu_folder + app + "_" + method + "_" + scenario + "_" + injection + "_" + detector + "_" + str(stepsize) + ".csv"
    est_file = est_folder + app + "_" + method + "_" + scenario + "_" + injection + "_" + detector + "_" + str(stepsize) + ".csv"    
    err_file = sol_folder + app + "_" + method + "_" + scenario + "_" + injection + "_" + detector + "_" + str(stepsize) + ".csv"    
    corrupt = np.genfromtxt(corrupt_file, delimiter=",", dtype=int)
    est = np.genfromtxt(est_file, delimiter=",")
    
    sol = np.genfromtxt(err_file, delimiter=",")  
    delta,x2,err,lte1,lte2,lte3,lte4,sig = np.zeros(len(sol)),np.zeros(len(sol)),np.zeros(len(sol)),np.zeros(len(sol)),np.zeros(len(sol)), \
                                        np.zeros(len(sol)),np.zeros(len(sol)),np.zeros(len(sol))
    if len(sol[0]) > 2*dim:
        for j in range(len(sol)):  
            # x2[j] = abs(sol[j,dim])
#err[j] = abs(sol[j,0] - sol[j,dim])/x2[j]
            x2[j] = sqrt(sum(sol[j,i]*sol[j,i] for i in range(dim,2*dim) ))
            err[j] = sqrt( sum( pow( abs(sol[j,i] - sol[j,dim+i])/x2[j],2) for i in range(dim) ))
    for j in range(len(sol)):
        sig[j] = sqrt(sum(est[j,i]*est[j,i] for i in range(3*dim,4*dim) ))
        lte1[j] = sqrt(sum(est[j,i]*est[j,i] for i in range(dim) ))
        lte2[j] = sqrt(sum(est[j,i]*est[j,i] for i in range(dim,2*dim) ))
        lte3[j] = sqrt(sum(est[j,i]*est[j,i] for i in range(2*dim,3*dim) ))
        delta[j] = sqrt(sum((est[j,i]-est[j,i+dim])**2 for i in range(dim) ))
    gt = np.array(corrupt[:,1], dtype=bool)
    detect =   np.array(corrupt[:,0], dtype=bool)
    
    abscis = np.arange(len(lte1))
    gt_nsys = np.array(corrupt[:,1], dtype=bool)
    gt_sys = np.array(corrupt[:,3], dtype=bool)
    det_nsys =   np.array(corrupt[:,0], dtype=bool)
    det_sys =   np.array(corrupt[:,2], dtype=bool)
    recNSys_steps = abscis[gt_nsys * det_nsys]
    recSys_steps = abscis[gt_sys * det_sys]
    corrSys_steps = abscis[gt_sys * ~det_sys]
    corrNSys_steps = abscis[gt_nsys * ~det_nsys]
    
    # plot 
    fig = plt.figure()
    ax = plt.axes()
    for x in corrNSys_steps:
       ax.axvline(x, linestyle='--', color='r',label="Uncorrected non syst. SDC" if x == corrNSys_steps[0] else "")
    for x in recNSys_steps:
        ax.axvline(x, linestyle='--', color='g',label="Corrected non syst. SDC" if x == recNSys_steps[0] else "")
    for x in corrSys_steps:
       ax.axvline(x, linestyle='-', alpha=0.1,color='r',label="Undetected syst. SDC" if x == corrSys_steps[0] else "")
    for x in recSys_steps:
        ax.axvline(x, linestyle='-', alpha=0.1, color='g',label="Detected syst. SDC" if x == recSys_steps[0] else "")
        
    plt.plot(abscis, lte1, 'b-', label='LTE1')
    plt.plot(abscis, lte2, 'c-', label='LTE2')
    plt.plot(abscis, lte1, 'b+')
    plt.plot(abscis, lte2, 'c+')
        
    plt.plot(abscis, lte3, '-',color="chartreuse", label='LTE3')
    # plt.plot(abscis, lte4, '-',color="chartreuse", label='LMM estimate')
    plt.plot(abscis, lte3, '+',color="chartreuse")
    # plt.plot(abscis, lte4, '+',color="chartreuse")
    
    
    if len(sol[0]) > 2*dim:
        plt.plot(abscis, err, 'r+', label='Rel. error')
    plt.plot(abscis, delta, 'm+', label='$|LTE1|-|LTE2|$')
    plt.xlabel('$Time$')
    plt.ylabel('$Errors$')

    plt.fill(np.concatenate([abscis, abscis[::-1]]),
        np.concatenate([np.zeros(len(delta)),
              (sig)[::-1]]),
       alpha=.1, fc='c', ec='None', label='Confidence interval')
    plt.ylim([0.0, min(max(lte2),1e2)])
    plt.legend(loc='upper right')
    plt.title('Errors for ' + app + "_" + method + "_" + scenario + "_" + injection + "_" + str(stepsize) )
    #plt.show()
    
     
def Solution(application, method, injection, scenario, detector, stepsize):
    """Plot x1,x2 and GT (if available) """
    cmrfont = {'fontname':'Computer Roman Modern'}
    hfont = {'fontname':'Helvetica'}
    plt.rc('font',family='Computer Modern Roman')
    if scenario == "NoError" or scenario == "NoCorrection":
        return

    # read results
    dim = application.GetDimension()
    app = application.GetName()
    
    # files & data
    corrupt_file = simu_folder + app + "_" + method + "_" + scenario + "_" + injection + "_" + detector + "_"+ str(stepsize) + ".csv"
    err_file = sol_folder + app + "_" + method + "_" + scenario + "_" + injection + "_"+ detector + "_" + str(stepsize) + ".csv"    
    noerr_file = sol_folder + app + "_" + method +"_NoError_"  + injection + "_"+ detector + "_" + str(stepsize) + ".csv"    
    nocorr_file = sol_folder + app + "_" + method + "_NoCorrection_" + injection + "_" + detector + "_"+ str(stepsize) + ".csv"    
    ben_file = sol_folder + app + "_" + method + "_" + scenario + "_" + injection + "_" + "Benson" + "_"+ str(stepsize) + ".csv"    
    aid_file = sol_folder + app + "_" + method + "_" + scenario + "_" + injection + "_" + "AID" + "_"+ str(stepsize) + ".csv"    
    highsens_file = sol_folder + app + "_" + method + "_" + scenario +"_" +  injection + "_" + "HighSens" + "_"+ str(stepsize) + ".csv"    
    lowfp_file = sol_folder + app + "_" + method + "_" + scenario +"_" +  injection + "_" + "LowFP" + "_"+ str(stepsize) + ".csv"    
    corrupt = np.genfromtxt(corrupt_file, delimiter=",", dtype=int)
    name_file = [noerr_file]#,nocorr_file,ben_file]#, aid_file,highsens_file,lowfp_file]
    legend = ["No error", "No correction", "No correction", "AID", "H.R. HR", "H.R. LFP"]
    flag = [True if os.path.isfile(file)  else False for file in name_file]#and file != err_file
    data = [np.genfromtxt(name_file[i], delimiter=",") if flag[i] else np.zeros(1) for i in range(len(name_file))]
    curve = []
    colors = ["g", "orangered","magenta","midnightblue","chocolate","rosybrown"]
    markers= ["D", "","","","p","o"]
    styles= ['-','-','-','-',':','-']
    for d in data:
         a =  np.zeros(len(d))
         if len(d) > 1:
             for j in range(len(d)):
                  a[j] = sqrt(sum(d[j,i]*d[j,i] for i in range(dim) )) - earth_radius
         curve.append(a)

    sol = np.genfromtxt(err_file, delimiter=",")    
    x1,x2,gt = np.zeros(len(sol)),np.zeros(len(sol)),np.zeros(len(sol))

    if len(sol[0]) > 2*dim:
        for j in range(len(sol)):
            # gt[j] = abs(sol[j,2*dim])
            gt[j] = sqrt(sum(sol[j,i]*sol[j,i] for i in range(2*dim,3*dim) ))

    
    abscis = np.arange(len(x1))
    gt_nsys = np.array(corrupt[:,1], dtype=bool)
    gt_sys = np.array(corrupt[:,3], dtype=bool)
    det_nsys =   np.array(corrupt[:,0], dtype=bool)
    det_sys =   np.array(corrupt[:,2], dtype=bool)
    recNSys_steps = abscis[gt_nsys * det_nsys]
    recSys_steps = abscis[gt_sys * det_sys]
    corrSys_steps = abscis[gt_sys * ~det_sys]
    corrNSys_steps = abscis[gt_nsys * ~det_nsys]
    gtNSys_steps = abscis[gt_nsys]
    
    # plot 
    fig = plt.figure()
    ax = plt.axes()
    ax.get_yaxis().get_major_formatter().set_useOffset(False)
    # for x in corrNSys_steps:
    #    ax.axvline(x, linestyle='--', color='r',label="Uncorrected non syst. SDC" if x == corrNSys_steps[0] else "")
    # for x in recNSys_steps:
    #     ax.axvline(x, linestyle='--', color='g',label="Corrected non syst. SDC" if x == recNSys_steps[0] else "")
    # for x in corrSys_steps:
    #     ax.axvline(x, linestyle=':', color='r',label="Uncorrected syst. SDC" if x == corrSys_steps[0] else "")
    #for x in  gtNSys_steps:
    #    ax.axvline(x, linestyle='--', color='k',label="SDC" if x == gtNSys_steps[0] else "")

    #        ax = plt.gca()
    if flag[0]: #no error
        max_y = max(curve[0])
        min_y = min(curve[0])
        #get lte 
        #lte_file = est_folder + app + "_" + method +"_NoError_"  + injection + "_"+ detector + "_" + str(stepsize) + ".csv"   
        #lte = np.genfromtxt(lte_file, delimiter=",", dtype=int)[0]
        sig = 1e-6*6.3775e6*np.ones(len(x1))
        #plt.fill(np.concatenate([abscis, abscis[::-1]]),
        #        np.concatenate([(curve[0] - sig),
        #	      (curve[0] + sig)[::-1]]),
        #          alpha=.3, fc='c', ec='None', label='$\pm$ LTE')
    for i in range(0,len(name_file)):
        if flag[i]:
            plt.plot(curve[i], styles[i], label=legend[i] if markers[i] == "" else "",color =colors[i])
    for i in range(0,len(name_file)):
        if flag[i] and markers[i] != '':
            subset = range(i*5, len(curve[i]))
	    for x in subset:
		        ax.axvline(x, linestyle='--', color='k',label="SDC" if x == subset[0] else "")
            plt.plot(subset, curve[i][subset[:]], ' ',marker=markers[i], label=legend[i],color =colors[i])
    
    # add legen
    # for i in range(1,len(name_file)):
   #      if flag[i] and markers[i] != "":
   #          plt.plot(curve[i][0], '-', label=legend[i] if markers[i] == "" else "",color =colors[i])
    
#    plt.plot(abscis, x2, 'm-', label='sol 2')
   # plt.plot(abscis, x1, '-', color="midnightblue", label=detector)
    

    if len(sol[0]) > 2*dim:
        plt.plot(abscis, gt, 'r+', label='Ground truth')
    plt.xlabel('Step')
    plt.ylabel('Solution (m)')
    #plt.ylim([max(min_y,1e5), min(max_y,1e7)])
   # plt.legend(loc="upper left",ncol=2,prop={'size':11})
    # plt.title('Solution for ' + app + " " + method + " " + scenario + " " + injection + " " + str(stepsize) )
    #plt.show()
       
       
def LTEvsLTE(application, method, injection, scenario,detector, stepsize):
    """Plot LTE1 as a function of LTE2 with confidence interval"""
    
    # read results
    dim = application.GetDimension()
    app = application.GetName()
    corrupt_file = simu_folder + app + "_" + method + "_" + scenario + "_" + injection + "_"+ detector + "_" + str(stepsize) + ".csv"
    est_file = est_folder + app + "_" + method + "_" + scenario + "_" + injection + "_" + detector + "_"+ str(stepsize) + ".csv"    
    err_file = sol_folder + app + "_" + method + "_" + scenario + "_" + injection + "_" + detector + "_"+ str(stepsize) + ".csv"    
    corrupt = np.genfromtxt(corrupt_file, delimiter=",", dtype=int)
    est = np.genfromtxt(est_file, delimiter=",")
    
    lte1,lte2,ci = np.zeros(len(est)),np.zeros(len(est)),np.zeros(len(est))  
    for j in range(len(est)):
        # lte1[j] = est[j,0] # first component
        # lte2[j] = est[j,dim]
        # ci[j] = est[j,2*dim] # confidence interval
        lte1[j] = sqrt(sum(est[j,i]*est[j,i] for i in range(dim) )) # magnitude
        lte2[j] = sqrt(sum(est[j,i]*est[j,i] for i in range(dim,2*dim) ))
        ci[j] = sqrt(sum(est[j,i]*est[j,i] for i in range(2*dim,3*dim) )) # confidence interval
        
    abscis = np.arange(len(lte1))
    gt_detect = np.array(corrupt[:,1], dtype=bool)
    detect =   np.array(corrupt[:,0], dtype=bool)

    order = np.argsort(lte1)
    x = lte1[order]
    y = lte2[order]
    gt_detect = gt_detect[order]
    detect = detect[order]
    ci = ci[order]
    
    ncorr_steps = abscis[~gt_detect]
    corr_steps = abscis[gt_detect * ~detect]
    x_cor = x[corr_steps]
    y_cor = y[corr_steps]
    x_ncor = x[ncorr_steps]
    y_ncor = y[ncorr_steps]
    
    fig = plt.figure()
    plt.plot(x_ncor, y_ncor, 'g+', label='Non corrupted data')
    plt.plot(x_cor, y_cor, 'ro', label='Corrupted data')
    plt.plot(x, x, 'c-', label='Prediction')
    plt.xlabel('$LTE 2$')
    plt.ylabel('$LTE 1$')
    plt.fill(np.concatenate([x, x[::-1]]), np.concatenate([x -  ci, (x + ci)[::-1]]),
                alpha=.5, fc='c', ec='None', label='Confidence interval')
  
    plt.title('Recovery based on the correlation between estimates: ' + app + " " + method + " " + scenario + " " + injection + " " + detector + " " + str(stepsize) )
    #plt.show()
    
    
def Velocity(application, method, injection, scenario, detector, stepsize):
    """Plot the velocity with finite difference of the solution. Display error from systematic case (third column for detection) """
    
    #if scenario == "NoError" or scenario == "NoCorrection":
    #    return

    # read results
    dim = application.GetDimension()
    app = application.GetName()
    corrupt_file = simu_folder + app + "_" + method + "_" + scenario + "_" + injection + "_" + str(stepsize) + ".csv"
    err_file = sol_folder + app + "_" + method + "_" + scenario + "_" + injection + "_" + str(stepsize) + ".csv"    
    corrupt = np.genfromtxt(corrupt_file, delimiter=",", dtype=int)
    sol = np.genfromtxt(err_file, delimiter=",")    

    v = np.zeros(len(sol))
    for j in range(1,len(sol)):  
        # x1[j] = abs(sol[j,0])
        # x2[j] = abs(sol[j,dim])
        v[j] = sqrt(sum(pow(sol[j,i]-sol[j-1,i],2) for i in range(dim) ))
    p = np.percentile(v, 90)
    print "90th percentile: %f" % p
    
    abscis = np.arange(len(v))
    gt_detect = np.array(corrupt[:,1], dtype=bool)
    if len(corrupt[0,:]) == 3:
        detect =   np.array(corrupt[:,2], dtype=bool)  
    else:
        detect =   np.array(corrupt[:,0], dtype=bool)  
    recover_steps = abscis[gt_detect * detect]
    corr_steps = abscis[gt_detect * ~detect]

    # plot 
    fig = plt.figure()
    ax = plt.axes()
    for x in corr_steps:
       ax.axvline(x, linestyle='--', color='r')
    for x in recover_steps:
        ax.axvline(x, linestyle='--', color='g')
    #        ax = plt.gca()
    max_y = max(v)
    min_y = min(v)
    plt.plot(abscis, v, 'b-', label='Corrected solution')

    plt.xlabel('$Time$',**cmr)
    plt.ylabel('$Solution$',**cmr)
#    plt.ylim([max(min_y,1e5), min(max_y,1e7)])
    plt.legend(loc='upper left')
    plt.title('Velocity for ' + app + " " + method + " " + scenario + " " + injection + " " + detector + " " + str(stepsize) )
    #plt.show()
       
       
def RelError(application, method, injection, scenario,detector, stepsize):
    """Plot the % detection as a fct of relative error  """
    
    # read results
    dim = application.GetDimension()
    app = application.GetName()
    inj_file = inj_folder + app + "_" + method + "_" + scenario + "_" + injection + "_" + detector + "_" + str(stepsize) + ".csv"    
    inj = np.genfromtxt(inj_file, delimiter=",", dtype=float)

    rel_err = np.zeros(len(inj),dtype="float")
    for j in range(len(inj)):
        #test = [np.isinf((inj[j,i]-inj[j,i+dim])**2) for i in range(1, 1+dim)]
        #if not (test[0] or test[1] or test[2]):
            rel_err[j] = sqrt(sum((inj[j,i]-inj[j,i+dim])**2 for i in range(1, 1+dim) )/ \
                sum(pow(inj[j,i],2) for i in range(1, 1+dim) ))
    mask = np.ma.masked_invalid(rel_err).mask 
    rel_real = rel_err[~mask]
    rel_real = rel_real[np.where(rel_real < 1e-7)]
    bound = [max(rel_real), min(rel_real)]
    step = (bound[0] - bound[1])/10    
    bins = np.zeros(10) #bin of relative errors
    detect1 = np.zeros(10)
    detect2 = np.zeros(10)
    for i in range(len(rel_err)):
        if mask[i] == True:
            idx = 9
        else:
            idx = trunc((rel_err[i]-bound[1])/step) if rel_err[i] != bound[0] else 9
        bins[idx] += 1
        detect1[idx] += 1 if inj[i,7] == 1 else 0
        detect2[idx] += 1 if inj[i,8] == 1 else 0
    
    for i in range(10):
        detect1[i] *= 100/bins[i] if bins[i] != 0 else 1
        detect2[i] *= 100/bins[i] if bins[i] != 0 else 1
        
    # plot 
    fig = plt.figure()
    x = np.arange(10)*step+bound[0]
    # x = 2*np.arange(10)
    plt.bar(x,detect1,facecolor="b",alpha=0.5,width=step, label='Detector 1')
    plt.bar(x,detect2,facecolor="m",alpha=0.5,width=step, label='Detector 2')

    plt.xlabel('Bins of relative error')
    plt.ylabel('% detection')
#    plt.ylim([max(min_y,1e5), min(max_y,1e7)])
    plt.legend(loc='upper left')
    plt.title('Relative error for ' + app + " " + method + " " + scenario + " " + injection + " " + str(stepsize) )
    #plt.show()
       
       
       
