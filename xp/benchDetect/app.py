# pierre-louis.guhur@laposte.net
# created: 12.28.15

from math import *
import subprocess
import os

NaN = float('nan')
wrf_exe = "~/bin/sfm/expt/wrf2sl"
# wrf_exe = "~/bin/test/sfm/expt/wrf2sl"

# solving x' = x with Runge-Kutta methods and testing the detector on it
class Exponential:
    """Application for solving x' = x"""

    ini_t = 0
    ini_x = 1
    lim_t = 100
    stepsizes = [0.01, 0.001]
    dim = 1
    
    def GetDimension(self):
        return self.dim

    
    def Function(self, t,x):
        """ compute f. the one if f(x,t) = x'(x,t)"""
        return x
        
    def GetStepsizes(self):
        return self.stepsizes
        
    def GetInitialPoint(self):
        return [self.ini_t, self.ini_x]
        
    def GetTimeLimit(self):
        return self.lim_t
    
    def GetName(self):
        return "exponential"
        
    def Solution(self,t):
	return exp(t)
        
class yDiv_t:
    """Eq: y' = y/t. Sol: y = ini_t*t"""

    ini_t = 1
    ini_x = 1
    lim_t = 100
    stepsizes = [0.01, 0.001]
    dim = 1
    
    
    def GetDimension(self):
        return self.dim
        
    def Function(self, t,x):
        """ compute f. the one if f(x,t) = x'(x,t)"""
        return x/t
        
    def GetStepsizes(self):
        return self.stepsizes
        
    def GetInitialPoint(self):
        return [self.ini_t, self.ini_x]
        
    def GetTimeLimit(self):
        return self.lim_t
    
    def GetName(self):
        return "yDiv_t"
    
    def Solution(self,t):
	return self.ini_t*t
        
class sqr_t:
    """Eq: y' = sqrt(y)*t. Sol: 1/16(t^2+4)^2"""
    ini_t = 0.0
    ini_x = 1.0
    lim_t = 1000.0
    stepsizes = [10]
    dim = 1
    
    def GetDimension(self):
        return self.dim
    
    def Function(self, t,x):
        """ compute f. the one in f(x,t) = x'(x,t)"""
        return sqrt(abs(x))*t
        
    def GetStepsizes(self):
        return self.stepsizes
        
    def GetInitialPoint(self):
        return [self.ini_t, self.ini_x]
        
    def GetTimeLimit(self):
        return self.lim_t
    
    def GetName(self):
        return "sqr_t"
    
    def Solution(self,t):
	 return 1.0/16.0*(t*t+4.0)*(t*t+4.0)
        
class HullB4:
    """Hull1972B4. quoted in paper of Emil"""
    ini_t = 0.0
    ini_x = [3.0,0.0,0.0]
    lim_t = 1000.0
    stepsizes = [1]
    dim = 3
    
    def GetDimension(self):
        return self.dim
    
    def Function(self, t,x):
        """ compute f. the one in f(x,t) = x'(x,t)"""
        return [-x[1]-x[0]*x[2]/sqrt(x[0]**2+x[1]**2), \
                 -x[0]-x[1]*x[2]/sqrt(x[0]**2+x[1]**2), \
                 x[0]/sqrt(x[0]**2+x[1]**2)]
        
    def GetStepsizes(self):
        return self.stepsizes
        
    def GetInitialPoint(self):
        return [self.ini_t, self.ini_x]
        
    def GetTimeLimit(self):
        return self.lim_t
    
    def GetName(self):
        return "HullB4"
    
    def Solution(self,t):
	 return NaN
     
     
class Sin:
    """Eq: y' = sin(y)"""

    ini_t = 0.0
    ini_x = 1.0
    lim_t = 10.0
    stepsizes = [100]
    dim = 1
    
    def Function(self, t,x):
        """ compute f. the one in f(x,t) = x'(x,t)"""
        return sin(x)
        
    def GetDimension(self):
        return self.dim
        
    def GetStepsizes(self):
        return self.stepsizes
        
    def GetInitialPoint(self):
        return [self.ini_t, self.ini_x]
        
    def GetTimeLimit(self):
        return self.lim_t
    
    def GetName(self):
        return "sin"
        
    def Solution(self,t):
	    return NaN
        
        
                 
class wrf:
    # wrf application

    ini_t = 0.0
    ini_x = 1.0
    lim_t = 10.0
    stepsizes = [1]
    dim = 3
    
    def Launch(self, scenario, injection, method, detector, stepsize,maxire=-1,minire=-1):
        cmd = wrf_exe + ' --stepsize ' + str(stepsize) + " --scenario " + scenario + " --injection " + injection + \
                          " --method " + method + " --detector " + detector + " --data 18  --number -1 --minire 7e-8"#--order wrf_errors.csv"
        if maxire>0:
            cmd += " --maxire " + str(maxire)
        if minire > 0:
            cmd += " --minire " + str(minire)
        print cmd
        os.system(cmd)
        print "End of WRF's simulation"

    def GetDimension(self):
        return self.dim
        
    def GetStepsizes(self):
        return self.stepsizes
        
    def GetInitialPoint(self):
        return [self.ini_t, self.ini_x]
        
    def GetTimeLimit(self):
        return self.lim_t
    
    def GetName(self):
        return "wrf"
        
    def Solution(self,t):
	    return NaN
    
    
    