# basic functions for dealing with simulations
# pierre-louis.guhur@laposte.net
# created: 12.23.15

import os, sys
import ConfigParser
from simulation import *
from app import *
import plots
import matplotlib.pyplot as plt
import time

simu_folder = "results/simulation/"
rate_folder = "results/rate/"

# return the parameter
def initialization(filename):
    config = ConfigParser.ConfigParser()
    config.read(filename)
    all_meth = ['HeunEuler1', 'HeunEuler2', 'RalstonEuler', 'BogackiShampine',"CashKarp", 'FehlbergI', 'FehlbergII', "DormandPrince", 'RK41','RK41c','RK31']
    all_scen = ['NoError', 'NoCorrection', 'NonSystematic', 'Input', 'Memory', 'Bug']
    all_appl = ['Exponential', 'Sin', 'yDiv_t', 'sqr_t', 'wrf', 'HullB4']
    all_inje = ['Single', 'Multi']
    all_plot = ['LTEs', 'Errors', 'Solution', 'LTEvsLTE', 'Velocity','RelError']
    all_det = [ 'Benson','AID','HighSens','LowFP',"Perfect"]
    meth, scen, appl,plt,inj,det= [],[],[],[],[],[]
    for i in all_meth:
        if int(config.get('Method', i)):
            meth.append(i) 
    for i in all_inje:
        if int(config.get('Injection', i)):
            inj.append(i)
    for i in all_scen:
        if int(config.get('Scenario', i)):
            scen.append(i)
    for i in all_appl:
        if int(config.get('Application', i)):
            a = eval(i+'()')
            appl.append(a)
    for i in all_plot:
        if int(config.get('Plot', i)):
            plt.append(i)
    for i in all_det:
        if int(config.get('Detector', i)):
            det.append(i)
            
    return [meth, inj, scen, appl, det, plt]
    
# launch a simulation whether its results for each stepsize doesn't exist
def launch(application, method, injection, scenario, detector, l_plt):
    simulateur = Simulation()
    simulateur.SetApplication(application)
    simulateur.SetMethod(method)
    simulateur.SetInjection(injection)
    simulateur.SetScenario(scenario)
    simulateur.SetDetector(detector)
        
    stepsizes = simulateur.GetStepsizes()
    
    for stepsize in stepsizes:
        simulateur.SetStepsize(stepsize)
        if not simulateur.IsDone():
            start = time.time()
            sys.stdout.write("Simulation: " + simulateur.GetName() + " " + method + " " + \
            scenario + " " + injection +  " " + detector + " stepsize:" + str(stepsize) + "\n")
            simulateur.Simulate()
            sys.stdout.write("Time elapsed: " + str(time.time()-start) + "\n")
       
        rate(application,method,injection,scenario, detector,stepsize)
        
        if "LTEs" in l_plt:
             plots.LTE(application, method, injection, scenario, detector, stepsize)
    	if "Errors" in l_plt:
                 plots.Errors(application, method, injection, scenario, detector, stepsize)
    	if "Solution" in l_plt:
                 plots.Solution(application, method, injection, scenario, detector, stepsize)
    	if "LTEvsLTE" in l_plt:
                 plots.LTEvsLTE(application, method, injection, scenario, detector, stepsize)
    	if "Velocity" in l_plt:
                 plots.Velocity(application, method, injection, scenario, detector, stepsize)
    	if "RelError" in l_plt:
                 plots.RelError(application, method, injection, scenario, detector, stepsize)
  

def rate(application, method, injection, scenario, det, stepsize, TPR = False):

    
    # read results
    app = application.GetName()
    corrupt_file = simu_folder + app + "_" + method + "_" + scenario + "_" + injection + "_" + det + "_" + str(stepsize) + ".csv"
    est_file = est_folder + app + "_" + method + "_" + scenario + "_" + injection + "_" + det + "_" + str(stepsize) + ".csv"    
    rate_file = rate_folder + app + "_" + method + "_" + scenario + "_" + injection + det + "_" + ".csv"    
    np_rates = np.genfromtxt(corrupt_file, delimiter=",", dtype=int)
    corrupt = np.genfromtxt(corrupt_file, delimiter=",", dtype=int)
    est = np.genfromtxt(est_file, delimiter=",")   
    
    gt_nsys = np.array(corrupt[:,1], dtype=bool)
    gt_sys = np.array(corrupt[:,3], dtype=bool)
    det_nsys =   np.array(corrupt[:,0], dtype=bool)
    det_sys =   np.array(corrupt[:,2], dtype=bool)
    
    fp_nsys = det_nsys * ~gt_nsys
    tp_nsys = det_nsys * gt_nsys     
    fp_sys = det_sys * ~gt_sys
    tp_sys = det_sys * gt_sys
    
    if TPR:
        return float(sum(tp_nsys))/float(sum(gt_nsys))
        
    # transform existing rates into a dictionary
    dico_rates = dict()
    for i in range(len(np_rates)):
        dico_rates[np_rates[i,0]] = np_rates[i,1:]
        
    # add/update rate for given stepsize

    Np_nsys = float(sum(gt_nsys))
    Nn_nsys = float(sum(~gt_nsys))
    tpr_nsys = sum(tp_nsys)*100.0/Np_nsys if Np_nsys != 0 else NaN
    fpr_nsys = sum(fp_nsys)*100.0/Nn_nsys if Nn_nsys != 0 else NaN
    Np_sys = float(sum(gt_sys))
    Nn_sys = float(sum(~gt_sys))
    tpr_sys = sum(tp_sys)*100.0/Np_sys if Np_sys != 0 else NaN
    fpr_sys = sum(fp_sys)*100.0/Nn_sys if Nn_sys != 0 else NaN
    dico_rates[stepsize] = [tpr_nsys, fpr_nsys,tpr_sys, fpr_sys]
    
    #display and save
    print "Rates for non systematic errors: " + app + " " + method + " " + scenario + " " + injection + " " + det+ " " + str(stepsize) + \
            ' -- TPR: %.2f (%d) FPR: %.2f (%d)' % (tpr_nsys, sum(tp_nsys), fpr_nsys, sum(fp_nsys))
    print "Rates for systematic errors: " + app + " " + method + " " + scenario + " " + injection + " "  + det+ " " + str(stepsize) + \
                ' -- TPR: %.2f (%d) FPR: %.2f (%d)' % (tpr_sys, sum(tp_sys), fpr_sys, sum(fp_sys))
    fid = open(rate_file,"w")
    for h, pr in dico_rates.iteritems():
        for i in pr:
            fid.write("%.2f" % (i))
        fid.write('\n')
    fid.close()    
    
    
def ire(application, method, injection, scenario, det, stepsize):
    """Compute the Injected Relative Error detected 95% of the times """
    simulateur = Simulation()
    simulateur.SetApplication(application)
    simulateur.SetMethod(method)
    simulateur.SetInjection(injection)
    simulateur.SetScenario(scenario)
    simulateur.SetDetector(det)
    
    stepsizes = simulateur.GetStepsizes()
    stepsize = stepsizes[0] #only for the first stepsize
    simulateur.SetStepsize(stepsize)
    
    ratio = 0.5 #each step, we divide minIRE by 2
    nb_sl = 5
    
    if det == "AID":
        minIRE = 2e-4
        maxIRE = 1e-4
        maxBit = 51
        minBit = 30
    elif det == "Benson":
        minIRE = 2e-5
        maxIRE = 1e-5
        maxBit = 51
        minBit = 30
    elif det == "HighSens":
        minIRE = 2e-3
        maxIRE = 1e-3
        maxBit = 41
        minBit = 20
    elif det == "LowFP":
        minIRE = 2e-1
        maxIRE = 1e-1
        maxBit = 41
        minBit = 20
    
    #create a file with bit:
    order = open("wrf_err_ire.csv", 'w')
    for i in range(50,980,50):
        bit = (i % (maxBit - minBit)) + minBit
        order.write("%d, 1, %d\n" % (i, bit))
    
    
    i = 0
    # simulateur.SetMinIRE(minIRE)
   #  simulateur.SetMaxIRE(maxIRE)
   #  simulateur.Simulate()
   #  old_tpr = rate(application,method,injection,scenario, detector,stepsize, TPR = True)
    while i < 10 :
        print "Test %d : minIRE = %f " % (i,minIRE)
        i += 1
        # modify wrf_errors.csv 
        simulateur.Simulate()
        simulateur.SetMinIRE(minIRE)
        simulateur.SetMaxIRE(maxIRE)
        new_tpr = rate(application,method,injection,scenario, detector,stepsize, TPR = True)
        if isnan(new_tpr):
            nb_sl += 5
             
        if new_tpr > 0.95:
            minIRE *= ratio
            maxIRE *= ratio
        else:
            print "End"
            break
    
