# this file deals with output
# pierre-louis.guhur@laposte.net
# created: 4.1.16

import os, sys
import ConfigParser
from math import *

simu_folder = "results/simulation/"
rate_folder = "results/rate/"
est_folder = "results/estimate/"
sol_folder = "results/solution/"
res_folder = "results/"

class Output:
    """Ouput for simulations"""
    
    def SetApplication(self, appl):
        self.app = appl.GetName()
        
    def SetMethod(self, method):
        self.method = method
                    
    def SetInjection(self, injection):
        self.injection = injection
    
    def SetScenario(self, scenario):
        self.scenario = scenario
     
    def SetDetector(self, detector):
         self.detector = detector
            
    def SetStepsize(self,stepsize):
        self.stepsize = stepsize
  
    def GetCorruptFile(self):
        return simu_folder + self.app + "_" + self.method + "_" + self.scenario + "_" + self.injection + "_" + self.detector + "_" + str(self.stepsize) + ".csv"
        
    def GetRateFile(self):
        return rate_folder + self.app + "_" + self.method + "_" + self.scenario + "_" + self.injection + "_" + self.detector + "_" + str(self.stepsize) + ".csv"
    
    def GetEstFile(self):
        return est_folder + self.app + "_" + self.method + "_" + self.scenario + "_" + self.injection + "_" + self.detector + "_" + str(self.stepsize) + ".csv"
        
    def GetResFile(self):
        return res_folder + self.app + "_" + self.method + "_" + self.scenario + "_" + self.injection + "_" + self.detector + "_" + str(self.stepsize) + ".csv"
    
    def GetSolFile(self):
        return sol_folder + self.app + "_" + self.method + "_" + self.scenario + "_" + self.injection + "_" + self.detector + "_" + str(self.stepsize) + ".csv"
    

    def Exist(self):
        return os.path.isfile(self.GetCorruptFile())
        
    def Start(self):
        print self.stepsize
        self.rates = open(self.GetRateFile(), 'w')
        self.est = open(self.GetEstFile(), 'w')
        self.corrupt = open(self.GetCorruptFile(), 'w')
	self.sol = open(self.GetSolFile(), 'w')
	
    def End(self):
        self.corrupt.close()
        self.est.close()
        self.rates.close()
        self.sol.close()
        
    def Estimates(self,a,b,c,sigma):
        #[self.est.write(i) for i in a]
        #[self.est.write(i) for i in b]
        [self.est.write("%.15e, " %i) for i in a]
        [self.est.write("%.15e, " %i) for i in b]
        [self.est.write("%.15e, " %i) for i in c]
        [self.est.write("%.15e, " %i) for i in sigma]
        self.est.write("\n")
        
    def Rates(self,a,b):
        [self.rates.write(i) for i in a]
        [self.rates.write(i) for i in b]
        self.rates.write("\n")

    def Corrupt(self,detect,gt,bit=""):
        self.corrupt.write(str(int(detect)) + ", " + str(int(gt)) + ", 0, 0")
        if bit != "":
            self.corrupt.write(", " + str(bit))
        self.corrupt.write("\n")
        
    def Solution(self,x1,x2,gt):
        [self.sol.write("%.15e, "%i) for i in x1]
        if not isnan(gt):
            [self.sol.write("%.15e, "%i) for i in x2]
            for i in range(len(gt) - 1) :
                self.sol.write("%.15e, "%gt[i])
            self.sol.write("%.15e"%gt[len(gt)-1])
        else:
            for i in range(len(x2) - 1) :
                self.sol.write("%.15e, "%x2[i])
            self.sol.write("%.15e"%x2[len(x2)-1])
        self.sol.write("\n")
    
                
    