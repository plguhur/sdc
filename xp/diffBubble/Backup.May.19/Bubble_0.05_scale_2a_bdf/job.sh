#!/bin/bash 
#PBS -e log/e${PBS_JOBID}.txt 
#PBS -o log/o${PBS_JOBID}.txt 
#PBS -l nodes=1:ppn=16,walltime=00:01:00 
#PBS -m ae 
#PBS -M pguhur@anl.gov
./init
mpiexec -n 8\
 ~/xp/hypar/bin/HyPar\
 -log_view ascii:log_view.txt \
  -ts_inject -ts_inject_mode multi -ts_inject_proba 0.05 \
  -ts_adapt_basic_double_lte none \
  -ts_rk_type 2a
