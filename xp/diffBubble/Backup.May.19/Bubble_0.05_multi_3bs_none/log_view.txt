************************************************************************************************************************
***             WIDEN YOUR WINDOW TO 120 CHARACTERS.  Use 'enscript -r -fCourier9' to print this document            ***
************************************************************************************************************************

---------------------------------------------- PETSc Performance Summary: ----------------------------------------------

/homes/pguhur/xp/hypar/bin/HyPar on a arch-linux2-c-debug named red with 8 processors, by pguhur Sat May 14 16:29:02 2016
Using Petsc Development GIT revision: pre-tsfc-428-ge4f61bb  GIT Date: 2016-05-09 13:04:14 -0500

                         Max       Max/Min        Avg      Total 
Time (sec):           8.581e+03      1.00000   8.581e+03
Objects:              1.000e+00      1.00000   1.000e+00
Flops:                0.000e+00      0.00000   0.000e+00  0.000e+00
Flops/sec:            0.000e+00      0.00000   0.000e+00  0.000e+00
Memory:               2.805e+04      1.00000              2.244e+05
MPI Messages:         0.000e+00      0.00000   0.000e+00  0.000e+00
MPI Message Lengths:  0.000e+00      0.00000   0.000e+00  0.000e+00
MPI Reductions:       6.000e+00      1.00000

Flop counting convention: 1 flop = 1 real number operation of type (multiply/divide/add/subtract)
                            e.g., VecAXPY() for real vectors of length N --> 2N flops
                            and VecAXPY() for complex vectors of length N --> 8N flops

Summary of Stages:   ----- Time ------  ----- Flops -----  --- Messages ---  -- Message Lengths --  -- Reductions --
                        Avg     %Total     Avg     %Total   counts   %Total     Avg         %Total   counts   %Total 
 0:      Main Stage: 8.5808e+03 100.0%  0.0000e+00   0.0%  0.000e+00   0.0%  0.000e+00        0.0%  4.000e+00  66.7% 

------------------------------------------------------------------------------------------------------------------------
See the 'Profiling' chapter of the users' manual for details on interpreting output.
Phase summary info:
   Count: number of times phase was executed
   Time and Flops: Max - maximum over all processors
                   Ratio - ratio of maximum to minimum over all processors
   Mess: number of messages sent
   Avg. len: average message length (bytes)
   Reduct: number of global reductions
   Global: entire computation
   Stage: stages of a computation. Set stages with PetscLogStagePush() and PetscLogStagePop().
      %T - percent time in this phase         %F - percent flops in this phase
      %M - percent messages in this phase     %L - percent message lengths in this phase
      %R - percent reductions in this phase
   Total Mflop/s: 10e-6 * (sum of flops over all processors)/(max time over all processors)
------------------------------------------------------------------------------------------------------------------------


      ##########################################################
      #                                                        #
      #                          WARNING!!!                    #
      #                                                        #
      #   This code was compiled with a debugging option,      #
      #   To get timing results run ./configure                #
      #   using --with-debugging=no, the performance will      #
      #   be generally two or three times faster.              #
      #                                                        #
      ##########################################################


Event                Count      Time (sec)     Flops                             --- Global ---  --- Stage ---   Total
                   Max Ratio  Max     Ratio   Max  Ratio  Mess   Avg len Reduct  %T %F %M %L %R  %T %F %M %L %R Mflop/s
------------------------------------------------------------------------------------------------------------------------

--- Event Stage 0: Main Stage

------------------------------------------------------------------------------------------------------------------------

Memory usage is given in bytes:

Object Type          Creations   Destructions     Memory  Descendants' Mem.
Reports information only for process 0.

--- Event Stage 0: Main Stage

              Viewer     1              0            0     0.
========================================================================================================================
Average time to get PetscTime(): 9.53674e-08
Average time for MPI_Barrier(): 0.00010457
Average time for zero size MPI_Send(): 2.41399e-05
#PETSc Option Table entries:
-log_view ascii:log_view.txt
-ts_adapt_basic_double_lte none
-ts_inject
-ts_inject_mode multi
-ts_inject_proba 0.05
-ts_rk_type 2a
#End of PETSc Option Table entries
Compiled without FORTRAN kernels
Compiled with full precision matrices (default)
sizeof(short) 2 sizeof(int) 4 sizeof(long) 8 sizeof(void*) 8 sizeof(PetscScalar) 8 sizeof(PetscInt) 4
Configure options: --with-cc=gcc --with-cxx=g++ --with-fc=gfortran --download-fblaslapack --download-mpich --with-shared-libraries --with-debugging=1 --with-64-bit-ints
-----------------------------------------
Libraries compiled on Mon May  9 13:47:06 2016 on red 
Machine characteristics: Linux-3.5.0-54-generic-x86_64-with-Ubuntu-12.04-precise
Using PETSc directory: /homes/pguhur/src/petsc
Using PETSc arch: arch-linux2-c-debug
-----------------------------------------

Using C compiler: /homes/pguhur/src/petsc/arch-linux2-c-debug/bin/mpicc    -Wall -Wwrite-strings -Wno-strict-aliasing -Wno-unknown-pragmas -g3  ${COPTFLAGS} ${CFLAGS}
Using Fortran compiler: /homes/pguhur/src/petsc/arch-linux2-c-debug/bin/mpif90    -Wall -Wno-unused-variable -ffree-line-length-0 -Wno-unused-dummy-argument -g  ${FOPTFLAGS} ${FFLAGS} 
-----------------------------------------

Using include paths: -I/homes/pguhur/src/petsc/arch-linux2-c-debug/include -I/homes/pguhur/src/petsc/include -I/homes/pguhur/src/petsc/include -I/homes/pguhur/src/petsc/arch-linux2-c-debug/include
-----------------------------------------

Using C linker: /homes/pguhur/src/petsc/arch-linux2-c-debug/bin/mpicc
Using Fortran linker: /homes/pguhur/src/petsc/arch-linux2-c-debug/bin/mpif90
Using libraries: -Wl,-rpath,/homes/pguhur/src/petsc/arch-linux2-c-debug/lib -L/homes/pguhur/src/petsc/arch-linux2-c-debug/lib -lpetsc -Wl,-rpath,/homes/pguhur/src/petsc/arch-linux2-c-debug/lib -L/homes/pguhur/src/petsc/arch-linux2-c-debug/lib -lflapack -lfblas -lX11 -lpthread -lhwloc -lssl -lcrypto -lm -Wl,-rpath,/nfs/software/linux-ubuntu_precise_amd64/apps/packages/gcc-4.8.0/lib/gcc/x86_64-unknown-linux-gnu/4.8.0 -L/nfs/software/linux-ubuntu_precise_amd64/apps/packages/gcc-4.8.0/lib/gcc/x86_64-unknown-linux-gnu/4.8.0 -Wl,-rpath,/nfs/software/linux-ubuntu_precise_amd64/apps/packages/gcc-4.8.0/lib/gcc -L/nfs/software/linux-ubuntu_precise_amd64/apps/packages/gcc-4.8.0/lib/gcc -Wl,-rpath,/nfs/software/linux-ubuntu_precise_amd64/apps/packages/gcc-4.8.0/lib64 -L/nfs/software/linux-ubuntu_precise_amd64/apps/packages/gcc-4.8.0/lib64 -Wl,-rpath,/lib/x86_64-linux-gnu -L/lib/x86_64-linux-gnu -Wl,-rpath,/usr/lib/x86_64-linux-gnu -L/usr/lib/x86_64-linux-gnu -Wl,-rpath,/nfs/software/linux-ubuntu_precise_amd64/apps/packages/gcc-4.8.0/lib -L/nfs/software/linux-ubuntu_precise_amd64/apps/packages/gcc-4.8.0/lib -lmpifort -lgfortran -lm -lgfortran -lm -lquadmath -lm -lmpicxx -lstdc++ -Wl,-rpath,/homes/pguhur/src/petsc/arch-linux2-c-debug/lib -L/homes/pguhur/src/petsc/arch-linux2-c-debug/lib -Wl,-rpath,/nfs/software/linux-ubuntu_precise_amd64/apps/packages/gcc-4.8.0/lib/gcc/x86_64-unknown-linux-gnu/4.8.0 -L/nfs/software/linux-ubuntu_precise_amd64/apps/packages/gcc-4.8.0/lib/gcc/x86_64-unknown-linux-gnu/4.8.0 -Wl,-rpath,/nfs/software/linux-ubuntu_precise_amd64/apps/packages/gcc-4.8.0/lib/gcc -L/nfs/software/linux-ubuntu_precise_amd64/apps/packages/gcc-4.8.0/lib/gcc -Wl,-rpath,/nfs/software/linux-ubuntu_precise_amd64/apps/packages/gcc-4.8.0/lib64 -L/nfs/software/linux-ubuntu_precise_amd64/apps/packages/gcc-4.8.0/lib64 -Wl,-rpath,/lib/x86_64-linux-gnu -L/lib/x86_64-linux-gnu -Wl,-rpath,/usr/lib/x86_64-linux-gnu -L/usr/lib/x86_64-linux-gnu -Wl,-rpath,/usr/lib/x86_64-linux-gnu -L/usr/lib/x86_64-linux-gnu -Wl,-rpath,/nfs/software/linux-ubuntu_precise_amd64/apps/packages/gcc-4.8.0/lib -L/nfs/software/linux-ubuntu_precise_amd64/apps/packages/gcc-4.8.0/lib -ldl -Wl,-rpath,/homes/pguhur/src/petsc/arch-linux2-c-debug/lib -lmpi -lgcc_s -ldl 
-----------------------------------------

