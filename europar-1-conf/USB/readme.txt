LNCS 9833
 
In order to read this data carrier you will need an up-to-date Internet 
browser and Adobe Acrobat Reader. 
 
If you run a Windows system, you can install Adobe Acrobat Reader  
directly from the acrobat directory on this data carrier. Change to the  
/acrobat/ directory on this data carrier and start one of the following 
files: 
 
Acrobat Reader 9.3 (Windows 7 / Vista / XP [SP 3, SP 2] / 2000 [SP 4]) 
/acrobat/vista_engl/AdbeRdr80_en_US.exe 
/acrobat/vista_germ/AdbeRdr80_de_DE.exe 
 
In the directories /acrobat/mac_engl/ and /acrobat/mac_germ/ you can 
find installation files for Macintosh (English and German, respectively). 
 
You can get other Adobe Acrobat Reader versions for other operating  
systems at: 
 
http://get.adobe.com/reader/otherversions/
