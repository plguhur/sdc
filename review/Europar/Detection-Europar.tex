\documentclass[runningheads,a4paper]{llncs}

\newcommand*{\rootPath}{./}

\input{\rootPath Annexes/Packages.tex}

\standalonetrue

\title{Resilience To Silent Data Corruptions In Solving Ordinary Differential Equations}
\titlerunning{Resilience To SDC In Solving ODE} 

\begin{document}

\author{Pierre-Louis Guhur\inst{1,2} \and Hong Zhang\inst{1}  \and Tom Peterka\inst{1} \and Emil Constantinescu\inst{1} \and Franck Cappello\inst{1}}
\authorrunning{Guhur et al.}
\institute{
Argonne National Laboratory (USA)\\
\email{\{hongzh, emconsta, tpeterka, cappello\}@mcs.anl.gov}\and
ENS de Cachan (France)\\
\email{pierre-louis.guhur@laposte.net}
}

\maketitle

\begin{abstract}
% context
Silent Data Corruptions (SDC) are errors that corrupt the system state, or falsify results, while remaining unnoticed. 
Detecting SDC in high performance computing (HPC) for numerical integration solvers is necessary, because their results need to be trustworthy and the increase of the number and complexity of components in emerging large scale architectures makes them more likely to occur.
% problematique
Until recently, SDC detection/mitigation methods consisted in replicating the processes of the execution or in using checksums (ABFT). 
Recently, new detection methods have been propose relying on mathematical properties of numerical kernels or performing data analytics of the results modified by the application.  
However none of those approaches provide a lightweight solution guaranteeing that all SDC beyond a user set threshold were detected.
% solution
We propose a new method to detect SDC offering a solution to this problem. It checks and potentially corrects the data produced by numerical integration solvers. We present two detectors. The first detector has a high detection accuracy, while a second has a low false detection rate. Our theoretical model shows that all SDCs beyond a user set threshold can be detected.
We developed this method for an explicit Runge-Kutta solver, although it can be generalized to other solvers.
% results
Experiments are conducted on streamline integration from the WRF meteorology application. Compared to the best algorithmic detection methods, the accuracy of our first detector is increased by $52 \%$ with a similar false detection rate. The second has a false detection rate one order or magnitude lower than the best existing algorithmic detectors while reaching similar detection accuracy.


\end{abstract}


\keywords{resilience, fault-tolerance, Runge-Kutta, numerical integration  solvers, HPC, SDC}


%%=============================================================================

\section{Introduction}
\label{sec:intro}

% reason for resilience: challenge for HPC, improving trust into results
Trusts in results has always been a critical challenge for scientists. Trust in results of numerical simulations could be impaired by Silent Data Corruptions (SDCs). Because of an ever increasing rate of faults in top machines, all exascale reports project an increase of the SDC rate in future systems [Snir11]. 

% difficulties: dealing with sys and nsys sdc, stochastic process
 The origins of SDCs are wide: whereas some of them are due to electromagnetic interferences \cite{lapinsky2006electromagnetic}, ionizing radiation \cite{bagatin2015ionizing} and aging might also be important sources of SDC. %What makes the challenge even more difficult is that this program may be stochastic: with the same input, outputs may differ and so SDC would not be the same.

% traditional tools arenot a good answer: ecc (high energy consumption + big error), replication (too much overheads), checkpointing (waste of time), abft for linear algebra: pb with big errors
Replication \cite{guerraoui1997software} allows to detect SDC by duplicating the same program (or other versions in n-version programming \cite{chen1978n}) and comparing their results. In a deterministic program, all duplications must provide exactly the same result; otherwise it means results are corrupted. 
%Checkpointing methods \cite{koo1987checkpointing} store results at certain time of execution. In case of a crash, the program restarts at the last checkpoint and results after it are recomputed. It assumes that the program will not crash again with the same inputs. 
The protection of linear algebra in ABFT \cite{huang1984algorithm}, \cite{chen2013online}, \cite{silva1998practical} and the error-correction code memory (ECC memory) \cite{ghosh2005selecting} are both based on checksums, though the first one computes and performs detection inside the software and the second one inside the hardware. All these methods are generic (although ABFT is limited to certain numerical kernels) and provide arguably satisfying results. However, are either too expensive (replication) or do not detect all SDCs: ABFT covers only the data used in the kernel and not the other data used by the application; ECC protect only memory, caches and registers. It usually does not protect the CPU control logic nor its functional units.

% new tools: sheng&franck: cool but pb with stiff problems, schreiber: cool but idem
% intuitive: bring more information: detecting coherence instead of checksums will filter biggest errors. problem: assumption fct continuous.
In the context of iterative, time stepping methods, new detection techniques compare the result of the numerical method with the ones produced by a surrogate function.  Because previous steps of the numerical method have already been validated, the surrogate function can use these values as trusted references to compute its own results for the current step. In the Adaptive Impact Driven (AID) detector \cite{di2016aid}, the surrogate function computes value predictions for the current step by extrapolation from several past steps of the numerical method. If the difference between the numerical method results and the surrogate function predictions is outside of a certain confidence interval, a SDC is reported. AID uses different extrapolation methods and selects dynamically the one that minimizes the prediction error. The confidence interval is built from the acceptable bound upon which SDC become significant in the results (given by the user), the number of false positives and the maximum error of extrapolation. Following a different direction, Benson et al. \cite{benson2014silent} propose a more complex surrogate function computing estimates that is embedded in the numerical kernel \cref{sec:emb}. Like in AID the estimate calculation use prediction method (linear extrapolation). If the estimate for the current step is not similar to previous estimates, a SDC is reported. Their detector is called \emph{BS14} thereafter. While the two approaches are different, they both use extrapolation and thus rely on the smoothness property of the data set (AID) or of the estimate (BS14) to perform accurate detection. They do not guarantee that all SDCs that have an impact on the results remain undetected, in particular in when the data set (AID) or the estimate (BS14) present stiff variations. Moreover none of them provide a mathematical guarantee that all SDCs exceeding the approximation error of the solver are detected.

We consider that a SDC is significant, when their impact on results is significant compared with the 

% our solutions: comparing 2 lte. correlated iff no error. if recomputed value is still wrong: then check with a zero fp detector.
Our objective is to design and develop a new SDC detection technique that will present high detection accuracy and a low false detection rate, does not rely on extrapolation and is mathematically proven to detect all significant SDCs. In the context of numerical integration solvers, we have built two new detectors relying on mathematical properties of the ODE integration method. In numerical simulation, a solver is chosen because its approximation error is acceptable with respect to the required accuracy of the results. Our detection technique compares two estimates of this approximation error.  The estimates are designed to be correlated if and only if no significant SDC occurs. A confidence interval on the correlations, established from a simple machine learning algorithm, controls the SDC detection. In the case where a SDC is detected, the correction is done by recomputing the step. The two detectors present different tradeoffs: one has a high accuracy (we call it \emph{HighSens}) and small false detection rate. The other one has a negligible false detection rate but also a lower accuracy (we call it \emph{LowFP}). The two detectors have been designed to protect the Cash-Karp's method  \cite{cash1990variable}, a fourth-order Runge-Kutta method with a fixed-step size. However our technique can be applied to other ODE methods as discussed in \cref{sec:ccl}. 

% our contributions: algorithm for detecting non sys. + recovery, lte estimate, great detectors
Because all significant SDCs are detected, our detectors improve the trustworthiness of the results, while they avoid a waste of resources to recover from insignificant SDC. 
% xp: real case
Experiments have been performed on a streamline integrator used for the visualization of WRF meteorology application results~\cite{peterka_ipdps11}, \cite{mcloughlin09}. 

The paper is organized as follows: in \cref{sec:context}, explanations about the context are brought and an ad hoc LTE estimate is derived. In \cref{sec:method}, the method for detecting SDC is detailed and recall is computed. In \cref{sec:xp}, it is tested in a meteorology application and compared with replication, AID detector and \emph{Embedded}.



\section{Context}
\label{sec:context}
% what is ode? where do we find them?

ODE is a differential equation of one independent variable and its derivatives. It contrasts with a partial differential equation (PDE) in the meaning that a PDE can have more than one independent variable. Solving time-dependent PDE may end up solving ODE if method of lines is used. Because numerical integration solvers are widely used, the trust of their results is critical.  For the initial value problem, a problem can be settled into the following formulation:
\begin{equation}
x'(t) = f(t,x(t)), x(t_0) = x_0,
\end{equation}
with : $t_0 \in \mathbb{R}, x_0 : \mathbb{R} \rightarrow \mathbb{R}^N, f : \mathbb{R} \times \mathbb{R}^N \rightarrow \mathbb{R}^N$. $f$ is $L$-Lipschitz continuous.


% what is rkm? bogacki-shampine? emb est? radau extrapolation? bfd?
\subsection{Presentation of Runge-Kutta Methods}
\label{sec:rkm}

For each $n = 1,...,N$ with $N$ the total number of steps, Runge-Kutta methods (RKM) provide an approximation $x_{n}$ of $x(t_n)$ where $t_n = t_0 + nh$, $h \in \mathbb{R}^*_+$ the step-size and $x(t_n)$ the exact solution of the ODE.
A s-stage explicit RKM is defined by:
\begin{align*}
\forall i\leq s, k_i = f\left(t_n + c_ih, x_n + h\sum_{j = 1}^{i-1}{a_{ij}k_i} \right), 
x_{n+1} = x_n + h\sum_{j = 1}^s{b_i k_i}.
\end{align*}
$(k_i)_i$ are called the stages and represents the most computational expensive part of the method.
Denoting $x(t,x_n)$ the exact solution of the ODE at the time $t_n$, $x'(t) = f(t,x(t)), x(t_n) = x_n$,
the Local Truncation Error (LTE) that is the approximation error introduced at a step $n+1$ can be defined by $LTE_{n+1} = x_{n+1} - x(t_{n+1},x_n)$. The Global Truncation Error (GTE) is absolute difference between the correct value $x(t_n,x_0)$ and the approximated value $x_n$. A ODE integration method is said to have an order $p$, if $LTE_n = O(h^p+1)$ and $GTE_N = O(h^p)$ where $N$ is the last step.


In the following, we focus on the Cash-Karp method \cite{cash1990variable}, while generalization is discussed in \cref{sec:ccl}. The Cash-Karp's method has an order 4 and computes 6 stages. With respect to the classical Runge-Kutta method which has an order 4 with 4 stages, two extra-stages are required here. But it allows to compute the embedded method which is used in \emph{Embedded}, in our detectors and in adaptive integration method.



\subsection{The embedded methods}
\label{sec:emb}
LTE can be estimated with \emph{embedded methods}. Those methods compute two results $x_n^p$ and $x_n^{p'}$ from two RKM with respective orders $p$ and $p'$ (in general $|p'-p| = 1$). The solution is propagated by one of them, whereas the other one is computed with the last result and stages from the first one (though some extra-stages can be required) to achieve a low overhead. If $LTE^{p'}_n$ is at least one order higher than $LTE^p$, the difference between $x_n^p$ and $x_n^{p'}$ estimates $LTE^p$:
\begin{align*}
x_n^p - x_n^{p'} &=  x_n^{p} - x(t_n,x_{n-1}) - \left(x(t,x_{n-1}) - x_n^{p'}\right), \\
&= LTE^{p}_n -LTE^{p'}_n, \\
&= LTE^{p}_n + O(h^{p'+1}).
\end{align*}
In the case of the Cash-Karp's method, $p=4$ and $p'=5$.
%the embedded estimate is equal to $LTE^{4}_n + O(h^{6})$, because the solution is propagated with a 4 order method, and the second method is a 5 order method. 
%The extension of the article for other RKM can be done with Verner's methods \cite{verner1978explicit} for high order RKM, Gauss-Legendre's \cite{chawla1968error}, Radau's or Lobatto's methods \cite{butcher2005front} for implicit RKM. In the adaptive RKM, an LTE estimate is already computed for adapting the step-size.


\subsection{The Radau's quadrature}

Another way for estimating LTE is suggested by Stoller and Morrison \cite{stoller1958method} and extended in \cite{ceschino1966numerical}. Relying on the Radau quadrature and the Taylor's expansion, Ceschino and Kuntzmann \cref{ceschino1966numerical} give an expression of the LTE of a method with a order $p\leq 5$. The estimate $\mathcal{R}$ called here the \textit{Radau's estimates} does not require the computation of any extra-stage, but it checkpoints previous stages and solutions. Therefore, it has a memory overhead, rather than a computational overhead like the embedded method. 
%For more explanations about it, the interested reader should look at \cite{butcher1987numerical}.  Because these LTE estimates are obtained from the Radau quadrature, we will refer to these methods as the \textit{Radau's estimates}. An extension for adaptive methods has been achieved in \cite{butcher1993estimating}.
Since the embedded estimate is 6 order, the following estimate $\mathcal{R}$ is used:
\begin{align*}
\mathcal{R} &= \frac{h}{10}\left[f(t_{n-3},x_{n-3}) + 6f(t_{n-2},x_{n-2}) + 3f(t_{n-1},x_{n-1}) \right], \\
&+ \frac{1}{2}\left[11x_{n-3} + 27x_{n-2} - 27x_{n-1} - 11 x_n\right].
\end{align*}


%\subsection{The Richardson's extrapolation}

%The Richardson's extrapolation \cite{richardson1927deferred} can estimate the LTE of any numerical integration solvers. Because it is the method that requires the most extra-stages, we have not used it for the Cash-Karp's method. However, it could be used as a replacement of other estimates, or even of the confidence interval.




\section{Method}
\label{sec:method}

The method relies on a surrogate function $\Delta_n$ that is the difference between two estimates: $\Delta_n = \mathcal{A}_n - \mathcal{B}_n$. For the Cash-Karp's method, we use the embedded estimate and the Radau's estimate. Though other estimate such as the Richardson's extrapolation \cite{richardson1927deferred} could have been employed. When no SDC occurs, the surrogate function is expected to be almost zero: 
\begin{equation}
\Delta_n = \left(LTE_n + O(h^6)\right) - \left(LTE_n + O(h^6)\right) = O(h^6).
\end{equation}

Our surrogate function is one order higher than the LTE. Hence, SDC whose introduced errors are smaller than the approximation error are expected to be detected. 
In the first detector \emph{HighSens}, the surrogate function is compared to a certain confidence interval. A proof shows that undetected SDC are insignificant with respect to the approximation error. However its false positive rate might be significant. That is why, we derived a second detector with a larger confidence interval. Now its false positive rate remains above a few percent.


\subsection{First detector: \emph{HighSens}}
\label{sec:frst_det}
% radau and emb are correlated iff no error
The difference between the Radau's estimate and the embedded estimate is an order 6 while the GTE is an order 4. Therefore, these estimates appear well correlated. Moreover, they are using a high diversity of terms: the embedded estimate uses the second method's results, whereas the Radau's estimate uses some solutions and stages from previous steps. In case of a corrupted data, they react thus differently and are not correlated anymore. Knowing that this difference is inside a certain confidence interval induce a SDC detection. The latter assertion is proofed hereafter.

\subsubsection{Proof of the efficiency}
% give a proof of it
The efficiency of this detector can be measured by the minimum injected error $\epsilon_{min}$ that can be detected, namely that push $\Delta_n$ outside of the confidence interval. 
%We will show that:
%\begin{equation}
% \epsilon_{min} = O(\sigma_n/h)
% \label{eq:eps_min}
%\end{equation}

The proof is done only for an corrupted stage $k_i$, as the case of a corrupted result itself is similar. Here, $\epsilon_{min}= k_i^c - k_i^o$, where $^c$ (resp. $^o$) denotes a corrupted (resp. uncorrupted) data.
\begin{align*}
 \Delta^{c}_n - \Delta^{o}_n &= \mathcal{E}^{c}_n - \mathcal{E}^{o}_n - \left(\mathcal{R}^{c}_n - \mathcal{R}^{o}_n\right), \\
 &=  hk_i\epsilon \left[ \hat{b}_i + b_i \left( \frac{1}{30} - \frac{3\delta_{i,1}}{10}  \right) \right], \\
\end{align*}
where $\delta_{ij}$ is the Kronecker's symbol which is defined by $\delta_ij = 1$ if $i=j$ else $\delta_{ij} = 0$, $(b_i)$ (resp.  $(\hat{b}_i)$) are the coefficients of the 4 (resp. 5) order in the Cash-Karp's method. 

$\epsilon$ is minimum when $\Delta_n^c = \mathcal{C}_n$ with $\mathcal{C}_n$ the half-length of the confidence interval at step $n$. In the worst case, $\Delta^{o}_n = 0$. We note $C = k_i\hat{b}_i + b_i \left( \frac{1}{30} - \frac{3\delta_{i,1}}{10} \right)$. This leads to: 
\begin{align*}
\epsilon_{min} = \frac{\mathcal{C}_n}{hC} = O\left(\frac{\mathcal{C}_n}{h}\right).
\end{align*}
When $x_n$ is corrupted instead of a stage, one can derive that $\epsilon_{min} = O(\mathcal{C}_n)$.
If $\mathcal{C}_n$ is at the same order of $\Delta_n$, then $\epsilon_{min} = O(h^5)$ when an error is injected inside a stage and $\epsilon_{min} = O(h^6)$ when an error is injected inside a result. In other words, the threshold of detection is at the same order (or better) than the LTE of the Cash-Karp's method. This means that undetected errors are insignificant one. Actually, correcting a error that is lower than the approximation error of the RKM can be considered as a waste of time.

\subsubsection{Confidence interval}
%this leads to a first conf int

Because $\Delta_n = O(h^6)$, one can assume than $\Delta_n$ acts as a random variable with a zero-mean, when no SDC occurs. Then, its standard deviation can be estimated from a training set $T$ composed of $N$ samples with the unbiased sample standard deviation:
\begin{equation}
 \sigma = \sqrt{\frac{1}{N-1} \sum_{n=1}^N{\Delta_n^2}}.
 \label{eq:sigma}
\end{equation}

Assuming that $(\Delta_n)_n$ follows a normal distribution, the ``three-sigma
rule''~\cite{krishnamoorthy2009statistical} suggests to choose $\mathcal{C}_n = 3\sigma$. Thus, we expect that $99.7\%$ of uncorrupted $(\Delta_n)_n$ fell within the confidence interval, or in other words a false positive rate (FPR) of $0.3\%$. The normal distribution is a natural choice for modeling the repartition
of training samples. However, when the samples
are not always independent, the central limit theorem \cite{fischer2010history}
cannot by applied and this hypothesis is invalidated. We will show the equation \eqref{eq:adapt-cont} corrects it.

Because items from $T$ are not labeled as trusted or untrusted samples, the evaluation of $\sigma$ might be done corrupted $(\Delta_n)_n$ that can have high values. It seriously jeopardizes the confidence interval and thus the SDC detector. To improve reliability, we have weighted each $\Delta_n$ with its own value. Equation~\ref{eq:sigma} becomes thus:

\begin{align*}
 \Sigma = \sum_{n=1}^N{\exp{(-\Delta_n^2)}}, 
 \sigma = \sqrt{\frac{1}{\Sigma} \sum_{n=1}^N{\exp{(-\Delta_n^2)}\Delta_n^2}} .
% \label{eq:sigma2}
\end{align*}

\subsubsection{Adaptive control}
\label{sec:adapt-cont}
When an SDC is reported, the current step is recomputed. If the result has the same value, we can assume it was a false positive and not a SDC.  Because of the normal assumption, the false positive rate (FPR) is expected to be $0.3\%$. In case of the FPR is an order of magnitude higher, at roughly $3\%$ for $k$ times, the confidence
interval is increased with a certain coefficient $1+\alpha$. $\mathbb{C}_n$ becomes: $\mathcal{C}_n = (1+\alpha)^k\times 3\sigma$.
$\alpha$ fixes the rate of the adaptive control. Because $(1+\alpha)^k = 1 + \alpha k + O(\alpha^2)$, we suggest to take $\alpha = 10/\max{FPR}/N_{steps}$ where $N_{steps}$ is the number of steps in the application and $\max{FPR}$ is the maximum acceptable false positive rate. The latter could be set with respect to the computational overhead's limit, because a false positive requires the re-computation of a noncorrupted step. Such choice allows the confidence interval to vary with a ratio of 10. In our experiments, we have $N_{steps} = 1000$ and $\max{FPR} = 0.05$, thus $\alpha = 0.2$.

Thanks to the adaptive control, the length of the training can strongly be reduced. In our experiments, we have observed that a length of 5 samples is enough to initialize the confidence interval.


\subsection{Second detector: \emph{lowFP}}
\label{sec:scnd_det}
% but we need a second detector: est1
Because the first detector can suffer from an important false positive rate, we have designed a second one with a larger confidence interval. Nonetheless, undetected errors ought to stay on an acceptable level.

This new confidence interval is defined by:
\begin{align}
\mathbb{C}_n = 10C_{99}(|\Delta| \in T).
\end{align}
$C_{99}$ denotes the 99th percentile of the training set. The interval can be interpreted as a threshold that is an order of magnitude bigger than the surrogate functions in the training set. 
Because this threshold is higher than the previous one, this detector's recall is lower. Because the estimates are at order $6$ for the Cash-Karp's method, the LTE at step n can be expressed as $LTE_n = Ch^5+O(h^6)$. The worst case where a SDC is undetected is: $\mathbb{C}_n = 10*\max(\mathcal{A},\mathcal{B}) = 10Ch^5 + O(h^6)$. Moreover, we can assume the probability that a SDC occurs and that the surrogate function is lower than this detector is small enough to guarantee that at worst only one SDC will be accepted. The worst case is when the SDC is accepted at the first step $n=1$. We will see that the GTE at the last step $N$ is still an order $4$ as it used to be without corruption.

At the first step:
\begin{align}
|GTE_{1}| = |x(t_{1})-x_{1}| = 10Ch^5 + O(h^6).
\end{align}
With $\tilde{x}$ the solution of the problem $x'(t) = f(t,x(t)), x(t_n) = x_n$, one can write that the GTE at a step $0<n< N$ is:
\begin{align}
|GTE_{n+1}| &= |x(t_{n+1})-\tilde{x}(t_{n+1}) + x(t_{n+1})-\tilde{x}(t_{n+1})|, \\
&\leq |x(t_{n+1})-\tilde{x}(t_{n+1})| + |x(t_{n+1})-\tilde{x}(t_{n+1})|. 
\end{align}

Because $f$ is $L$-Lipschitz continuous, the Gronwall's inequality simplifies the first term to:
\begin{equation}
|x(t_{n+1})-x(t,x_{n+1})| \leq |x(t_{n},x_0)-x(t,x_n)|e^{Lh} = |GTE_n|e^{Lh}.
\end{equation}
The second term,  $|x_{n+1}-x(t,x_{n+1})|$, is the LTE at the step $n+1$ and so is evaluated at $Ch^5+O(h^6)$. 
Denoting $\alpha=e^{Lh}$, we obtain:
\begin{align*}
\frac{|GTE_{n+1}|}{\alpha^{n}} \leq \frac{|GTE_n|}{\alpha^{n-1}} + \frac{Ch^5}{\alpha_i^{n}}
\leq ... \leq |GTE_1| + Ch^5\sum_{i=1}^n {\frac{1}{\alpha^i}}.
\end{align*}
Because $\sum_{i=1}^N {1/\alpha^i}=(\alpha^N-1)/\alpha^N(\alpha-1)$ and $\alpha - 1 \geq Lh$,  we achieve, noting $T=Nh$:
\begin{equation}
|GTE_n+1| \leq  10Ch^5 + \frac{Ch^4}{L}\left(e^{Lnh}-1\right) + O(h^6).
\end{equation}
At the last step, we have well verified that $GTE_n = O(h^4)$. 



% finally this is the algorithm
\subsection{Algorithm}
We have presented two detectors and showed their efficiency. While the second one has a FP rate lower than the second one, its recall is reduced. However, we have seen that accepting a SDC is still acceptable provided that a few SDC are accepted. The first detector needs the user to fix a parameter, whereas the second one is fully automatized.
We can thus derive two scenarii: in one hand, if a SDC is likely to happen (it could be the case when the processor is not protected from SDC by ECC memory or other protection system), then the first detector should be employed.  In the other hand, if the assumption of an unlikely SDC below the second threshold is validated, then the use of the second detector is enough.
The schema is illustrated in \cref{alg:likely-sdc}.

\begin{algorithm}[!h]
  \While{learning} {
  step $\leftarrow$ simulation(prev. step) \;
  $\Delta \leftarrow |est1(step, prev. steps) - est2(step, prev. steps) |$ \;
  TraininigSet.push($\Delta$) \;
  }
 \While{new step} {
  step $\leftarrow$ simulation(prev. step) \;
  $\Delta \leftarrow |est1(step, prev. steps) - est2(step, prev. steps) |$ \;
   \If{ (Detector $==$ \emph{HighSens} and $\Delta \leq C_n$) or (Detector $==$ \emph{LowFP} and $\Delta \leq \mathbb{C}_n$)} {
	report(``no error'') \;
	accept step \;
  		}
  \Else{
    		step $\leftarrow$ simulation(prev. step) \;
  		$\Delta' \leftarrow |est1(step, prev. steps) - est2(step, prev. steps) |$ \;

    		\If{ (Detector $==$ \emph{HighSens} and $\Delta' \leq C_n$) or (Detector $==$ \emph{LowFP} and $\Delta' \leq \mathbb{C}_n$)} {
			report(``false positive'') \;
			accept step \;
			\If{FP rate $> 3\%$}{k++ \;}
	    	  }
	    \Else{
			report(``systematic SDC'') \;
	    }
  	}
 }
	\caption{Pseudocode for the execution of our detectors}
	\label{alg:likely-sdc}
\end{algorithm}




\section{Experiments and results}
\label{sec:xp}
% presentation + hypotheses that no error occurs during recovery
\subsection{Environment}

We evaluated the SDC detector with a meteorology application described below. It was computed on a 
machine with four Intel Xeon E5620 CPUs (each with 4 cores and 8 threads), 12 GB RAM, and one NVIDIA Kepler K40 GPU
with 12 GB memory. It was programmed in C++11 using 
CUDA. The application is particle tracing for streamline flow
visualization~\cite{peterka_ipdps11}, \cite{mcloughlin09}. The result can be seen in \cref{fig:sl}.

\begin{figure*}[htb!]
\centering
			\includegraphics[width=.59\textwidth]{../img/flux.png}
		\centering
			\caption{Streamlines computed by the use case}
			\label{fig:sl}
\end{figure*}


	



\subsection{SDC Injection Methodology}
\label{sec:methodo}

SDC can arise from many sources in hardware and software~\cite{nicely2008pentium}, \cite{hwang2012cosmic} and these sources may change with new versions and generations of hardware and
software. 
We do not attempt to evaluate exhaustively the coverage of our approach due to lack of space.
Like most of SDC detectors, we assume that recomputing the same step will not lead to the exact same corruption. 
SDC are simulated by flipping bits in data items. SDC affect one or several
bits in the same data item, which case is called respectively single and multi-bit corruption, thereafter.
Because no model for SDC detection study has been unanimously accepted, we experimented with both cases. In multi-bit
corruption, we chose the number of bit flips $Nflips$ from a uniform distribution. Other
distributions such as normal and beta distributions were tested with several different parameters, but the
results were not significantly different from those reported below.
Corruption can affect data items in any stage (or even directly in the result). The position of a bit-flip is drawn from a uniform distribution. In multi-bit corruption, we have forced the $Nflips$ bits-flips to be applied on $Nflips$ different positions.
Some SDC have no impact on the results. In a third scenario, we inject only significant single-bit corruptions. We considered that a SDC is  significant when the difference between the corrupted result and the safe result is higher than the mean LTE divided by 100.

\subsection{Benchmark}
Our approach is compared with similar methods presented in \cref{sec:intro}: replication, AID and Benson et. al's detector. Those methods need to be parametrized. We have compared results with a set of parameters and pick the ones that provide best results. Using the same notation that in \cite{di2016aid}, AID is configured with $\theta r = 1$. Results are improved if the confidence interval is taken to $(1+\alpha)^k(\epsilon+\theta r)$ with $\alpha = 0.2$ and $k$ defined like in \cref{sec:adapt-cont}.
Concerning the other detector, five parameters should be set, but two of them are poorly documented. Eventually, \emph{Embedded} was parametrized in order to provide best results in our specific use-case. With the notation of \cite{benson2014silent}, the considered values are: $\tau_j = 1e^{-5}$, $\tau_v = 0.02$, $\Gamma = 1.4$, $\gamma = 0.95$ and $p=10$. %Finally, we combined the idea of taking as a surrogate function the embedded estimate as in \emph{Embedded}, the adaptive extrapolation as in AID and the confidence interval presented in \cref{sec:scnd_det} to create a new detector called \emph{Embedded 3}, which has no parameter to tune.


\subsection{Results}
\label{sec:res}

\begin{figure*}[htb!]
		\centering
			\includegraphics[width=.99\textwidth]{../img/benchmark.eps}
			\caption{One streamline computed by the different detectors. The curve without error is overlapped with the curve from our detectors. Single-bit injection is made every 50 steps. The position of the corrupted bit varies from 27 to 40.}
			\label{fig:solutions}
	\end{figure*}
	
	
% test non sys.: single and multibits
\begin{table}
\centering
\begin{tabular}{ r | c |c|c|c|c|c|c| }

  \multirow{2}{*}{Name} & \multicolumn{3}{c|}{TPR (\%)} & \multirow{2}{*}{FPR (\%)} & \multirow{2}{*}{IRE 95\%} & \multicolumn{2}{c|}{Overhead  (\%)}  \\ \cline{2-4} \cline{7-8}

     & Single-bit & Multi-bit & Critical & & & Computation & Memory \\
     \hline
  HighSens & $28.6$ & $69.6$ & $100.0$ & $1.2$ & $1e^{-8}$ & $+4.4$ & $7$ \\
  \hline
  LowFP & $23.1$  & $64.6$ & $100.0$ &  $0.01$ & $1e^{-8}$ & $+3.8$ & $7$ \\
  \hline
  Embedded & $ 18.8$ & $49.5$  & ? & $0.6$ & ? & $+3.7$ & $12$\\
  \hline
  AID & $14.3$ &  $43.2$ & ? & $1.7$ & ? & $+4.6$ & $4$ \\
  \hline
  Replication & $100.0$ & $100.0$ & $100.0$ & $100.0$ & $0.0$ & $+100$ & $7$

\end{tabular}
  \caption{Benchmark of our detectors with replication, AID and Embedded. The overhead is in computation and is estimated from the cost of extra-stages. Values in the column ``IRE 95\%'' are the injected relative errors (IRE) that were detected 95\% of the times.}
 \label{table:res}
\end{table}



\Cref{table:res} presents results from our benchmark, in which our two detectors were compared to replication, to AID detector \cite{di2016aid} and \emph{Embedded}.  To measure computational overheads, we have divided the time execution of the considered detector with those of a detector that knows the ground truth. It appears that our detectors are as lightweight than similar detectors. But, contrary to AID detector, they have to be employed on an embedded integration method which computes more stages than some other integration method of the same order. Memory overheads are define as the number of results that need to be stored. Here again, our detectors are in the same range than similar methods.

The true positive rate (TPR) shows that they detect perfectly significant SDC. Replication do as well, but with a more higher cost. 
Moreover, the ``IRE 95\%'' value of our detectors are smaller than the mean local error estimate by a factor 100.


\Cref{fig:solutions} shows that with our detectors, solutions are the same than when no error is injected. This is not the case for similar SDC detectors. 
% what about the overheads

% ccl


\section{Conclusion}
\label{sec:ccl}


This study has presented two new SDC detectors in the context of ODE solvers. Experiment and theory show that all significant SDC are detected. Excepts for replication, no actual SDC detectors achieve it. But contrary to replication, the computational overhead is strongly reduced. Compared to similar SDC detectors, the true positive rate for single-bits corruption is improved by at least $52\%$.
However, our detectors have still issues that should be corrected in future works. 

Firstly, the detectors were employed only for one of the ODE integration method. We firmly believe  that it could be improved. Other embedded Runge-Kutta methods can be directly employed. The Radau's estimates have a general expression in the case of adaptive step-size by Butcher and Johnston \ref{butcher1993estimating}. For implicit methods or linear multi-steps, the Richardson's estimates can replace theses estimates.  Last but not least, an extension for PDE solvers is planned, because similar methods are employed for them.

%Secondly, the computational overhead is higher than some other detectors, because of the embedded estimate. If the method is applied in an adaptive stepsize solver, this extra-cost is paid by the solver itself. Otherwise, the value of an estimate does not change significantly from step to the other one and it might not be necessary to compute the estimate at each step. In the same idea, the detector might have not to validate each step, because the Radau's estimate employs previous steps. Hence, a SDC located at step $n-1$ could be detected at step $n$ and all not validated steps will then be recomputed.


%It leads to a new kind of corruption's detection has been achieved for ODE solvers. It gathers a very good sensibility, while its overhead in memory and in computation is strongly limited. Because numerical integration solvers are a critical component in numerous applications, it is believed this method can be an important contribution. A theoretic model conducts to an expression of the sensibility. Coverage concerns both systematic and non-systematic SDC. Notably a wrong step-size can be detected, and only insignificant errors are undetected. An algorithm derived from a zero false-positive detector is presented for recovering. Experiments have been led in a meteorology application and compared to other SDC detectors.



Recovery assumes that the SDC would not be two times in a row exactly the same. However, this can happen, when the origin of a SDC is a bug. We plan to improve the coverage of our detectors by comparing our detectors with a lower order LTE estimate. This may provide a detector less sensitive but that does no make false positives. Such detector would then recognize the last example of SDC from a correct result.




%=============================================================================
\subsubsection*{Acknowledgments}

The authors wish to express their gratitude to Julie Bessac for her reading and critical analysts. The authors also gratefully acknowledge use of the services and facilities of the Decaf project at Argonne National Laboratory, funded by XXXXXX Grant XXXXXX.

%=============================================================================
%%=============================================================================
\ifstandalone
	\bibliographystyle{splncs03}
	\bibliography{\rootPath Annexes/biblio.bib}
\fi
%%=============================================================================
%%=============================================================================

\end{document}