===========================================================================
                           DSN 2016 Review #73A
---------------------------------------------------------------------------
Paper #73: Detecting Silent Data Corruption using an Auxiliary Method
           Overseen by an External Algorithmic Observer
---------------------------------------------------------------------------

                      Overall merit: 3. Weak accept
                 Reviewer expertise: 2. Some familiarity

                         ===== Paper summary =====

Silent data corruption (SDC) may occur due to external influence while data is stored or being processed, or from logical problems in the application. With their enormous amounts of data HPC applications are particularly vulnerable to SDC.  This study explores methods to detect SDC in HPC through re-validation of the computation output with a second algorithm solving the same problem in a different way.  The approach is therefore inherently specific to the considered problems.

The proposed observer is a systematic way of presenting a revalidation through a separate algorithm.  It is tailored to learning problems, where the observer can be an algorithm for the same problem with reduced accuracy, for example.  The validation metrics are also problem dependent, but the paper addresses mostly samples drawn from Gaussian distributions.

The generic method is described at a high level in section III.  The main contribution seems to be in section V, where three case studies are developed according to the method.  Three different kinds of SDC are explored.  Many problem specific heuristic methods are described in this section.

The construction of the paper (generic method is brief, the use cases are detailed) is symptomatic: It seems difficult to construct a generic validation method that works for many problems, and it is usually necessary to exploit domain-specific knowledge for constructing the validation.

The paper is clearly written and presents its method adequately.  Its main focus seems to be an audience in HPC, as is also evident from the references.  On the negative side, I question how useful the detailed results in section V will be for other work using the same approach.  As the generic component of the method is short compared to the adaptation to the benchmark problems, this seems difficult to generalize.

The algorithm in III should be described more accurately.  The special role of the "ImportantInput" values did not become clear, and the text does not refer to them.

The paper contains many detailed evaluation plots.  Their size or other problems in the PDF made it impossible to print this from Acrobat.  The use of color in the figures is not helpful unless other features are present ("purple" in Figure 2).

===========================================================================
                           DSN 2016 Review #73B
---------------------------------------------------------------------------
Paper #73: Detecting Silent Data Corruption using an Auxiliary Method
           Overseen by an External Algorithmic Observer
---------------------------------------------------------------------------

                      Overall merit: 1. Reject
                 Reviewer expertise: 3. Knowledgeable

                         ===== Paper summary =====

The paper describes a method to detect silent data corruptions for numerical computations by comparing results of two different methods the original one and an auxiliary method with a lower precision allowing faster computations. The results of both methods are compared using basic machine learning techniques.

                           ===== Strengths =====

A more or less relevant problem, evaluation based on real applications.

                          ===== Weaknesses =====

The general approach is not really new and relies on some assumption that usually do not hold. The computations of confidence intervals is based on independent normal distributed values. The output of the experiments is not normal distributed and probably also not independent.

                      ===== Comments for author =====

The proposed approach uses different algorithms to compute two results that are compared to detect silent error corruption and also bugs in a program. One algorithm is more efficient than the other one and yields less precise results. This is not completely new, in numerical analysis similar ideas are applied to estimate the error or the step length. However, I agree that it might be new to use this approach to detect errors. However, in my opinion the proposed approach has several weak points which have to resolved before the paper should be published.
Under the notation systematic corruption several different types or errors are subsumed which show a completely different effect and have to handled with different methods. E.g., a bug in the program from a bug of the memory.
The approach suffers in some cases from the same problem as N-version programming. If for example the routine computing the product of the vector with a matrix is erroneous, then RK-4 and Euler will bot suffer from the bug.
It is unclear how to assure that the training set contains no wrong results. To really train the method one has to be sure that both primary and auxiliary method yield correct results, otherwise the data is biased. I do not think that the approaches proposed in the paper really solve this problem.
The behavior of most iterative numerical methods depends heavily on the input. E.g., on some problems RK4 and Euler may yield very similar results whereas on other inputs a large difference occurs. I cannot see how this aspect can be taken into account. Thus, the approach may work for inputs similar to the training set but probably fails if the input changes.
The computation of confidence intervals and decisions based on the confidence level do not really help here. Confidence intervals are based on independent normally distributed data. I cannot see that the data used here observes these properties.

===========================================================================
                           DSN 2016 Review #73C
---------------------------------------------------------------------------
Paper #73: Detecting Silent Data Corruption using an Auxiliary Method
           Overseen by an External Algorithmic Observer
---------------------------------------------------------------------------

                      Overall merit: 3. Weak accept
                 Reviewer expertise: 2. Some familiarity

                         ===== Paper summary =====

This paper presents a framework for detecting systematic and
nonsystematic silent data corruption errors. The framework computes
the distance between the output of the primary method (i.e., program
or workflow) and the output of an auxiliary method (e.g., a relatively
lightweight approximation algorithm).  The distance is obtained using
a custom metric. Machine learning is used to train an external
algorithmic observer, which is used to determine whether the observed
distance is within the expected range (based on the inputs and
parameters of the processing methods) or whether the distance is
sufficiently high that an error should be flagged.  The paper applies
the framework to three HPC applications.

                           ===== Strengths =====

-The paper presents a general framework for detecting both systematic
and nonsystematic forms of silent data corruption errors, addressing
an important problem.

-The paper applies the proposed technique to three diverse use cases
and evaluates the performance (false positives, true positives,
overhead) in each case.

                          ===== Weaknesses =====

-The Evaluation section omits some important details and appears to
contain some errors, making it difficult to assess the effectiveness
of the proposed approach.

-While general, the proposed framework would seem to require a
significant amount of manual, per-application effort to choose an
auxiliary method and validate it. It would have been helpful for the
authors to include an indication of the level of effort required in
crafting custom metrics for each of the three applications, and how
one might estimate the level of effort for different kinds of
applications.

                      ===== Comments for author =====

-The second paragraph of Section IV mentions that the paper will
present two solutions for ensuring a trusted training set, but only
one (the bootstrapping process, using two trusted methods) seems to be
discussed.

-Please do not rely on colors to differentiate parts of a diagram.
Instead, use textures or labels such that the differences can be
appreciated even when printing in black and white.

-Figure 8 does not have a figure label.

-When injecting bit-flip errors, the primary method is always the one
corrupted. What about errors in the auxiliary method?

-Section IV-B should discuss how the prediction model is trained.

-In Table II and Table III, the "Cost" column should be explained.  It
seems to be (y / 32) * 100%, where the scan position is y x y, but the
text does not explain why this is the case.

-The values in Table IV appear to be simply the result of plugging
values into the formulas for P(TP) and P(FP) listed on Page 7, Column
1. It is not clear if any experiment was carried out.

-Figure 8(b) requires additional explanation. First, the legend should
be labeled; I assume each shows a different step size. However, the
legend lists "10" and "0.01" while the text and caption discuss step
sizes of "0.01" and "1".  The text states that the figure "shows in
which proportion some bits are not detected when they are flipped. In
the worst case, the highest undetected bit is the 32nd." It is not
clear to me how the graph shows that.

===========================================================================
                           DSN 2016 Review #73D
---------------------------------------------------------------------------
Paper #73: Detecting Silent Data Corruption using an Auxiliary Method
           Overseen by an External Algorithmic Observer
---------------------------------------------------------------------------

                      Overall merit: 2. Weak reject
                 Reviewer expertise: 3. Knowledgeable

                         ===== Paper summary =====

This paper presents a framework to detect both systematic and nonsystematic silent data corruption. For a given use case, both primary and auxiliary methods are run and validated by an external algorithmic observer. The paper describes the system including explaining the validation pipeline, defining the confidence interval, and how to build the trusted training set, etc. Three use cases are presented with detailed analysis.

                           ===== Strengths =====

- The evaluation with three use cases is done in a detailed fashion.

                          ===== Weaknesses =====

- The contribution and novelty is minor.
- The organization and presentation of the paper is weak.
- The framework proposed is general, however, the usage to a given application is special, which means for any use case, the primary and the auxiliary methods are needed to be designed and chosen specifically.

                      ===== Comments for author =====

This paper presents a framework to detect both systematic and nonsystematic silent data corruption with both primary and auxiliary methods which are validated by an external algorithmic observer. The paper explains the approach including a validation pipeline, how to define the confidence interval, and how to build the trusted training set, etc. Three use cases are presented in detail. However, the contribution and novelty is minor. Though the framework proposed is general, the usage of this approach to a given application is special, which means for any use case, primary method and auxiliary methods are needed to be designed and chosen specially.

Besides, the organization and presentation of the paper is weak. Some issues are as below:

1. With the first paragraph of Section III, it’s hard to understand “For simplicity, we simply call this the primary method”. A reader cannot understand what the “primary method” is until he gets to the detailed use cases in Section V.

2. The second paragraph of Section III presents material that would be provided in Section V, which makes it not friendly readable.

3. Throughout the whole paper, the equations are not labeled consistently, such as, “Equation (III-B)” but there is no equation marked as III-B. 

4. “three-sigma rule” is cited from a paper but there is no explanation on it. Also, what does “n” mean in the context?

6. Factors “m” and “k” appear in several equations, but there is no definition for them.

7. Figure 8 has no caption at all.

