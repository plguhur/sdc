# this file shows that an unstability increases the noise with a growing step size
from utils import *
import subprocess 
import sys, os, shutil
import matplotlib.pyplot as plt
import copy
from matplotlib import rc
rc('font',**{'family':'sans-serif','sans-serif':['Computer Modern Roman']})


xp = XP()
xp.pattern   = "lastLTE\s+([+-]?\d+(?:\.\d+)?(?:[eE][+-]?\d+)?)"
xp.adapt     = "radau"
xp.threshold = xp.pattern
xp.ts_type   = "rk"
xp.method    = "5dp"
xp.counter   = 0

#simple plot of Kulik2013I
# def groundTruth(time):
#     a = np.exp(np.sin(time*time));
#     b = np.exp(5.*np.sin(time*time));
#     c = np.sin(time*time)+1.0;
#     d = np.cos(time*time);
#     return np.sqrt(a*a+b*b+c*c+d*d)
#
# time = np.arange(0,5,0.001)
# sol = groundTruth(time)
#
# plt.figure()
# plt.xlabel("Time")
# plt.ylabel("Euclidean norm of the solution")
# plt.title("Ground truth of Kulik2013I")
# plt.plot(time, sol)
# plt.savefig("presentation-kulik.pdf", transparent=True)


#simple plot of Euler1D small perturbations with eta = 1 or 0
# xp.app      = "HyPar"
# xp.usecase  = "~/HyPar"
# xp.problem  = "/Users/pguhur/src/hypar/Examples/1D/Euler1D/SmallPerturbationWithGravity_02"
# xp.dims     = 20
# xp.space    = 1
# xp.n        = 1
# xp.stdout   = "stdout.log"
# xp.init_arg = 1
# xp.root     = "results/presentation/SmallPerturbationWithGravity_02/"
# xp.steps    = 100
# xp.ts_dt    = 1e-3
#
# Launch(xp)
# folder     = GetResultsFolder(xp)
# res        = np.genfromtxt(folder + '/op_00099.dat')
# plt.figure()
# plt.title("Ground truth of Euler1D")
# plt.plot(res[:,0], res[:,2], label="$\eta = 1.0$")
#
# xp.init_arg = 0
# Launch(xp)
# folder     = GetResultsFolder(xp)
# res        = np.genfromtxt(folder + '/op_00099.dat')
# plt.plot(res[:,0], res[:,2], label="$\eta = 0.0$")
# plt.savefig("presentation-euler1d.pdf", transparent=True)
# plt.show()

# simple plot of ShallowWater small perturbations with eta = 1 or 0
xp.app      = "HyPar"
xp.usecase  = "~/HyPar"
xp.init_arg = 0
xp.root     = "results/presentation/SW-SmallPerturbation2D/"
xp.problem  = "/Users/pguhur/src/hypar/Examples/2D/ShallowWater2D/SmallPerturbation"
xp.dims     = (41,21)
xp.space    = 2
xp.n        = 1
xp.stdout   = "" #"stdout.log"
#xp.init_arg = 1 
root        = "results/SmallPerturbation2D"
xp.steps    = 300

Launch(xp)
folder     = GetResultsFolder(xp)
res        = np.genfromtxt(folder + '/op_00299.dat')[:,5]
res        = np.reshape(res, xp.dims, order="F")
plt.figure()
plt.title("Ground truth of ShallowWater")
plt.plot(res, label="$\eta = 1.0$")
plt.show()    

